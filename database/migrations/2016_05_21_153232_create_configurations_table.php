<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('header_title');
            $table->string('main_title');
            $table->string('logo_image_link');
            $table->string('color_picker');
            $table->string('index_page_view');
            $table->string('localization');
            $table->string('email');
            $table->text('about_us');
            $table->string('office_address');
            $table->string('country');
            $table->string('phone_number');
            $table->string('currency_sign');
            $table->string('favicon');
            $table->string('twitter');
            $table->string('facebook');
            $table->string('youtube');
            $table->string('instagram');
            $table->string('linkedin');
            $table->string('copy_right');
            $table->text('mission');
            $table->text('vision');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configurations');
    }
}
