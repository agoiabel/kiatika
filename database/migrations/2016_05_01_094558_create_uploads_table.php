<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id');                 
            $table->integer('agent_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('purpose_id')->unsigned(); 

            $table->string('slug');
            $table->string('title'); 
            $table->integer('price'); 
            $table->text('address');
            // $table->longtext('short_description');
            $table->longtext('description');

            $table->boolean('featured')->default(false);
            $table->boolean('status')->default(false);          
            // $table->boolean('has_image')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uploads');
    }
}
