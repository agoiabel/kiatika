<?php

use App\Role;
use App\User;
use App\Blog;
use App\BlogTag;
use Illuminate\Database\Seeder;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $users = (new User())->where('role_id', '!=', Role::CUSTOMER)->get();

        foreach ($users as $user) {
            Blog::create([
                'authur_id' => $user->id,
                'category_id' => $faker->numberBetween($min = 1, $max = 10),
                'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'description' => $faker->paragraph($nbSentences = 7, $variableNbSentences = true),
                'image' => 'images/blog.jpg',
                'published_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
            ]);
        }

        $blogs = (new Blog())->get();

        foreach ($blogs as $blog) {
            (new BlogTag())->initModel(['blog_id' => $blog->id, 'tag_id' => $faker->numberBetween($min = 1, $max = 10)])->saveModel();    
        }

    }
}
