<?php

use App\Purpose;
use Illuminate\Database\Seeder;

class PurposeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $purposes = [
            ['name' => 'Sale'],
            ['name' => 'Rent'],
        ];

        foreach ($purposes as $purpose) 
        {
            Purpose::create($purpose);
        }

    }
}
