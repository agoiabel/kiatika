<?php

use App\Feature;
use Illuminate\Database\Seeder;

class PropertyFeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $features = [
            ['name' => 'Air Conditioning'],
            ['name' => 'Balcony'],
            ['name' => 'Bedding'],
            ['name' => 'Cable TV'],
            ['name' => 'Coffee Pot'],
            ['name' => 'Dishwasher'],
            ['name' => 'Fridge'],
            ['name' => 'Grill'],
            ['name' => 'Heating'],
            ['name' => 'Internet'],
            ['name' => 'Parking'],
            ['name' => 'Pool'],
            ['name' => 'Terrance'],
            ['name' => 'Toaster'],
        ];

        foreach ($features as $feature) 
        {
            Feature::create($feature);
        }

    }
}
