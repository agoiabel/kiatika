<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tags = [
            ['name' => 'Forsale'],
            ['name' => 'HTML'],
            ['name' => 'CSS'],
            ['name' => 'Design'],
            ['name' => 'Video'],
            ['name' => 'Wordpress'],
            ['name' => 'Layout'],
            ['name' => 'Computers'],
            ['name' => 'SmartPhone'],
            ['name' => 'Technology'],
        ];

        foreach ($tags as $tag) 
        {
            Tag::create($tag);
        }

    }
}
