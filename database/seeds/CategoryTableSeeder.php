<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            ['name' => 'Forsale'],
            ['name' => 'HTML'],
            ['name' => 'CSS'],
            ['name' => 'Design'],
            ['name' => 'Video'],
            ['name' => 'Wordpress'],
            ['name' => 'Layout'],
            ['name' => 'Computers'],
            ['name' => 'SmartPhone'],
            ['name' => 'Technology'],
        ];

        foreach ($categories as $category) 
        {
            Category::create($category);
        }

    }
}
