<?php

use App\City;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $cities = [
            ['city_name' => 'Lagos'],
            ['city_name' => 'Abeokuta'],
            ['city_name' => 'Oshogbo'],
        ];

        foreach ($cities as $city) 
        {
            City::create($city);
        }

    }
}
