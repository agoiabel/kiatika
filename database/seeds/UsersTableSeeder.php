<?php

use App\User;
use App\Role;
use App\Profile;
use App\Experience;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();


        # Create one admin account
        $user = User::create([
            'role_id' => Role::ADMIN,
            'city_id' => '1',
            'name' => 'admin',            
            'email' => 'johndoe@admin.com',
            'password' => 'abc',
            'status' => '1',
        ]);         

        Experience::create([
            'user_id' => $user->id,
        ]);

        Profile::create([
            'user_id' => $user->id,
            'image_icon' => 'images/user5.png',
            'front_image_icon' => 'images/emma-small.png',
            'phone' => $faker->phoneNumber,
            'about' => 'Single origin coffee crucifix tousled freegan lo-fi wayfare flexitaria Marfa deepbanh mi church-key direct trad street American Apparel Pinterest pop-up banh mi you probably.',
        ]);


        # Create one admin account
        $user = User::create([
            'role_id' => Role::AGENT,
            'city_id' => '1',
            'name' => 'agent',            
            'email' => 'johndoe@agent.com',
            'password' => 'abc',
            'status' => '1',
        ]);         

        Experience::create([
            'user_id' => $user->id,
        ]);

        Profile::create([
            'user_id' => $user->id,
            'image_icon' => 'images/user5.png',
            'front_image_icon' => 'images/emma-small.png',
            'phone' => $faker->phoneNumber,
            'about' => 'Single origin coffee crucifix tousled freegan lo-fi wayfare flexitaria Marfa deepbanh mi church-key direct trad street American Apparel Pinterest pop-up banh mi you probably.',
        ]);


        foreach (range(1, 11) as $key => $value) 
        {
            # Create twelve agent account
            $user = User::create([
                'role_id' => Role::AGENT,
                'city_id' => '1',
                'name' => $faker->name,            
                'email' => $faker->email,
                'password' => 'abc',
                'status' => '1',
            ]);         

            Experience::create([
                'user_id' => $user->id,
            ]);

            Profile::create([
                'user_id' => $user->id,
                'image_icon' => 'images/user5.png',
                'front_image_icon' => 'images/emma-small.png',
                'phone' => $faker->phoneNumber,
                'about' => 'Single origin coffee crucifix tousled freegan lo-fi wayfare flexitaria Marfa deepbanh mi church-key direct trad street American Apparel Pinterest pop-up banh mi you probably.',
            ]);
            
        }

    }
}
