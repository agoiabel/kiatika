<?php

use App\Upload;
use App\Feature;
use App\PropertyImage;
use App\PropertyFeature;
use App\Services\FileUploader;
use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $faker = Faker\Factory::create();

        // foreach (range(1, 15) as $key => $value) {
        //     Upload::create([
        //         'agent_id' => $faker->numberBetween($min = 1, $max = 13),
        //         'city_id' => $faker->numberBetween($min = 1, $max = 5),
        //         'type_id' => $faker->numberBetween($min = 1, $max = 2),
        //         'purpose_id' => $faker->numberBetween($min = 1, $max = 2),
        //         'title' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        //         'price' => $faker->randomNumber(),
        //         'address' => $faker->address,
        //         'description' => $faker->paragraph($nbSentences = 7, $variableNbSentences = true),
        //         'featured' => $faker->numberBetween($min = 0, $max = 1),
        //     ]);   
        // }

        // foreach (range(1, 30) as $key => $value) {
        //     PropertyImage::create([
        //         'property_id' => $faker->numberBetween($min = 1, $max = 15),
        //         'slider_path' => 'images/slider_2.jpg',
        //         'property_homepage_path' => 'images/property_homepage_path.jpg',
        //         'property_single_path' => 'images/slider_2.jpg',
        //     ]);
        // }


        // $properties = (new Upload())->get();
        // $features = (new Feature())->get();
        
        // foreach ($properties as $property) {
        //     foreach ($features as $feature) {
        //         (new PropertyFeature())->initModel(['upload_id' => $property->id, 'feature_id' => $feature->id, 'is_present' => $faker->numberBetween($min = 0, $max = 1)])->saveModel();
        //     }
        // }
    }
    
}
