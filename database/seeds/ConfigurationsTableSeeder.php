<?php

use App\Configuration;
use Illuminate\Database\Seeder;

class ConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuration::create([
            'logo_image_link' => 'images/logo2.png',
            'favicon' => 'upload/files/favicon.ico',
            'email' => 'info@kiatika.com',
            'about_us' => 'Kiatika is advanced real estate application kit. Template implements specialized components which will help you in the developement of your website or application.',
            'office_address' => '371 Linden Avenue Longwood, FL 32750',
            'phone_number' => '407-546-2034',
            'header_title' => 'Kiatika',
            'main_title' => 'Kiatika Real Estate',
            'currency_sign' => '$',
            'copy_right' => 'Copyright 2016. All Rights Reserved by Kiatika',
            'color_picker' => 'green.css',
            'index_page_view' => 'pages.testing_index',
            'vision' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            'mission' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
        ]);
    }
}
