<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FAQTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $question = 'Sed ullamcorper purus nec dapibus suscipit non nec turpis?';
        $answer = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla blandit eu eros nec ultrices. Aliquam gravida dictum odio sed gravida. Nunc posuere imperdiet lectus, et rutrum arcu bibendum eu. Praesent vitae purus vulputate, mattis dolor non, euismod urna. Vivamus porta et urna ultricies commodo.';

        foreach (range(1, 12) as $key ) {
            Faq::create([
                'question' => $question,
                'answer' => $answer,
            ]);                        
        }

    }
}
