<?php

use App\Service;
use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $services = [
            [
                'title' => 'You can List Your Services Here',
                'icon' => 'fa-thumbs-up',
                'description' => 'you can list your services here and this come from the backend where you can easily configure',
            ],
            [
                'title' => 'You can chose different type of home page',
                'icon' => 'fa-thumbs-up',
                'description' => 'you can list your services here and this come from the backend where you can easily configure',
            ],
            [
                'title' => 'You can chose different site colors',
                'icon' => 'fa-thumbs-up',
                'description' => 'you can list your services here and this come from the backend where you can easily configure',
            ],
        ];

        foreach ($services as $service) 
        {
            Service::create($service);
        }

    }
}
