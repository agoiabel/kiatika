<?php

use App\Testimonial;
use Illuminate\Database\Seeder;

class TestimonialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $testimonials = [
            [
            	'name' => 'Agoi Abel',
                'occupation' => 'web developer',
            	'testimony' => 'Vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen.',
                'image' => 'images/testim_1.jpg',
            ],
            [
                'name' => 'Agoi Abel',
                'occupation' => 'web developer',
                'testimony' => 'Vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen.',
                'image' => 'images/testim_1.jpg',
            ],
            [
                'name' => 'Agoi Abel',
                'occupation' => 'web developer',
                'testimony' => 'Vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen.',
                'image' => 'images/testim_1.jpg',
            ],
            [
                'name' => 'Agoi Abel',
                'occupation' => 'web developer',
                'testimony' => 'Vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen.',
                'image' => 'images/testim_1.jpg',
            ],
            [
                'name' => 'Agoi Abel',
                'occupation' => 'web developer',
                'testimony' => 'Vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen.',
                'image' => 'images/testim_1.jpg',
            ],
            [
                'name' => 'Agoi Abel',
                'occupation' => 'web developer',
                'testimony' => 'Vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor of willekeurig gekozen woorden die nog niet half geloofwaardig ogen.',
                'image' => 'images/testim_1.jpg',
            ],
        ];

        foreach ($testimonials as $testimonial) 
        {
            Testimonial::create($testimonial);
        }

    }
}
