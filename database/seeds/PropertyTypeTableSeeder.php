<?php

use App\PropertyType;
use Illuminate\Database\Seeder;

class PropertyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $types = [
            ['type' => 'House'],
            ['type' => 'Land'],
        ];

        foreach ($types as $type) 
        {
            PropertyType::create($type);
        }

    }
}
