<?php

use App\Client;
use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $clients = [
            [
            	'name' => 'Client 01',
            	'image_path' => 'images/partner_01.jpg',
            ],
            [
            	'name' => 'Client 02',
            	'image_path' => 'images/partner_02.jpg',
            ],
            [
            	'name' => 'Client 03',
            	'image_path' => 'images/partner_03.jpg',
            ],
            [
            	'name' => 'Client 04',
            	'image_path' => 'images/partner_04.jpg',
            ],
            [
            	'name' => 'Client 05',
            	'image_path' => 'images/partner_05.jpg',
            ],
            [
            	'name' => 'Client 06',
            	'image_path' => 'images/partner_03.jpg',
            ],
            [
            	'name' => 'Client 07',
            	'image_path' => 'images/partner_02.jpg',
            ],
        ];

        foreach ($clients as $client) 
        {
            Client::create($client);
        }

    }
}
