(function () {

	var submitRegAjaxRequest = function (e) {

		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name="_method"]').val() || 'POST';
		var url = form.prop('action');
		var data = form.serialize();

		$.ajax({
			type: method,
			url: url,
			data: form.serialize(),
			error: function(data) {

				var errors = data.responseJSON;

				if (errors.constructor !== Array) 
				{
					$('.javascript_flash').html(errors).fadeIn(300).delay(2500).fadeOut(300);
				}

				$('div.text-danger').remove();

				form.find(':input').map(function(i, input)
				{
				  	$(input.parentElement).removeClass('has-error');

					if (errors[input.name]) 
					{
					  	var err = errors[input.name];
		
						err.map(function(e){
							var errhtml = '<div class="text-danger '+e+'"">'+e+'</div>'
							$(input.parentElement).append(errhtml)
							$(input.parentElement).addClass('has-error')
						});
				  	}

				})

			},
			success: function (response, data, statusCode) {

				$('.javascript_flash').html(response).fadeIn(300).delay(3500).fadeOut(300);

			}	

		});
	};

	var submitAjaxRequest = function (e) {

		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name="_method"]').val() || 'POST';
		var url = form.prop('action');
		var data = form.serialize();

		$.ajax({
			type: method,
			url: url,
			data: form.serialize(),
			error: function(data) {

				var errors = data.responseJSON;

				if (errors.constructor !== Array) 
				{
					$('.javascript_flash').html(errors).fadeIn(300).delay(2500).fadeOut(300);
				}

				$('div.text-danger').remove();

				form.find(':input').map(function(i, input)
				{
				  	$(input.parentElement).removeClass('has-error');

					if (errors[input.name]) 
					{
					  	var err = errors[input.name];
		
						err.map(function(e){
							var errhtml = '<div class="text-danger '+e+'"">'+e+'</div>'
							$(input.parentElement).append(errhtml)
							$(input.parentElement).addClass('has-error')
						});
				  	}

				})

			}	
		});
	};


	// Form marked by "data-remote" will submit via ajax
	$('#loginForm').on('submit', submitAjaxRequest);
	$('#regForm').on('submit', submitRegAjaxRequest);

	// Form marked by "data-remote" will submit via ajax
	$('*[data-click-submits-form]').on('change', function () {
		$(this).closest('form').submit();
	});

})();
