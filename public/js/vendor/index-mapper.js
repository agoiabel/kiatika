var map;

var latitude = document.getElementById('latitude').textContent;
var longitude = document.getElementById('longitude').textContent;

function initialize() {

	var currentLocation = new google.maps.LatLng(latitude, longitude);

    var mapOptions = {
        center: currentLocation,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    for (var i = 0; i < var locations; i++)
    {
        var marker = new google.maps.Marker({
            position: currentLocation,
            map: map
        });
    }
    
}

google.maps.event.addDomListener(window, 'load', initialize);
