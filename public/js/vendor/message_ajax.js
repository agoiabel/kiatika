(function () {

	var submitAjaxRequest = function (e) {

		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name="_method"]').val() || 'POST';
		var url = form.prop('action');
		var data = form.serialize();

		$.ajax({
			type: method,
			url: url,
			data: form.serialize(),
			error: function(data) {

				var errors = data.responseJSON;

				if (errors.constructor !== Array) 
				{
					$('.javascript_flash').html(errors).fadeIn(300).delay(2500).fadeOut(300);
				}

				$('div.text-danger').remove();

				form.find(':input').map(function(i, input)
				{
				  	$(input.parentElement).removeClass('has-error');

					if (errors[input.name]) 
					{
					  	var err = errors[input.name];
		
						err.map(function(e){
							var errhtml = '<div class="text-danger '+e+'"">'+e+'</div>'
							$(input.parentElement).append(errhtml)
							$(input.parentElement).addClass('has-error')
						});
				  	}

				})

			},
			success: function (response, data, statusCode) {

				var msg = 'Your message was sent successfully'; 
				
				$('.javascript_flash').html(msg).fadeIn(300).delay(1000).fadeOut(300);

				var delay = 2000;

				window.setTimeout(function () {
					window.location.replace("http://propertynew.dev/message/sent");
				}, delay);

			}	

		});
	};

	// Form marked by "data-remote" will submit via ajax
	$('#messageForm').on('submit', submitAjaxRequest);

})();
