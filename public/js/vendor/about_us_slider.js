'use strict';
$("#pag-carousel").owlCarousel({
    items: 4,
    itemsDesktop: [1199, 4],
    itemsDesktopSmall: [979, 3],
    itemsTablet: [768, 2],
    itemsMobile: [479, 1],
    navigation: true,
    navigationText: [
        "<i class='fa fa-chevron-left icon-white'></i>",
        "<i class='fa fa-chevron-right icon-white'></i>"
    ],
    autoPlay: false,
    pagination: false
});
