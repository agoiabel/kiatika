        //Scripts to Activate the counters 

        $('.counter').counterUp({
            delay: 50,
            time: 5000
        });
        $('.progress-bar span').counterUp({
            delay: 10,
            time: 3000
        });
        
        $('ul.drop_menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });