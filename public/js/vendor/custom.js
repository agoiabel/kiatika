$(document).ready(function() {

	/** @owl index page slider navigation */
	$("#owl-single-slider").owlCarousel({
		autoPlay: true,
		singleItem:true,
		navigation: true,
		navigationText: ["<img src='images/slide_nav2.jpg'>", "<img src='images/slide_nav.jpg'>"],
		pagination: false, 
		responsive: true,
	});

	/** @client slider navigation */
	$('#owl-three-slider').owlCarousel({
		autoPlay: true,
		items:4,
		navigation: true,
	});

	/** @flexslider index page slider navigation */
    $('.flexslider').flexslider({
        animation: 'fade',
        controlsContainer: '.flexslider',
        controlNav: false,

    });


    $('#partner_logo').owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        autoPlay: true,
		pagination: false, 

        autoplayTimeout: 1500,
        autoplaySpeed: 1000,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            550: {
                items: 3
            },
            1001: {
                items: 5
            }
        }
    });
    

});