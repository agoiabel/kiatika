/* Product Slider Codes */
$(document).ready(function () {
    'use strict';

    $('#prop_slid').bxSlider({
        pagerCustom: '#slid_nav'
    });
	
	$('ul.drop_menu [data-toggle=dropdown]').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });	
});