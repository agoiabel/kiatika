(function () {

	var submitAjaxRequest = function (e) {

		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name="_method"]').val() || 'POST';
		var url = form.prop('action');
		var data = form.serialize();

		$.ajax({
			type: method,
			url: url,
			data: form.serialize(),
			success: function () {

				var message = form.data('remote-success-message');

				if (message === 'display_upload') 
				{
            		$('#display_upload').show();

            		return;
				}

				if (message === 'message sent successfully')
				{
					window.location.replace("http://propertynew.dev/message/inbox");
				}

				message && $('.javascript_flash').html(message).fadeIn(300).delay(2500).fadeOut(300);

			},
			error: function(data) {
				var errors = data.responseJSON;
				
				if (errors.constructor !== Array) 
				{
					console.log('not an array');
				}

				if (errors.constructor === Array) 
				{
					console.log("is an array");
				}


				$('div.text-danger').remove();

				form.find(':input').map(function(i, input)
				{
				  	$(input.parentElement).removeClass('has-error');

					if (errors[input.name]) 
					{
					  	var err = errors[input.name];
		
						err.map(function(e){
							var errhtml = '<div class="text-danger '+e+'"">'+e+'</div>'
							$(input.parentElement).append(errhtml)
							$(input.parentElement).addClass('has-error')
						});
				  	}

				})

			}	
		});
	};


	// Form marked by "data-remote" will submit via ajax
	$('form[data-remote]').on('submit', submitAjaxRequest);

	// Form marked by "data-remote" will submit via ajax
	$('*[data-click-submits-form]').on('change', function () {
		$(this).closest('form').submit();
	});

})();
