@extends('vendor.installer.layouts.master')

@section('title', trans('words.final.title'))
@section('container')
    <p class="paragraph">{{ session('message')['message'] }}</p>
    <div class="buttons">
        <a href="/" class="button">{{ trans('words.final.exit') }}</a>
    </div>
@stop