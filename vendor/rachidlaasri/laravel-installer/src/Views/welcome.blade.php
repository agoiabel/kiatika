@extends('vendor.installer.layouts.master')

@section('title', trans('words.welcome.title'))
@section('container')
    <p class="paragraph">{{ trans('words.welcome.message') }}</p>
    <div class="buttons">
        <a href="{{ route('LaravelInstaller::environment') }}" class="button">{{ trans('words.next') }}</a>
    </div>
@stop