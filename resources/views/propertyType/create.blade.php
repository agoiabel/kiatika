@extends("back_layout")
@section("content")

        
<!-- page start-->
<div align="right">
  <a href="{{ route('propertyType.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

@include('errors.list')
@include('flash')

<div class="panel panel-default">
    <div class="panel-body">

        {!! Form::open(['route' => 'propertyType.store','class'=>'form-horizontal padding-15','name'=>'type_form','id'=>'type_form','role'=>'form']) !!} 

            @include('propertyType._form', ['btn' => 'Add new'])

        {!! Form::close() !!} 

    </div>
</div>
   

@endsection