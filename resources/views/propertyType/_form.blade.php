<div class="form-group">
    <label for="" class="col-sm-3 control-label">Property Type</label>
      <div class="col-sm-9">
        {!! Form::text('type', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-sm-9 ">
    	<button type="submit" class="btn btn-primary">{{ $btn }}</button>                         
    </div>
</div>