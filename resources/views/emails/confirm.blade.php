<!DOCTYPE html>
<html>
<head>
	<title>Sign up confirmation</title>
</head>
<body>
	<h1>Thanks for signing up!</h1>

	<p>
		We just need you to <a href='{{ url("auth/register/confirm/{$user->confirmation_code}") }}'>confirm your email address</a> real quick!  
	</p>
</body>
</html>