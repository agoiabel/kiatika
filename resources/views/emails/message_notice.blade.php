Hi, You have a new message from {{ $msg->sender->name }}

Below is the message content:
{{ $msg->body }}

<a href='{{ url("message/inbox-details/{$receivedMessage->slug}") }}'>
	Click to reply the message
</a>
