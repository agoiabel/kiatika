<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{!! getcong('header_title') !!}</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="/css/{{ getcong('color_picker') }}">
    @yield('header')
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      @include('partials.customer_backend_header')
      <!--header end-->

      <!--main content start-->
          <section class="wrapper">
              

              <!--mail inbox start-->
              <div class="mail-box">
                  <aside class="sm-side">
                      <div class="user-head">
                          <a href="javascript:;" class="inbox-avatar">
                              <img src="img/mail-avatar.jpg" alt="">
                          </a>
                          <div class="user-name">
                              <h5><a href="#">{{ $currentUser->name }}</a></h5>
                              <span><a href="#">{{ $currentUser->email }}</a></span>
                          </div>
                          <a href="javascript:;" class="mail-dropdown pull-right">
                              <i class="fa fa-chevron-down"></i>
                          </a>
                      </div>

                      <ul class="inbox-nav inbox-divider">
                          <li class="active">
                              <a href="#">
                                <i class="fa fa-inbox"> </i> Inbox 
                                <span class="label label-danger pull-right">2</span>
                              </a>
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-envelope-o"></i> Sent Mail</a>
                          </li>
                      </ul>
                  </aside>
                  <aside class="lg-side">
                      <div class="inbox-head">
                          <h3>View Mail</h3>
                          <form class="pull-right position" action="#">
                              <div class="input-append">
                                  <input type="text"  placeholder="Search Mail" class="sr-input">
                                  <button type="button" class="btn sr-btn"><i class="fa fa-search"></i></button>
                              </div>
                          </form>
                      </div>
                      <div class="inbox-body">
                              <div class="heading-inbox row">
                                  <div class="col-md-8">
                                      <div class="compose-btn">
                                          <a class="btn btn-sm btn-primary" href="#">
                                            <i class="fa fa-reply"></i> Reply
                                          </a>
                                      </div>
                                  </div>
                                  <div class="col-md-4 text-right">
                                      <p class="date"> Message created_at </p>
                                  </div>
                                  <div class="col-md-12">
                                      <h4>Message Subject</h4>
                                  </div>
                              </div>
                              <div class="sender-info">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <img alt="" src="img/mail-avatar.jpg">
                                          <strong>Message sender name</strong>
                                          <span>message_sender_email</span>
                                          to
                                          <strong>me</strong>
                                          <a class="sender-dropdown " href="javascript:;">
                                              <i class="fa fa-chevron-down"></i>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                              <div class="view-mail">
                                  <p>Message body</p>
                              </div>

                              <div class="compose-btn pull-left">
                                  <a class="btn btn-sm btn-primary" href="#">
                                    <i class="fa fa-reply"></i> Reply
                                  </a>
                              </div>
                          </div>
                  </aside>
              </div>
              <!--mail inbox end-->



          </section>

  </section>

  <script src="/js/backend.js"></script>
  @yield('footer')
  
  </body>

</html>
