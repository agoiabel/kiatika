@extends("back_layout")
@section("content")

        
<!-- page start-->
<div align="right">
  <a href="{{ route('client.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

@include('errors.list')
@include('flash')

<div class="panel panel-default">
    <div class="panel-body">

            {!! Form::open(['route' => 'client.store','class'=>'form-horizontal padding-15','name'=>'type_form','id'=>'type_form','role'=>'form', 'files'=>true]) !!} 
            
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Feature name</label>
                      <div class="col-sm-9">
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Feature Image</label>
                      <div class="col-sm-9">
                        {!! Form::file('image', ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-9 ">
                        <button type="submit" class="btn btn-primary">Create</button>                         
                    </div>
                </div>            
            {!! Form::close() !!} 
        </div>
    </div>
</div>

@endsection

