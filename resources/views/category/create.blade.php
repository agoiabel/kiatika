@extends("back_layout")
@section("content")

        
<!-- page start-->
<div align="right">
    <a href="{{ route('blog.index') }}" class="btn btn-primary"><i class="fa fa-folder-o" aria-hidden="true"></i> View Blog</a>
</div>
<hr>

<div class="panel panel-default">
    <div class="panel-body">
            @include('errors.list')
            @include('flash')
            {!! Form::open(['route' => 'category.store', 'class'=>'form-horizontal padding-15', 'enctype' => 'multipart/form-data']) !!} 

                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Category Name</label>
                      <div class="col-sm-9">
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-9 ">
                        <button type="submit" class="btn btn-primary">Create</button>                         
                    </div>
                </div>

            {!! Form::close() !!} 
        </div>
    </div>
</div>

@endsection

