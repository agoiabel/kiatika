@extends("back_layout")

@section('header')
    <link rel="stylesheet" type="text/css" href="/css/vendor/select2.min.css">
@endsection

@section("content")

        
<!-- page start-->
<div align="right">
    <a href="{{ route('blog.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

<div class="panel panel-default">
    <div class="panel-body">
            @include('errors.list')
            @include('flash')
            {!! Form::open(['route' => 'blog.store', 'class'=>'form-horizontal padding-15', 'enctype' => 'multipart/form-data']) !!} 

                @include('blog._form', ['btn' => 'Post'])    

            {!! Form::close() !!} 
        </div>
    </div>
</div>

@endsection

@section('footer')
    <script type="text/javascript" src="/js/vendor/select2.min.js"></script>
    <script type="text/javascript">

            $('#input-file').fileinput({
                'showUpload': false
            });    

            $('#category').select2({
                placeholder: 'Choose Blog Category',
                tags: true,
            });

            $('#tag').select2({
                placeholder: 'Choose Blog Tag',
                tags: true,
            });

    </script>
@endsection
