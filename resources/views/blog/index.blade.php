@extends("back_layout")

@section('header')
  <link rel="stylesheet" type="text/css" href="/css/vendor/jquery.dataTables.min.css">
@endsection

@section("content")
<div class="clearfix pull-right">
  <a href="{{ route('blog.create') }}" class="btn btn-primary"><i class="fa fa-plus"> </i> Create New Blog</a>
  <a href="{{ route('category.index') }}" class="btn btn-primary"><i class="fa fa-folder-o" aria-hidden="true"></i> View Category</a>
	<a href="{{ route('tag.index') }}" class="btn btn-primary"><i class="fa fa-folder-o" aria-hidden="true"></i> View Tag</a>
</div>
<hr>

@include('flash')

<div class="row">
    <div class="col-sm-12">
        
      <div class="row">
          <div class="col-lg-12">
              <section class="panel">

                <table class="table table-striped table-bordered datatable-responsive">
                    <thead>
                      <tr>
                          <th>Category</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($blogs as $blog)
                        <tr>
                            <td>{{ $blog->category->name }}</td>
                            <td>{{ $blog->title }}</td>
                            <td>{{ $blog->description }}</td>
                            <td>
                              <a href="{{ route('blog.destroy', $blog->id) }}" class="btn btn-danger btn-xs btn-circle"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>

              </section>
          </div>
      </div>

    </div>
</div>

@endsection


@section('footer')
  <script type="text/javascript" src="/js/vendor/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $('.datatable-responsive').DataTable({
      select: true,
    })
  </script>
@endsection