<input type="hidden" name="authur_id" value="{{ $currentUser->id }}">

<div class="form-group">
    <label for="" class="col-sm-3 control-label">Category</label>
      <div class="col-sm-9">
        <select name="category" id="category" class="form-control" required>         
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
      </div>
</div>

<div class="form-group">
    <label for="" class="col-sm-3 control-label">Title</label>
      <div class="col-sm-9">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="" class="col-sm-3 control-label">Description</label>
      <div class="col-sm-9">
        <textarea name="description" class="form-control" rows="5"></textarea>
    </div>
</div>

<div class="form-group">
    <label for="" class="col-sm-3 control-label">Image</label>
      <div class="col-sm-9">
        <input type="file" id="input-file" name="image">
    </div>
</div>

<div class="form-group">
    <label for="" class="col-sm-3 control-label">Tag</label>
      <div class="col-sm-9">
        <select name="tag[]" id="tag" class="form-control" multiple required>         
            @foreach ($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
      </div>
</div>


<div class="form-group">
    <label for="" class="col-sm-3 control-label">Published At</label>
      <div class="col-sm-9">
        {!! Form::input('date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-sm-9 ">
        <button type="submit" class="btn btn-primary">{{ $btn }}</button>                         
    </div>
</div>            
