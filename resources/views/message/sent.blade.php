@extends('message.index', ['groupAction' => 'message._sentGroupAction'])

@section('page_header')
	Sent
@endsection

@section('message_view')
	
	@include('message._sent')

@endsection

@section('footer')
<script>

	$("#deleteGroup").on('click', function (e) {

	  e.preventDefault();

	  $("#action").val("1");

	  $("#messageGroupAction").trigger('submit');

	});
</script>
@endsection