		<form method="POST" action="{{ route('messageGroupAction') }}" id="messageGroupAction">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="action" value="" id="action">
			<input type="hidden" name="message_type" value="{{ 'sent' }}">

			@forelse ($sentMessages as $sentMessage)
			  
			<tr class="{{ readStatus($sentMessage->is_read) }}">
				<td class="inbox-small-cells">
				  <input type="checkbox" class="mail-checkbox" name="message_ids[]" value="{{ $sentMessage->id }}">
				</td>
				<td class="view-message  dont-show">{{ $sentMessage->message->receiver->name }} </td>
				<td class="view-message ">{{ $sentMessage->message->subject }}</td>
				<td class="view-message ">{{ $sentMessage->message->created_at }}</td>
				<td class="view-message  inbox-small-cells">
					<a href="{{ route('message.destroy', [$sentMessage->slug, 'sent']) }}" class="btn btn-danger btn-xs btn-circle"><i class="fa fa-trash-o"></i></a>
				</td>
			</tr>

			@empty
				<tr>
					<td colspan="5"><div align="center">You have not sent any message yet</div></td>
				</tr>
			@endforelse

		</form>

        </tbody>
      </table>
      {!! $sentMessages->render() !!}
</div>