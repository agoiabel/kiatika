@extends('message.index', ['groupAction' => 'message._trashGroupAction'])

@section('page_header')
  Trash
@endsection

@section('message_view')
  
  @include('message._trash')

@endsection

@section('footer')
<script>

	$("#restoreGroup").on('click', function (e) {

	  e.preventDefault();

	  $("#action").val("3");

	  $("#messageGroupAction").trigger('submit');

	});

</script>
@endsection