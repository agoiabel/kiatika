<ul class="inbox-nav inbox-divider">
    <li class="active">
        <a href="{{ route('message.index') }}"><i class="fa fa-inbox"></i> Inbox 
                
            @if ( $recievedMessageCount )
                <span class="label label-danger pull-right">{{ $recievedMessageCount }}</span>
            @endif
        
        </a>
    </li>
    <li>
        <a href="{{ route('message.sent') }}"><i class="fa fa-envelope-o"></i> Sent Mail</a>
    </li>
    <li>
        <a href="{{ route('message.trash') }}"><i class=" fa fa-trash-o"></i> Trash</a>
    </li>
</ul>
