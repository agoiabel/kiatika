<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Agoi abel adeyemi">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{!! getcong('header_title') !!}</title>

    <link href="/css/backend.css" rel="stylesheet">
    @yield('header')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      @include('partials.customer_backend_header')
      <!--header end-->

      <!--main content start-->
          <section class="wrapper">
              

              <!--mail inbox start-->
              <div class="mail-box">
                  <aside class="sm-side">
                      <div class="user-head">
                          <a href="javascript:;" class="inbox-avatar">
                              <img src="{{ $receivedMessage->message->sender->profile->image_icon }}" alt="">
                          </a>
                          <div class="user-name">
                              <h5><a href="#">{{ $currentUser->name }}</a></h5>
                              <span><a href="#">{{ $currentUser->email }}</a></span>
                          </div>
                          <a href="javascript:;" class="mail-dropdown pull-right">
                              <i class="fa fa-chevron-down"></i>
                          </a>
                      </div>

                      <ul class="inbox-nav inbox-divider">
                          <li class="active">
                              <a href="#">
                                <i class="fa fa-inbox"> </i> Inbox 
                                <span class="label label-danger pull-right">2</span>
                              </a>
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-envelope-o"></i> Sent Mail</a>
                          </li>
                      </ul>
                  </aside>
                  
                  @include('message._reply')

              </div>
              <!--mail inbox end-->



          </section>

      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              {!! getcong('copy_right') !!}
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

  <script src="/js/backend.js"></script>
  <script src="/js/vendor/ajax.js"></script>
  <script type="text/javascript">
      $(".reply").hide();

      $(".open-reply").on('click', function (e) {      
          e.preventDefault();
          $(".reply").show();
          $(".message_body").focus();
      });
  </script>
  
  </body>

</html>


