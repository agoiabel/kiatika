@extends('back_layout')
@section('content')

<!--mail inbox start-->
@include('flash')
<div class="mail-box">
  <aside class="sm-side">
      <div class="user-head">
          <a href="javascript:;" class="inbox-avatar">
              <img src="/{{ $currentUser->profile->image_icon }}" width="60" height="60">
          </a>
          <div class="user-name">
              <h5><a href="#">{{ $currentUser->name }}</a></h5>
              <span><a href="#">{{ $currentUser->email }}</a></span>
          </div>
          <a href="javascript:;" class="mail-dropdown pull-right">
              <i class="fa fa-chevron-down"></i>
          </a>
      </div>
      @if ( $currentUser->isAdmin() )

        @include('message._create')

      @endif

      @include('message._sideMenu')
  </aside>

  @include('message._reply')

</div>
<!--mail inbox end-->


@endsection


@section('footer')
  <script src="/js/vendor/ajax.js"></script>
  <script type="text/javascript" src="/js/vendor/message_ajax.js"></script>
  <script type="text/javascript">
      $(".reply").hide();

      $(".open-reply").on('click', function (e) {      
          e.preventDefault();
          $(".reply").show();
          $(".message_body").focus();
      });
  </script>
@endsection