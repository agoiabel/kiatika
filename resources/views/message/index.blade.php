<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{{ getcong('title') }}</title>

    <link href="/css/backend.css" rel="stylesheet">
    @yield('header')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      @include('partials.backend_header')
      <!--header end-->

      <!--sidebar start-->
      @include('partials.backend_sidebar')
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

            <!--mail inbox start-->
            @include('flash')
            <div class="mail-box">
              <aside class="sm-side">
                  <div class="user-head">
                      <a href="javascript:;" class="inbox-avatar">
                          <img src="/{{ $currentUser->profile->image_icon }}" width="60" height="60">
                      </a>
                      <div class="user-name">
                          <h5><a href="#">{{ $currentUser->name }}</a></h5>
                          <span><a href="#">{{ $currentUser->email }}</a></span>
                      </div>
                      <a href="javascript:;" class="mail-dropdown pull-right">
                          <i class="fa fa-chevron-down"></i>
                      </a>
                  </div>
                  @if ( $currentUser->isAdmin() )

                    @include('message._create')

                  @endif

                  @include('message._sideMenu')

              </aside>

              <aside class="lg-side">
                  <div class="inbox-head">
                      <h3>@yield('page_header')</h3>
                      <form class="pull-right position">
                          <div class="input-append">
                              <input type="text"  placeholder="Search Mail" class="sr-input">
                              <button type="button" class="btn sr-btn"><i class="fa fa-search"></i></button>
                          </div>
                      </form>
                  </div>
                  <div class="inbox-body">
                     <div class="mail-option">

                         <div class="btn-group">
                             <a class="btn mini blue" href="#" data-toggle="dropdown">
                                 Action<i class="fa fa-angle-down "></i>
                             </a>
                             @include($groupAction)
                         </div>

                     </div>
                      <table class="table table-inbox table-hover">
                        <tbody>
                            <tr>
                              <th></th>
                              <th>Sender name</th>
                              <th>subject</th>
                              <th>Date/Time</th>
                              <th align="center">action</th>
                            </tr>
                            @yield('message_view')

              </aside>
            </div>
            <!--mail inbox end-->

          </section>
      </section>


  </section>

  <script src="/js/backend.js"></script>
  <script type="text/javascript" src="/js/vendor/message_ajax.js"></script>
  @yield('footer')

  
  </body>

</html>
