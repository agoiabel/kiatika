<div class="inbox-body">
    <a class="btn btn-compose" data-toggle="modal" href="#myModal">
        Compose
    </a>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Compose</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="messageForm" action="{{ route('message.send') }}" enctype="multipart/form-data">

                    	  <input type="hidden" name="_token" value="{{csrf_token()}}">
                        	
                        <div class="form-group">
                            <label  class="col-lg-2 control-label">To</label>
                            <div class="col-lg-10">
                            	  <select name="recipient" class="form-control">
                            	  	<option value="a">all-agents</option>
                            	  	<option value="b">all-customers</option>
                                  @foreach ($agents as $agent)
                                  <option value="{{ $agent->id }}">{{ $agent->name }} | {{ $agent->email }}</option>
                                  @endforeach
                            	  </select>
                            </div>
                        </div>
                        <div class="form-group">    
                            <label class="col-lg-2 control-label">Subject</label>
                            <div class="col-lg-10">
                                <input type="text" name="subject" class="form-control" id="inputPassword1" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Message</label>
                            <div class="col-lg-10">
                                <textarea name="body" id="" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-send">Send</button>
                                @include('flash')
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>