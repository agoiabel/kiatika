@extends('message.index', ['groupAction' => 'message._inboxGroupAction'])

@section('page_header')
	Inbox
@endsection

@section('message_view')
	
	@include('message._inbox')

@endsection

@section('footer')
<script>

	$("#deleteGroup").on('click', function (e) {

	  e.preventDefault();

	  $("#action").val("1");

	  $("#messageGroupAction").trigger('submit');

	});

	$("#readGroup").on('click', function (e) {

	  e.preventDefault();

	  $("#action").val("0");

	  $("#messageGroupAction").trigger('submit');

	}); 
</script>
@endsection