<form method="POST" action="{{ route('messageGroupAction') }}" id="messageGroupAction">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="action" value="" id="action">
	<input type="hidden" name="message_type" value="{{ 'restore' }}">

	@forelse ($recievedMessages as $recievedMessage)
	  
	<tr class="{{ readStatus($recievedMessage->is_read) }}">
		<td class="inbox-small-cells">
		  <input type="checkbox" class="mail-checkbox" name="message_ids[]" value="{{ $recievedMessage->id }}">
		</td>
		<td class="view-message  dont-show">{{ $recievedMessage->receiver->name }} </td>
		<td class="view-message ">{{ $recievedMessage->message->subject }}</td>
		<td class="view-message ">{{ $recievedMessage->message->created_at }}</td>
		<td class="view-message  inbox-small-cells">
			<a href="{{ route('message.restore', $recievedMessage->slug) }}" class="btn btn-success btn-xs btn-circle"><i class="fa fa-level-up"></i></a>
		</td>
	</tr>

	@empty
		<tr>
			<td colspan="5"><div align="center">You have no trashed message yet</div></td>
		</tr>
	@endforelse

		</form>

        </tbody>
      </table>
	{!! $recievedMessages->render() !!}
</div>


