<aside class="lg-side">
    <div class="inbox-head">
        <h3>View Mail</h3>
        <form class="pull-right position" action="#">
            <div class="input-append">
                <input type="text"  placeholder="Search Mail" class="sr-input">
                <button type="button" class="btn sr-btn"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
    <div class="inbox-body">
        <div class="heading-inbox row">
            <div class="col-md-8">
                <div class="compose-btn">
                    <a class="btn btn-sm btn-primary open-reply" href="#">
                      <i class="fa fa-reply"></i> Reply
                    </a>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <p class="date">
                  {{ $receivedMessage->message->created_at }} 
                 </p>
            </div>
            <div class="col-md-12">
                <h4>
                  {{ $receivedMessage->message->subject }}
                </h4>
            </div>
        </div>
        <div class="sender-info">
            <div class="row">
                <div class="col-md-12">
                    <img alt="" src="img/mail-avatar.jpg">
                    <strong>
                      {{ $receivedMessage->message->sender->name }}
                    </strong>
                    <span>{{ $receivedMessage->message->sender->email }}</span>
                    to
                    <strong>me</strong>
                    <a class="sender-dropdown " href="javascript:;">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="view-mail">
          <p>{{ $receivedMessage->message->body }}</p>
        </div>
        <div class="compose-btn pull-left">
            <a class="btn btn-sm btn-primary open-reply" href="#">
              <i class="fa fa-reply"></i> Reply
            </a>
        </div>
        <div class="reply">
          <br><br><hr>
          @include('errors.list')
          @include('flash')
          {!! Form::open(['data-remote', 'data-remote-success-message' => 'message sent successfully', 'route' => 'message.reply']) !!}

            <input type="hidden" name="receiver_id" value="{{ $receivedMessage->message->sender_id }}">
            <input type="hidden" name="subject" value="{{ $receivedMessage->message->subject }}">

            <div class="form-group">
              <textarea name="body" class="form-control message_body" rows="5"></textarea>            
            </div>

            <div class="form-group pull-right">
              <button type="submit" class="btn btn-sm btn-primary">send</button>
            </div>
            
          {!! Form::close() !!}
        </div>
    </div>
</aside>