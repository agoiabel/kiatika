<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="agoi abel">
    <meta name="keyword" content="property listing, codecanyon, property listing script">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{{ getcong('header_title') }}</title>

    <link href="/css/backend.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/{{ getcong('color_picker') }}">

    @yield('header')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      @include('partials.backend_header')
      <!--header end-->

      <!--sidebar start-->
      @include('partials.backend_sidebar')
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              @yield('content')
          </section>
      </section>

  </section>

  <script src="/js/backend.js"></script>
  @yield('footer')
  
  </body>

</html>
