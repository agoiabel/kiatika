@extends("back_layout")

@section('header')
	<script type="text/javascript" src="/css/vendor/fontawesome-iconpicker.css"></script>
@endsection

@section("content")

        
<!-- page start-->
<div align="right">
  <a href="{{ route('propertyFeature.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

@include('errors.list')
@include('flash')

<div class="panel panel-default">
    <div class="panel-body">

        {!! Form::open(['route' => 'propertyFeature.store','class'=>'form-horizontal padding-15','name'=>'type_form','id'=>'type_form','role'=>'form']) !!} 

            @include('feature._form', ['btn' => 'Add new'])

        {!! Form::close() !!} 

    </div>
</div>
   

@endsection

@section('footer')
	<script type="text/javascript" src="/js/vendor/fontawesome-iconpicker.js"></script>
	<script type="text/javascript">
		$('.icon-picker').iconpicker();
	</script>
@endsection