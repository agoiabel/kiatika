@extends("back_layout")
@section("content")
        
<!-- page start-->
<div align="right">
  <a href="{{ route('propertyFeature.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

@include('errors.list')
@include('flash')

<div class="panel panel-default">
    <div class="panel-body">

        {!! Form::model($propertyFeature, ['route' => 'propertyFeature.update','class'=>'form-horizontal padding-15','name'=>'type_form','id'=>'type_form','role'=>'form']) !!} 
            <input type="hidden" name="slug" value="{{ $propertyFeature->slug }}">
            @include('feature._form', ['btn' => 'Update'])

        {!! Form::close() !!} 

    </div>
</div>
   

@endsection