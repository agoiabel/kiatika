@extends("back_layout")
@section('header')
  <link rel="stylesheet" type="text/css" href="/css/vendor/jquery.dataTables.min.css">
@endsection

@section("content")
<div class="pull-right">
	<a href="{{ route('propertyFeature.create') }}" class="btn btn-primary"><i class="fa fa-plus"> Add New</i></a>
</div>
<hr>

@include('flash')

<div class="row">
    <div class="col-sm-12">
        
      <div class="row">
          <div class="col-lg-12">
              <section class="panel">

                <table class="table table-striped table-bordered datatable-responsive">
                    <thead>
                      <tr>
                          <th>Name</th>
                          <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($features as $feature)
                        <tr>
                            <td>{{ $feature->name }}</td>
                            <td>
                            	<a href="{{ route('propertyFeature.edit', $feature->slug) }}" class="btn btn-primary btn-xs btn-circle"><i class="fa fa-pencil"></i></a>
                            	<a href="{{ route('propertyFeature.destroy', $feature->slug) }}" class="btn btn-danger btn-xs btn-circle"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>

              </section>
          </div>
      </div>

    </div>
</div>

@endsection


@section('footer')
  <script type="text/javascript" src="/js/vendor/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $('.datatable-responsive').DataTable({
      select: true,
    })
  </script>
@endsection



