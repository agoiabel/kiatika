@extends('back_layout')
@section('content')

<ul class="directory-list">
  <li><a href="{{ route('users.search', 'a') }}">a</a></li>
  <li><a href="{{ route('users.search', 'b') }}">b</a></li>
  <li><a href="{{ route('users.search', 'c') }}">c</a></li>
  <li><a href="{{ route('users.search', 'd') }}">d</a></li>
  <li><a href="{{ route('users.search', 'e') }}">e</a></li>
  <li><a href="{{ route('users.search', 'f') }}">f</a></li>
  <li><a href="{{ route('users.search', 'g') }}">g</a></li>
  <li><a href="{{ route('users.search', 'h') }}">h</a></li>
  <li><a href="{{ route('users.search', 'i') }}">i</a></li>
  <li><a href="{{ route('users.search', 'j') }}">j</a></li>
  <li><a href="{{ route('users.search', 'k') }}">k</a></li>
  <li><a href="{{ route('users.search', 'l') }}">l</a></li>
  <li><a href="{{ route('users.search', 'm') }}">m</a></li>
  <li><a href="{{ route('users.search', 'n') }}">n</a></li>
  <li><a href="{{ route('users.search', 'o') }}">o</a></li>
  <li><a href="{{ route('users.search', 'p') }}">p</a></li>
  <li><a href="{{ route('users.search', 'q') }}">q</a></li>
  <li><a href="{{ route('users.search', 'r') }}">r</a></li>
  <li><a href="{{ route('users.search', 's') }}">s</a></li>
  <li><a href="{{ route('users.search', 't') }}">t</a></li>
  <li><a href="{{ route('users.search', 'u') }}">u</a></li>
  <li><a href="{{ route('users.search', 'v') }}">v</a></li>
  <li><a href="{{ route('users.search', 'w') }}">w</a></li>
  <li><a href="{{ route('users.search', 'x') }}">x</a></li>
  <li><a href="{{ route('users.search', 'y') }}">y</a></li>
  <li><a href="{{ route('users.search', 'z') }}">z</a></li>
</ul>	
<div class="directory-info-row">
	@foreach ($users->chunk(2) as $set)
		<div class="row">

			@foreach ($set as $user)
				<div class="col-md-6 col-sm-6">
					<div class="panel">
					  <div class="panel-body">
					      <div class="media">
					          <a class="pull-left" href="#">
					              <img class="thumb media-object" src="/{{ $user->profile->image_icon }}" alt="">
					          </a>
					          <div class="media-body">
					              <h4>{{ $user->name }} <span class="text-muted small"> - {{ $user->role->name }}</span></h4>
					              <abbr title="Phone"><strong>Phone no </strong></abbr> - {{ $user->profile->phone }}<br><br>
					              <address>
					                  <strong>City </strong> - {{ $user->city->city_name }}<br><br>
					                  <a href="#" class="btn btn-info">message user</a>
					              </address>
					          </div>
					      </div>
					  </div>
					</div>
				</div>
			@endforeach

		</div>
	@endforeach
</div>

{!! $users->render() !!}

@endsection