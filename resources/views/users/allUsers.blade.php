@extends('back_layout')
@section('content')

<ul class="directory-list">
  <li><a href="{{ route('users.search', 'a') }}">a</a></li>
  <li><a href="{{ route('users.search', 'b') }}">b</a></li>
  <li><a href="{{ route('users.search', 'c') }}">c</a></li>
  <li><a href="{{ route('users.search', 'd') }}">d</a></li>
  <li><a href="{{ route('users.search', 'e') }}">e</a></li>
  <li><a href="{{ route('users.search', 'f') }}">f</a></li>
  <li><a href="{{ route('users.search', 'g') }}">g</a></li>
  <li><a href="{{ route('users.search', 'h') }}">h</a></li>
  <li><a href="{{ route('users.search', 'i') }}">i</a></li>
  <li><a href="{{ route('users.search', 'j') }}">j</a></li>
  <li><a href="{{ route('users.search', 'k') }}">k</a></li>
  <li><a href="{{ route('users.search', 'l') }}">l</a></li>
  <li><a href="{{ route('users.search', 'm') }}">m</a></li>
  <li><a href="{{ route('users.search', 'n') }}">n</a></li>
  <li><a href="{{ route('users.search', 'o') }}">o</a></li>
  <li><a href="{{ route('users.search', 'p') }}">p</a></li>
  <li><a href="{{ route('users.search', 'q') }}">q</a></li>
  <li><a href="{{ route('users.search', 'r') }}">r</a></li>
  <li><a href="{{ route('users.search', 's') }}">s</a></li>
  <li><a href="{{ route('users.search', 't') }}">t</a></li>
  <li><a href="{{ route('users.search', 'u') }}">u</a></li>
  <li><a href="{{ route('users.search', 'v') }}">v</a></li>
  <li><a href="{{ route('users.search', 'w') }}">w</a></li>
  <li><a href="{{ route('users.search', 'x') }}">x</a></li>
  <li><a href="{{ route('users.search', 'y') }}">y</a></li>
  <li><a href="{{ route('users.search', 'z') }}">z</a></li>
</ul>	
<div class="directory-info-row">
	@forelse ($users->chunk(2) as $set)
		<div class="row">

			@foreach ($set as $user)
				<div class="col-md-6 col-sm-6">
					<div class="panel">
					  <div class="panel-body">
					      <div class="media">
					          <a class="pull-left" href="#">
					              <img class="thumb media-object" src="/{{ $user->profile->image_icon }}" alt="">
					          </a>
					          <div class="media-body">
					              <h4>{{ $user->name }} <span class="text-muted small"> - {{ $user->role->name }}</span></h4>
					              <abbr title="Phone"><strong>Phone no </strong></abbr> - {{ $user->profile->phone }}<br><br>
					              <address>
					                  <strong>City </strong> - {{ $user->city->city_name }}<br><br>
					                  <a href="#myModal" data-toggle="modal" class="btn btn-info messageUser" id="{{ $user->id }}" >message user</a>
					              </address>
					          </div>
					      </div>
					  </div>
					</div>
				</div>
			@endforeach

		</div>
	@empty
    <p align="center">No User With Name Yet</p>
  @endforelse 
</div>

{!! $users->render() !!}

<div class="inbox-body">
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Compose</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="messageForm" action="{{ route('message.send') }}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          
                        <div class="form-group">
                            <div class="col-lg-10">
                              <input type="hidden" name="recipient" value="" id="recipient">
                            </div>
                        </div>
                        <div class="form-group">    
                            <label class="col-lg-2 control-label">Subject</label>
                            <div class="col-lg-10">
                                <input type="text" name="subject" class="form-control" id="inputPassword1" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Message</label>
                            <div class="col-lg-10">
                                <textarea name="body" id="" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-send">Send</button>
                                @include('flash')
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
@endsection

@section('footer')
<script type="text/javascript" src="/js/vendor/message_ajax.js"></script>
<script type="text/javascript">
  $('.messageUser').on('click', function (e) {

    e.preventDefault();

    var recipient = $(this).attr("id");

    $('input[name="recipient"]').val(recipient);

  });
</script>
@endsection