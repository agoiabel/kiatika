@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => '404 page'])

    <section id="error-404">
        <div class="container">

            <div id="white-form-background">
                <div class="text-center">
                    <h2 class="lrg_titl">404</h2>
                    <h3 class="sub_titl">OOPS, YOU’VE ENCOUNTERED AN ERROR</h3>
                    <p class="description">
                        The page you are looking for no longer exists. Perhaps you can return back to the site's homepage and see if you can find what you are looking for.
                    </p>

                    <div class="spacer-30"></div>

                    <a href="{{ route('testing_index') }}" class="btn btn-primary"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> RETURN HOME</a>

                    <div class="spacer-30"></div>

                </div>
            </div>

        </div>
    </section>


@endsection

