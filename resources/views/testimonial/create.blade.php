@extends("back_layout")

@section('header')
    <link rel="stylesheet" type="text/css" href="/css/vendor/jasny-bootstrap.min.css">
@endsection


@section("content")
        
<!-- page start-->
<div align="right">
  <a href="{{ route('testimonial.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

@include('errors.list')
@include('flash')

<div class="panel panel-default">
    <div class="panel-body">

        {!! Form::open(['route' => 'testimonial.store','class'=>'form-horizontal padding-15','name'=>'account_form','id'=>'account_form','role'=>'form','files' => true]) !!}


    <div class="row">
        <div class="col-md-8">

            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Occupation</label>
                <div class="col-sm-9">
                    {!! Form::text('occupation', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Testimonial</label>
                <div class="col-sm-9">
                    {!! Form::textarea('testimony', null, ['class' => 'form-control']) !!}                        
                </div>
            </div>

        </div>
        <div class="col-md-4">
            
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="media">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="#" alt="Photo" style="width: 200px;">
                          </div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                          <div class="text-center">
                            <span class="btn btn-primary btn-file"><span class="fileinput-new">Choose Photo</span>
                            <span class="fileinput-exists">Change Photo</span><input type="file" name="image" required></span>
                          </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>


    <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save Changes <i class="md md-lock-open"></i></button>
        </div>
    </div>                     

        {!! Form::close() !!} 

    </div>
</div>
   

@endsection

@section('footer')
    <script src="/js/vendor/jasny-bootstrap.min.js"></script>
@endsection