@extends("back_layout")
@section("content")
        
<!-- page start-->
<div align="right">
  <a href="{{ route('city.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

@include('errors.list')
@include('flash')

<div class="panel panel-default">
    <div class="panel-body">

       {!! Form::model($city, ['route' => 'city.update', 'class'=>'form-horizontal padding-15','name'=>'city_form','id'=>'city_form','role'=>'form']) !!} 

            @include('city._form', ['btn' => 'update'])

        {!! Form::close() !!} 

    </div>
</div>
   

@endsection