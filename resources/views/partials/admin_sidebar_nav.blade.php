<li class="{{classActivePath('dashboard')}}"><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>

<li class="{{classActivePath('types')}}"><a href="{{ route('propertyType.index') }}"><i class="fa fa-tags"></i>Property Types</a></li>

<li class="{{classActivePath('features')}}"><a href="{{ route('propertyFeature.index') }}"><i class="fa fa-tags"></i>Property Features</a></li>

<li class="{{classActivePath('purposes')}}"><a href="{{ route('purpose.index') }}"><i class="fa fa-tags"></i>Property Purpose</a></li>

<li class="{{classActivePath('properties')}}"><a href="{{ route('upload.index') }}"><i class="md md-pin-drop"></i>Property</a></li>

<li class="{{classActivePath('featuredproperties')}}"><a href="{{ URL::to('admin/featuredproperties') }}"><i class="md md-star"></i>Featured</a></li>

<li class="{{classActivePath('inquiries')}}"><a href="{{ URL::to('admin/inquiries') }}"><i class="fa fa-send"></i>Inquiries</a></li> 

<li class="{{classActivePath('slider')}}"><a href="{{ URL::to('admin/slider') }}"><i class="fa fa-sliders"></i>Home Slider</a></li>

<li class="{{classActivePath('testimonials')}}"><a href="{{ route('testimonial.index') }}"><i class="fa fa-list"></i>Testimonials</a></li>

<li class="{{classActivePath('clients')}}"><a href="{{ route('client.index') }}"><i class="fa fa-bookmark-o"></i>Clients</a></li>

<li class="{{classActivePath('subscriber')}}"><a href="{{ URL::to('admin/subscriber') }}"><i class="md md-email"></i>Subscribers</a></li>

<li class="{{classActivePath('cities')}}"><a href="{{ route('city.index') }}"><i class="md md-location-city"></i>Cities</a></li>

<li class="{{classActivePath('users')}}"><a href="{{ URL::to('admin/users') }}"><i class="fa fa-users"></i>Users</a></li>

<li class="{{classActivePath('settings')}}"><a href="{{ route('site.settings') }}"><i class="md md-settings"></i>Settings</a></li>
