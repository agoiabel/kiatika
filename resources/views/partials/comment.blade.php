<div class="post-comment">
    <div class="row">
        <div class="col-md-11 col-md-offset-1">
            <h2>Post a comment</h2>
            <div class="comment-create">

                @include('errors.list')
                @include('flash')

                <form method="post" action="{{ route('comment.store') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="role_id" value="{{'3'}}">
                    <input type="hidden" name="blog_id" value="{{$blog->id}}">      
                              
                    <div class="form-group">
                        <label>Message</label>
                        <textarea name="comment" class="form-control" rows="5"></textarea>
                    </div><!-- /.form-group -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control">
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control">
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                                    
                    </div><!-- /.row -->

                    <div class="form-group-btn">
                        <button type="submit" class="btn btn-primary pull-right">Post Comment</button>
                    </div><!-- /.form-group-btn -->
                </form>

            </div>
        </div>
    </div>
</div>
