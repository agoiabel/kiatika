<div class="service-count">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="counter-box">


                        <div class="col-xs-3 text-center">
                            <div class="service-count-detial">
                                <i class="fa fa-heart-o"></i>
                            </div>
                            <h2 class="fun_num counter">{{ $satisfied_customers }}</h2>
                            <p class="desc">Satisfied Customers</p>
                        </div>

                        <div class="col-xs-3 text-center">
                            <div class="service-count-detial">
                                <i class="fa fa-user"></i>
                            </div>
                            <h2 class="fun_num counter">{{ $team_members }}</h2>
                            <p class="desc"> team members </p>
                        </div>

                        <div class="col-xs-3 text-center">
                            <div class="service-count-detial">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <h2 class="fun_num counter">{{ $all_properties }}</h2>
                            <p class="desc">All properties</p>
                        </div>

                        <div class="col-xs-3 text-center">
                            <div class="service-count-detial">
                                <i class="fa fa-thumbs-up"></i>
                            </div>
                            <h2 class="fun_num counter">{{ $featured_properties }}</h2>
                            <p class="desc">Featured Properties</p>
                        </div>


                </div>                
            </div>
        </div>
    </div>
</div>

