<section id="testimonial">
    <div class="container">
        <div class="row">
            
            <div class="col-md-12 big_title text-center">
                <h3 class="big_title">Testimonials <small>Hundred thousand happy customers over 5 year </small></h3>
            </div>
            
            <div class="spacer-10"></div>

            @foreach ($testimonials->chunk(2) as $set)
            	@foreach ($set as $testimonial)
	                <div class="col-md-6">
	                    <div class="testim_box">
	                        <blockquote>
	                            {{ $testimonial->testimony }}
	                        </blockquote>
	                        <div class="auth_sec">
	                            <img src="/{{ $testimonial->image }}" alt="">
	                            <h6 class="auth_nam">
	                                {{ $testimonial->name }}
	                                <span class="auth_pos">
	                                {{ $testimonial->occupation }}
	                            </span>
	                            </h6>
	                        </div>
	                    </div>
	                </div>
            	@endforeach
            	<div class="spacer-10"></div>
            @endforeach

        </div>
    </div>
</section>