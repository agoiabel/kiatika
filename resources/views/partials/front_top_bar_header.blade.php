    <header>
        <!-- top header -->
        <div class="header-wrapper custom-blue">
            <div class="container">
                <div id="header-social">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                
                <div id="language-switcher">
                    <i class="fa fa-user" aria-hidden="true"></i> <a href="#">LOGIN</a> / <a href="#">SIGN UP</a>
                    
                    <a href="#"><span class="flag-icon flag-icon-fr"></span></a>
                    <a href="#"><span class="flag-icon flag-icon-es"></span></a>
                    <a href="#"><span class="flag-icon flag-icon-pg"></span></a>
                    <a href="#"><span class="flag-icon flag-icon-sh"></span></a>
                    <a href="#"><span class="flag-icon flag-icon-ro"></span></a>
                </div>

            </div>
        </div>
        <!-- end of top header -->

        <!-- middle header -->
        <div id="header-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12 col-sm-8">
                        <div id="logo"><img src="images/logo.png"></div>
                    </div>

                    <div class="col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-xs-6 col-sm-6">
                                <div class="header-information">
                                    <i class="fa fa-phone"></i>
                                    <div class="header-information-block">
                                        <strong>+234-903-386-3912</strong>
                                        <span>Call Us Monday To Friday</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 col-sm-6">
                                <div class="header-information">
                                    <i class="fa fa-at"></i>
                                    <div class="header-information-block">
                                        <strong>info@kiatika.com</strong>
                                        <span>Ask Us Anything</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end middle header -->

        <!-- navbar -->
        <nav class="main-nav custom-light-blue" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button><!--//nav-toggle-->
                </div><!--//navbar-header-->            
                <div class="navbar-collapse collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active nav-item"><a href="index.html">Home</a></li>
                        <li class="nav-item"><a href="property-list.html">Property List</a></li>
                        <li class="nav-item"><a href="agents.html">Agents</a></li>
                        <li class="nav-item"><a href="contact-us.html">Contact</a></li>
                        <li class="nav-item"><a href="about-us.html">About Us</a></li>
                        <li class="nav-item"><a href="login.html">Sign In</a></li>
                        <li class="nav-item"><a href="register.html">Sign Up</a></li>
                        <li class="nav-item"><a href="property-single.html">Single Property</a></li>
                        <li class="nav-item"><a href="404.html">404</a></li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" 
                               data-toggle="dropdown" 
                               data-hover="dropdown" 
                               data-delay="0" 
                               data-close-others="false" 
                               href="#">dropdown <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html">dropdown 001</a></li>
                                <li><a href="index.html">dropdown 002</a></li>   
                                <li><a href="index.html">dropdown 003</a></li>          
                            </ul>
                        </li>
                    </ul><!--//nav-->
                </div><!--//navabr-collapse-->
            </div><!--//container-->
        </nav>
        <!-- end of navbar -->
    </header>

    <div class="spacer-30"></div>