<div id="owl-three-slider" class="owl-carousel owl-theme">
    @foreach($latest_featured_properties as $property)
        <div> 
            <div id="property-item">
                <div class="panel panel-default">
                    <div class="panel-image">
                        <div class="property-type" href="#">{{ $property->type->type }}</div>

                        <img class="img-responsive img-hover" src="/{{ $property->images[0]->property_homepage_path }}" alt="">
                        <div class="img_hov_eff">
                            <a class="btn btn-default btn_trans" href="{{ route('property_detial',$property->slug) }}"> More Detials </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <h4><a href="#">{{ $property->address }}</a></h4>
                        <p>{{ $property->city->city_name }}</p>

                        <div id="property-divider"></div>
                        <p id="property-purpose" class="pull-left">{{ $property->purpose->name }}</p>
                        <p id="property-price" class="pull-right"> {{ getcong('currency_sign') }} {{ $property->price }}</p>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>  
