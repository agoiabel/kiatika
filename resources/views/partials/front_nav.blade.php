    <header>
        <!-- top header -->
        <div class="header-wrapper custom-blue">
            <div class="container">
                <div id="header-social">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                
                <div id="language-switcher">
                    <i class="fa fa-user" aria-hidden="true"></i> <a href="{{ route('auth.login') }}">{{ trans('words.login') }}</a> / <a href="{{ route('auth.register') }}">{{ trans('words.signup') }}</a>
                    
                    <a href="{{ route('change-local', ['slug' => 'fr']) }}"><span class="flag-icon flag-icon-fr"></span></a>
                    <a href="{{ route('change-local', ['slug' => 'es']) }}"><span class="flag-icon flag-icon-es"></span></a>
                    <a href="{{ route('change-local', ['slug' => 'pt']) }}"><span class="flag-icon flag-icon-pg"></span></a>
                    <a href="{{ route('change-local', ['slug' => 'sh']) }}"><span class="flag-icon flag-icon-sh"></span></a>
                    <a href="{{ route('change-local', ['slug' => 'ro']) }}"><span class="flag-icon flag-icon-ro"></span></a>
                </div>

            </div>
        </div>
        <!-- end of top header -->

        <!-- middle header -->
        <div id="header-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12 col-sm-8">
                        <div id="logo"><img src="{{ url(getcong('logo_image_link')) }}"></div>
                    </div>

                    <div class="col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-xs-6 col-sm-6">
                                <div class="header-information">
                                    <i class="fa fa-phone"></i>
                                    <div class="header-information-block">
                                        <strong>{{ getcong('phone_number') }}</strong>
                                        <span>{{ trans('words.call_us_monday_to_friday') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 col-sm-6">
                                <div class="header-information">
                                    <i class="fa fa-at" aria-hidden="true"></i>
                                    <div class="header-information-block">
                                        <strong>{{ getcong('email') }}</strong>
                                        <span>{{ trans('words.ask_us_anything') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end middle header -->

        <!-- navbar -->
        <nav class="main-nav navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button><!--//nav-toggle-->
                </div><!--//navbar-header-->            
                <div class="navbar-collapse collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        
                        <li class="nav-item {{classActivePublicPath('')}}"><a href="{{ route('testing_index') }}">{{ trans('words.home') }}</a></li>
                        <li class="nav-item {{classActivePublicPath('propertylist')}}"><a href="{{ route('propertylist') }}">{{ trans('words.property_list') }}</a></li>
                        <li class="nav-item {{classActivePublicPath('feature-propertylist')}}"><a href="{{ route('feature-propertylist') }}">{{ trans('words.featured_property') }}</a></li>

                        <li class="nav-item {{classActivePublicPath('agent')}}"><a href="{{ route('agent') }}">{{ trans('words.agents') }}</a></li>
                        <li class="nav-item {{classActivePublicPath('blog')}}"><a href="{{ route('blog') }}">Blog</a></li>
                        <li class="nav-item {{classActivePublicPath('services')}}"><a href="{{ route('services') }}">Services</a></li>
                        <li class="nav-item {{classActivePublicPath('about-us')}}"><a href="{{ route('about-us') }}">{{ trans('words.about-us') }}</a></li>
                        <li class="nav-item {{classActivePublicPath('contact')}}"><a href="{{ route('contact') }}">{{ trans('words.contact') }}</a></li>
                        <li class="nav-item {{classActivePublicPath('faq')}}"><a href="{{ route('faq') }}">faq</a></li>

                    </ul><!--//nav-->
                </div><!--//navabr-collapse-->
            </div><!--//container-->
        </nav>
        <!-- end of navbar -->
    </header>
