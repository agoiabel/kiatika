<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a class="active" href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('words.dashboard') }}</span>
                </a>
            </li>

            <li>
                <a  href="{{ route('upload.index') }}">
                    <i class="fa fa-map-marker"></i>
                    <i class="fa fa-subway" aria-hidden="true"></i>
                    <span>{{ trans('words.property') }}</span>
                </a>
            </li>

            <li>
                <a  href="{{ route('message.index') }}">
                    <i class="fa fa-envelope"></i>
                    <span>{{ trans('words.message') }}</span>
                </a>
            </li>

            <li>
                <a  href="{{ route('users') }}">
                    <i class="fa fa-user"></i>
                    <span>{{ trans('words.user') }}</span>
                </a>
            </li>

            <li>
                <a  href="{{ route('blog.index') }}">
                    <i class="fa fa-envelope"></i>
                    <span>blog</span>
                </a>
            </li>


            @if( $currentUser->isAdmin() )

                <li>
                    <a  href="{{ route('config.primary') }}">
                        <i class="fa fa-cog"></i>
                        <span>{{ trans('words.site_settings') }}</span>
                    </a>
                </li>

                <li>
                    <a  href="{{ route('city.index') }}">
                        <i class="fa fa-sitemap"></i>
                        <span>{{ trans('words.city') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('propertyType.index') }}">
                        <i class="fa fa-puzzle-piece"></i>
                        <span>{{ trans('words.type') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('propertyFeature.index') }}">
                        <i class="fa fa-star-half-o"></i>
                        <span>{{ trans('words.feature') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('purpose.index') }}">
                        <i class="fa fa-laptop"></i>
                        <span>{{ trans('words.purpose') }}</span>
                    </a>
                </li>

<!--                 <li>
                    <a  href="{{ URL::to('admin/featuredproperties') }}">
                        <i class="fa fa-user"></i>
                        <span>Featured</span>
                    </a>
                </li> -->

                <li>
                    <a  href="{{ route('testimonial.index') }}">
                        <i class="fa fa-thumbs-up"></i>
                        <span>{{ trans('words.testimony') }}</span>
                    </a>
                </li>

                <li>
                    <a  href="{{ route('client.index') }}">
                        <i class="fa fa-users"></i>
                        <span>{{ trans('words.client') }}</span>
                    </a>
                </li>

                <li>
                    <a  href="{{ route('faq.index') }}">
                        <i class="fa fa-users"></i>
                        <span>{{ trans('words.faq') }}</span>
                    </a>
                </li>

<!--                 <li>
                    <a  href="{{ URL::to('admin/subscriber') }}">
                        <i class="fa fa-user"></i>
                        <span>Subscriber</span>
                    </a>
                </li> -->

            @endif


        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
