<!--services-->
<section class="services-style-1 home-main-contant-style text-center ">
    <div class="container">
        <div class="row">
            @foreach ($services as $service)
                <div class="col-md-4 col-sm-4">
                    <div class="flip-effect">
                        <div class="flip-text-block">
                            <div class="text-box">
                                <i class="fa {{ $service->icon }}" aria-hidden="true"></i>
                                <h5>{{ $service->title }}</h5>
                                <p>{!! words($service->description, 100, '...') !!}</p>
                            </div>
                            <div class="text-box-sec">
                                <div class="info-content">
                                    <h5>{{ $service->title }}</h5>
                                    <p>{{ $service->description }}</p>
                                </div>
                            </div> 
                        </div>
                    </div> 
                </div>
            @endforeach
        </div>
    </div>
</section>
<!--services-->