<!-- Start Left Sidebar Categories -->
<div class="sidebar-block">
    <h3>Categories</h3>
    <div id="divider"></div>
    
    <ul class="list-unstyled">
        @foreach ($categories as $category)
            <li><a href="{{ route('category-blog', $category->slug) }}">{{ $category->name }}</a></li>
        @endforeach
    </ul>
</div>
<!-- End Sidebar Categories -->

<div class="spacer-30"></div>

<!-- Start Sidebar Tags -->
<div class="sidebar-block">
    <h3>Tags</h3>
    <div id="divider"></div>

    <div class="box-content">
        <div class="tags">
            @foreach ($tags as $tag)
                <a href="{{ route('blog-tag', $tag->slug) }}">{{ $tag->name }}</a> 
            @endforeach 
        </div>
    </div>
</div>
<!-- End Sidebar Tags -->