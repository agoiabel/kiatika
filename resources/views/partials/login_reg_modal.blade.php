    <!-- Modal HTML -->
    <div id="login_box" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="log_form">
                        <h2 class="frm_titl"> Login Form </h2>

                        @include('flash')

                        {!! Form::open(['route' => 'auth.postLogin', 'id'=>'loginForm', 'role'=>'form']) !!}

                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="text" name="email" class="form-control" id="u-email" placeholder="Email" required>
                                </div>

                                <div class="controls">
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                </div>

                                <div class="checkbox col-md-6">
                                    <label>
                                        <input type="checkbox"> Remember me
                                    </label>
                                </div>
                                <div class="forg_pass col-md-6 text-right">
                                    <a class="" href="#"> Forgot your password?  </a>
                                </div>
                                <div class="clearfix"></div>

                                <button type="submit" class="btn btn-primary">Sign In</button>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="reg_box" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="log_form">
                        <h2 class="frm_titl"> Create Account </h2>
                        
                        @include('flash')
                        
                        {!! Form::open(['route' => 'auth.postRegister', 'id'=>'regForm', 'role'=>'form']) !!}

                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="name" required>
                                </div>

                                <div class="controls">
                                    <input type="email" name="email" class="form-control" id="e-mail" placeholder="Email" required>
                                </div>

                                <div class="controls">
                                    <input type="password" name="password" class="form-control" id="passd" placeholder="Password" required>
                                </div>

                                <div class="controls">
                                    <input type="password" name="password_confirmation" class="form-control" id="passd" placeholder="Password Confirmation" required>
                                </div>

                                <div class="form-group">     
                                    <select name="city_id" id="basic" class="form-control">
                                            <option value="" disabled selected hidden> Chose city </option>
                                        @foreach($cities as $city)  
                                            <option value="{{$city->id}}">{{$city->city_name}}</option>         
                                        @endforeach                                       
                                    </select>
                                </div>

                                <div class="form-group">     
                                    <select name="role_id" id="basic" class="form-control">
                                        <option value="" disabled selected hidden> Chose profile type </option>
                                        <option value="3">Owner</option>
                                        <option value="2">Agent</option>
                                    </select>
                                </div> 

                                <button type="submit" class="btn btn-primary">Create Account</button>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
