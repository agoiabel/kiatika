    <section id="about-us">
        <div class="container">
            <div class="row">


				<section id="vision-mission">
				    <div class="col-md-12">
				            <div class="vision-mission">
				                <div class="col-md-6">
				                    <h3>Our Vision</h3>
				                    <div id="divider"></div>
				                    {{ getcong('mission')  }}
				                </div>
				                <div class="col-md-6">
				                    <h3>Our Mission</h3>
				                    <div id="divider"></div>
				                    {{ getcong('vision') }}
				                </div>
				            </div>
				    </div>
				</section> 

            </div>

        </div>
    </section>
