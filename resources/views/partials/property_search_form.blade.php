<div id="white-form-background">
    <h4 class="text-center">{{ trans('words.quick_search') }}</h4>
    
    <form id="contactForm" action="{{ route('propertysearch') }}">
    
        <div class="form-group">
            
            <label>{{ trans('words.location') }}</label>
            <select name="city_id" class="form-control" required>
                <option value="" selected="selected">{{ trans('words.chose_city') }}</option>
                @foreach ($cities as $city)
                    <option value="{{ $city->id }}">{{ $city->city_name }}</option>
                @endforeach
            </select>

        </div>

        <div class="form-group">

            <label>{{ trans('words.type') }}</label>
            <select name="type_id" class="form-control" required>
                <option value="" selected="selected">{{ trans('words.chose_property_type') }}</option>
                @foreach ($types as $type)
                    <option value="{{ $type->id }}">{{ $type->type }}</option>
                @endforeach
            </select>
        
        </div>

        <div class="form-group">
            
            <label>{{ trans('words.actions') }}</label>
            <select name="purpose_id" class="form-control" required>
                <option value="" selected="selected">{{ trans('words.chose_action') }}</option>
                @foreach ($purposes as $purpose)
                    <option value="{{ $purpose->id }}">{{ $purpose->name }}</option>
                @endforeach
            </select>
        
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>{{ trans('words.min_price') }} </label>
                    <select name="min_price" class="form-control" required>
                        <option value="" selected="selected">chose min</option>
                        <option value="200">{{ getcong('currency_sign') }}200</option>
                        <option value="300">{{ getcong('currency_sign') }}300</option>
                        <option value="400">{{ getcong('currency_sign') }}400</option>
                        <option value="500">{{ getcong('currency_sign') }}500</option>
                        <option value="700">{{ getcong('currency_sign') }}700</option>
                        <option value="800">{{ getcong('currency_sign') }}800</option>
                        <option value="900">{{ getcong('currency_sign') }}900</option>
                        <option value="1000">{{ getcong('currency_sign') }}1000</option>
                        <option value="5000">{{ getcong('currency_sign') }}5000</option>
                        <option value="20000">{{ getcong('currency_sign') }}20000</option>
                        <option value="25000">{{ getcong('currency_sign') }}25000</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <label>{{ trans('words.max_price') }} </label>
                    <select name="max_price" class="form-control" required>
                        <option value="" selected="selected">chose max</option>
                        <option value="400">{{ getcong('currency_sign') }}400</option>
                        <option value="500">{{ getcong('currency_sign') }}500</option>
                        <option value="700">{{ getcong('currency_sign') }}700</option>
                        <option value="800">{{ getcong('currency_sign') }}800</option>
                        <option value="900">{{ getcong('currency_sign') }}900</option>
                        <option value="1000">{{ getcong('currency_sign') }}1000</option>
                        <option value="5000">{{ getcong('currency_sign') }}5000</option>
                        <option value="20000">{{ getcong('currency_sign') }}20000</option>
                        <option value="25000">{{ getcong('currency_sign') }}25000</option>
                        <option value="30000">{{ getcong('currency_sign') }}30000</option>
                    </select>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">{{ trans('words.filter_now') }}</button>
    </form>
</div>


