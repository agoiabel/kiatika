            <!--services-->
            <section class="services-style-1 home-main-contant-style ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Our Services</h3>
                            <div id="divider"></div>

                            <div class="row text-center">
                                @foreach ($services->chunk(3) as $set)
                                    @foreach ($set as $service)
                                        <div class="col-md-4 col-sm-4">
                                            <div class="flip-effect">
                                                <div class="flip-text-block">
                                                    <div class="text-box">
                                                        <i class="fa {{ $service->icon }}" aria-hidden="true"></i>
                                                        <h5>{{ $service->title }}</h5>
                                                        <p>{{ $service->description }}</p>
                                                    </div>
                                                    <div class="text-box-sec">
                                                        <div class="info-content">
                                                            <h5>{{ $service->title }}</h5>
                                                            <p>{{ $service->description }}</p>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                        </div>
                                    @endforeach
                                    <div class="spacer-30"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--services--><!--services-->
