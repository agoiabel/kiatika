<section class="my-partner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="partner_logo" class="owl-carousel owl-theme">
                    @foreach ($clients as $client)
                        <div class="item"><img src="/{{ $client->image_path }}" alt="logo"></div>
                    @endforeach
                </div> <!-- End .partner_logo -->                        
            </div>
        </div>
    </div>                
</section>