<div id="white-form-background" class="enquire">
    <h4 class="text-center">Contact Property Agent</h4>
    @include('flash')
    @include('errors.list')
    {!! Form::open(['route'=>'enquire', 'method'=>'POST']) !!}
		<input type="hidden" name="property_id" value="{{ $property->id }}">
		<input type="hidden" name="role_id" value="{{ '3' }}">

		<div class="form-group">
			<label>Name </label>
			<input type="text" name="name" class="form-control" placeholder="Please your name" >
		</div>

		<div class="form-group">
			<label>Email </label>
			<input type="email" name="email" class="form-control" placeholder="Please provide an email you can be contacted with" required>
		</div>

		<div class="form-group">
			<label>Message </label>
			<textarea name="message" class="form-control" placeholder="message" rows="10"></textarea>
		</div>

		<div class="clearfix"></div>
		<button type="submit" class="btn btn-primary">Send</button>
    {!! Form::close() !!}
</div>