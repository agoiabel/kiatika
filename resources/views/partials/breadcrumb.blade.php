<!-- Start Breadcrumb -->
<div class="page-banner no-subtitle">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <h2>{{ $page }}</h2>
            </div>
            <div class="text-center">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('testing_index') }}">Home</a></li>
                    <li>{{ $page }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumb -->

<div class="spacer-30"></div>
