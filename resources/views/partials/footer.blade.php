<div class="spacer-30"></div>

<!-- Footer -->
<footer>
    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <!-- About Section -->
                <div class="col-md-6">
                    <div class="footer-top-left">
                        <h2>{!! getcong('main_title') !!}</h2>

                        <p>
                            {!! getcong('about_us') !!}
                        </p>

                        <div class="social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-envelope"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </div><!-- /.social -->                                 
                    </div><!-- /.footer-top-left -->
                </div>
                <!-- Latest Tweets -->
                <div class="col-md-6">
                    <div class="footer-top-right">
                        <h2>{{ trans('words.subscribe_to_newsletter') }}</h2>

                        {!! Form::open([ 'route' => 'subscribe', 'data-remote', 'data-remote-success-message' => 'you successfully subscribe for newsletter']) !!}
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" placeholder="Your e-mail address">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">{{ trans('words.subscribe') }}</button>
                                </span><!-- /.input-group-btn -->
                            </div><!-- /.form-group -->
                        {!! Form::close() !!}
                                     

                        <p>
                            * {{ trans('words.we_promise_that_we_will_not_send_you_spam_messages') }}.
                        </p>                    
                    </div><!-- /.footer-top-right -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- Copyright -->
    <div class="footer-copy-right">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 text-center">
                    <p>&copy; {{ trans('words.copyright_2016_all_rights_reserved_by') }} <a href="#"> {!! getcong('header_title') !!} </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>