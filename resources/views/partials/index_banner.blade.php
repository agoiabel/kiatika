<header id="banner" class="stat_bann">
    <div class="bannr_sec">
        <img src="{{ getcong('front_banner_image_link') }}" alt="Banner">
        <h1 class="main_titl">
        {{ getcong('main_title') }}
    </h1>
        <h4 class="sub_titl">
        {{ getcong('sub_title') }}
    </h4>
    </div>
</header>