@extends('back_layout')
@section('content')

  <!-- page start-->
  <div class="row">
      <aside class="profile-nav col-lg-3">
          <section class="panel">

              <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href="{{ route('config.primary') }}"> <i class="fa fa-user"></i> Primary</a></li>
                  <li class="active"><a href="{{ route('config.contact') }}"> <i class="fa fa-user"></i> Contact</a></li>
                  <li class="active"><a href="{{ route('config.what_we_do') }}"> <i class="fa fa-user"></i> What we do</a></li>
                  <li class="active"><a href="{{ route('config.mission_vision') }}"> <i class="fa fa-user"></i> Mision and Vision</a></li>
              </ul>

          </section>
      </aside>
      <aside class="profile-info col-lg-9">

          <section class="panel">
              <div class="bio-graph-heading">
                  Site configuration -- Our Mission And Vision
              </div>
              <div class="panel-body bio-graph-info">

            @include('flash')
            @include('errors.list')
            
            {!! Form::open(['route' => 'config.postMissionVision', 'role'=>'form', 'files'=>true]) !!}

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Our Mission</label>
                      <div class="col-sm-9">
                        {!! Form::textarea('mission', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Our Vision</label>
                      <div class="col-sm-9">
                        {!! Form::textarea('vision', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>


              <div class="form-group">
                  <div class="col-md-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">Save</button>                     
                  </div>
              </div>

              
            {!! Form::close() !!}                         


              </div>
          </section>
          
      </aside>
  </div>

<!--main content end-->


@endsection
