@extends('back_layout')

@section('header')
  <script type="text/javascript" src="/css/vendor/fontawesome-iconpicker.css"></script>
@endsection

@section('content')

  <!-- page start-->
  <div class="row">
      <aside class="profile-nav col-lg-3">
          <section class="panel">

              <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href="{{ route('config.primary') }}"> <i class="fa fa-user"></i> Primary</a></li>
                  <li class="active"><a href="{{ route('config.contact') }}"> <i class="fa fa-user"></i> Contact</a></li>
                  <li class="active"><a href="{{ route('config.what_we_do') }}"> <i class="fa fa-user"></i> What we do</a></li>
                  <li class="active"><a href="{{ route('config.mission_vision') }}"> <i class="fa fa-user"></i> Mision and Vision</a></li>
              </ul>

          </section>
      </aside>
      <aside class="profile-info col-lg-9">

          <section class="panel">
              <div class="bio-graph-heading">
                  Site configuration -- What we do
              </div>
              <div class="panel-body bio-graph-info">

            @include('flash')
            @include('errors.list')
            
            {!! Form::open(['route' => 'config.post_what_we_do', 'role'=>'form', 'files'=>true]) !!}


              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Title</label>
                      <div class="col-sm-9">
                        {!! Form::text('what_we_do_title', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>
              
              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Icon Picker</label>
                      <div class="col-sm-9">
                        {!! Form::text('what_we_do_icon', null, ['class' => 'form-control icon-picker']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Description</label>
                      <div class="col-sm-9">
                        {!! Form::text('what_we_do_description', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-offset-3 col-sm-offset-3 col-xm-offset-3 col-xm-9 col-sm-9 col-md-9 clearfix">
                    <div class="row">
                      <button type="submit" class="btn btn-primary pull-left what_we_do_button">Save</button>                     
                      <a href="{{ route('what_we_do.index') }}" class="btn btn-primary pull-right">View All</a>                                           
                    </div>
                  </div>
              </div>

              
            {!! Form::close() !!}                         


              </div>
          </section>
          
      </aside>
  </div>

<!--main content end-->


@endsection

@section('footer')
  <script type="text/javascript" src="/js/vendor/fontawesome-iconpicker.js"></script>
  <script type="text/javascript">
    $('.icon-picker').iconpicker();
  </script>
@endsection