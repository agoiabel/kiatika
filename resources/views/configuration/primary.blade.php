@extends('back_layout')
@section('content')

  <!-- page start-->
  <div class="row">
      <aside class="profile-nav col-lg-3">
          <section class="panel">

              <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href="{{ route('config.primary') }}"> <i class="fa fa-user"></i> Primary</a></li>
                  <li class="active"><a href="{{ route('config.contact') }}"> <i class="fa fa-user"></i> Contact</a></li>
                  <li class="active"><a href="{{ route('config.what_we_do') }}"> <i class="fa fa-user"></i> What we do</a></li>
                  <li class="active"><a href="{{ route('config.mission_vision') }}"> <i class="fa fa-user"></i> Mision and Vision</a></li>
              </ul>

          </section>
      </aside>
      <aside class="profile-info col-lg-9">

          <section class="panel">
              <div class="bio-graph-heading">
                  Site configuration -- Primary
              </div>
              <div class="panel-body bio-graph-info">

            @include('flash')
            @include('errors.list')
            
            {!! Form::open(['route' => 'config.postPrimary', 'role'=>'form', 'files'=>true]) !!}

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Logo</label>
                      <div class="col-sm-9">
                        {!! Form::file('logo', ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Favicon</label>
                      <div class="col-sm-9">
                        {!! Form::file('favicon', ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Header Title</label>
                      <div class="col-sm-9">
                        {!! Form::text('header_title', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Main Title</label>
                      <div class="col-sm-9">
                        {!! Form::text('main_title', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Color Picker</label>
                      <div class="col-sm-9">
                        <select name="color_picker" class="form-control" required>
                            <option value="" selected="selected">chose color</option>
                            <option value="blue.css">default</option>
                            <option value="cyan.css">Brown</option>
                            <option value="green.css">Green</option>
                            <option value="grey.css">Grey</option>
                            <option value="lemon_ginger.css">Lemon Ginger</option>
                            <option value="light-blue.css">Light Blue</option>
                        </select>
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Index Page Picker</label>
                      <div class="col-sm-9">
                        <select name="index_page_view" class="form-control" required>
                            <option value="" selected="selected">chose index page</option>
                            <option value="pages.testing_index">Default</option>
                            <option value="pages.map_index">index with full map</option>
                            <option value="pages.fullslider_index">index with fullslider</option>
                        </select>
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Language Picker</label>
                      <div class="col-sm-9">
                        <select name="localization" class="form-control" required>
                            <option value="" selected="selected">chose langugae</option>
                            <option value="es"> spanish</option>
                            <option value="fr"> french</option>
                            <option value="pt"> portuguese</option>
                            <option value="ro"> romania</option>
                            <option value="en"> english</option>
                            <option value="ar"> arabic</option>
                        </select>
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">Currency Symbol</label>
                      <div class="col-sm-9">
                        {!! Form::text('currency_sign', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="row">
                    <label for="" class="col-sm-3 control-label">About us</label>
                      <div class="col-sm-9">
                        {!! Form::textarea('about_us', null, ['class' => 'form-control']) !!}
                    </div>
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">Save</button>                     
                  </div>
              </div>

              
            {!! Form::close() !!}                         


              </div>
          </section>
          
      </aside>
  </div>

<!--main content end-->


@endsection
