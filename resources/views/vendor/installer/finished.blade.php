@extends('vendor.installer.layouts.master')

@section('title', trans('words.final.title'))
@section('container')
    <p class="paragraph">{{ session('message')['message'] }}</p>
    
    <p class="paragraph"><b>We create an admin account for you by default</b></p>
    <p class="paragraph">Email: johndoe@admin.com</p>
    <p class="paragraph">Password: abc</p>
    
    <div class="buttons">
        <a href="{{ route('testing_index') }}" class="button">{{ trans('words.final.exit') }}</a>
    </div>
@stop