@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Our Blog'])

    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8">                    
                    <div class="content">                  
                        <div class="posts"> 
                            @foreach ($blogs as $blog)
                                <div class="post">
                                    <div class="post-date">
                                        <strong>{{ blog_published_date($blog->published_at) }}</strong>
                                        <span>{{ blog_published_month($blog->published_at) }}</span>
                                    </div>

                                    <div class="post-image-block">
                                        <a href="{{ route('blog-single', $blog->slug) }}"><img src="{{ url($blog->image) }}" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="blog-dis">
                                        <h2 class="entry-title"><a href="#">{{ $blog->title }}</a></h2>
                                        <p class="post-meta">
                                            By <span class="author vcard"><a title="Posts By {{ $blog->authur->name }}" href="#">{{ $blog->authur->name }}</a></span>  |  <span class="published">{{ $blog->created_at }}</span>  |  <a rel="category tag" href="{{ route('category-blog', $blog->category->slug) }}">{{ $blog->category->name }}</a>  
                                        </p>
                                        <p>
                                            {{ str_limit($blog->description, 256) }}
                                        </p>
                                        <div class="post-read-more">
                                            <a href="{{ route('blog-single', $blog->slug) }}" class="read-more">Read More <i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div><!-- /.post-->
                            @endforeach
                        </div>
                    </div>

                    {!! $blogs->render() !!}

                </div>
                <div class="col-md-4">

                    @include('partials.property_search_form')

                    <div class="spacer-30"></div>

                    @include('partials.category_tag')
                </div>
            </div>
        </div>
    </section>


@endsection