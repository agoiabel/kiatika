@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Our Services'])

<section id="service-page">
        
    @include('partials.services_2')
    <div class="spacer-30"></div>
        
    @include('partials.service-counter')
    <div class="spacer-30"></div>

    
    @include('partials.our-partner')

</section>

@endsection
@section('footer')
    <script type="text/javascript" src="js/vendor/waypoints.min.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.counterUp.js"></script>
    <script type="text/javascript">
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
    </script>
@endsection