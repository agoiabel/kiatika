@extends('front_layout')
@section('content')

@include('partials.breadcrumb', ['page' => 'Contact Us'])

<div class="spacer-30"></div>

<div id="longitude" style="display:none;">{!! $longLat['longitude'] !!}</div>
<div id="latitude" style="display:none;">{!! $longLat['latitude'] !!}</div>

<section id="contact-us">
    <div class="container">

            <div class="contact-us-map">
                <div class="row">
                    <div class="col-md-12">
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>

            <div class="spacer-30"></div>

            <div class="contact-info">
                <div class="row">
                    <div class="col-md-4">
                        <div class="contact-info-description">
                            <div class="contact-info-icon text-center">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="contact-info-detial">
                                <small>{{ trans('words.we_are_on_24_7') }}</small>
                                <h5>{{ trans('words.local') }}: {{ getcong('phone_number') }}</h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-info-description">
                            <div class="contact-info-icon text-center">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="contact-info-detial">
                                <small>{{ trans('words.send_us_email_on') }}</small>
                                <h5>{{ getcong('email') }}</h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-info-description">
                            <div class="contact-info-icon text-center">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="contact-info-detial">
                                <small>{{ trans('words.meet_us_at') }}</small>
                                <h5>{{ getcong('office_address') }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="spacer-30"></div>

            <div class="email-us">
                <div class="row">
                    <div class="col-md-12">     

                        <h3>{{ trans('words.send_us_a_mail') }}</h3>

                        @include('errors.list')
                        @include('flash')

                    </div>

                    <form action="{{ route('postContact') }}" method="POST" >

                        <input type="hidden" name="role_id" value="{{ '3' }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{ trans('words.your_name') }}</label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>{{ trans('words.your_email') }}</label>
                                <input type="email" name="email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>{{ trans('words.your_subject') }}</label>
                                <input type="text" name="subject" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <label>{{ trans('words.your_message') }}</label>
                            <textarea class="form-control" name="message"></textarea>
                        </div>

                        <div class="col-md-4 col-md-offset-8">
                            <button type="submit" class="btn btn-primary btn-block">{{ trans('words.send_message') }}</button>
                        </div>

                    </form>

                </div>                  
            </div>

    </div>
</section>

@endsection

@section('footer')
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="/js/vendor/mapper.js"></script>
@endsection