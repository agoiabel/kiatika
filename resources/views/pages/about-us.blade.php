@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'About Us'])

    @include('partials.vision-mission')

    <div class="spacer-30"></div>            

    @include('partials.testimonial')

    <div class="spacer-30"></div>            

    @include('partials.our-partner')



@endsection
@section('footer')
    <script type="text/javascript" src="js/vendor/waypoints.min.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.counterUp.js"></script>
    <script type="text/javascript">
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
    </script>
@endsection