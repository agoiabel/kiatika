@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Blog Post Detials'])

    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8">                    
                    <div class="content">                  
                        <div class="posts"> 

                            <div class="post">
                                <div class="post-date">
                                    <strong>{{ blog_published_date($blog->published_at) }}</strong>
                                    <span>{{ blog_published_month($blog->published_at) }}</span>
                                </div>

                                <div class="post-image-block">
                                    <a href="{{ route('blog-single', $blog->slug) }}"><img src="{{ url($blog->image) }}" alt="" class="img-responsive"></a>
                                </div>
                                <div class="blog-dis">
                                    <h2 class="entry-title"><a href="#">{{ $blog->title }}</a></h2>
                                    <p class="post-meta">
                                        By <span class="author vcard"><a title="Posts By {{ $blog->authur->name }}" href="#">{{ $blog->authur->name }}</a></span>  |  <span class="published">{{ $blog->created_at }}</span>  |  <a rel="category tag" href="{{ route('category-blog', $blog->category->slug) }}">{{ $blog->category->name }}</a>  
                                    </p>
                                    <p>
                                        {{ $blog->description }}
                                    </p>

                                </div>
                            </div><!-- /.post-->


                            <div class="blog-comments">     
                                <div class="row">
                                    <div class="col-md-11 col-md-offset-1">
                                        <div class="blog-comment">

                                            <h1 class="comments_title">{{ $comments->count() }} {{ str_plural('comment', $comments->count() ) }}</h1>

                                            <ul class="comments">
                                                @forelse ($comments as $comment)
                                                    <li>
                                                        <div class="comment">           
                                                            <div class="comment-author">
                                                                <a href="#" style="background-image: url(/{{ $comment->user->profile->image_icon }});"></a>
                                                            </div><!-- /.comment-author -->

                                                            <div class="comment-content">       

                                                                <div class="comment-meta clearfix">
                                                                    <div class="comment-meta-author">
                                                                        Posted by <a href="#">{{ $comment->user->name }}</a>
                                                                    </div><!-- /.comment-meta-author -->


                                                                    <div class="comment-meta-date">
                                                                        <span>{{ $comment->created_at }}</span>
                                                                    </div>
                                                                </div><!-- /.comment -->

                                                                <div class="comment-body">
                                                                    {{ $comment->comment }}
                                                                </div><!-- /.comment-body -->
                                                            </div><!-- /.comment-content -->
                                                        </div><!-- /.comment -->        
                                                    </li>
                                                @empty 
                                                    <p>No comment yet</p>
                                                @endforelse

                                            </ul>
                                        </div>
                                    </div>
                                </div>                                
                            </div>

                            @include('partials.comment')

                        </div>
                    </div>


                </div>
                <div class="col-md-4">

                    @include('partials.property_search_form')

                    <div class="spacer-30"></div>

                    @include('partials.category_tag')
                </div>
            </div>
        </div>
    </section>


@endsection