@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Frequently Ask Questions'])

  <section id="faq">
    <div class="container">
      <div class="row">
        
        <div class="col-md-8">
            @forelse ($faqs as $faq)
                <div class="faq">
                    <div class="faq-item">
                        <div class="faq-item-question"><h2>{{ $faq->question }}</h2></div>
                        {{ $faq->answer }}
                    </div>
                </div>
            @empty
                <div class="faq">
                    <div class="faq-item">
                        <div class="faq-item-question"><h2>No question asked yet</h2></div>
                    </div>
                </div>
            @endforelse
        </div>
        
        <div class="col-md-4">
            @include('partials.property_search_form')                
        </div>

        <div class="col-md-12">
            {!! $faqs->render(); !!}
        </div>

      </div>
    </div>      
  </section>  

@endsection
