@extends('front_layout')
@section('content')



@include('partials.breadcrumb', ['page' => 'Single Property Detials' ])


<div id="longitude" style="display:none;">{!! $longLat['longitude'] !!}</div>
<div id="latitude" style="display:none;">{!! $longLat['latitude'] !!}</div>

<section id="property-attributes">
    <div id="single-search-slider">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <ul class="nav nav-tabs nav-tabs-items-4" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#property-description">Basic Detials</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#property-amenities">Amenities</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#property-map">Property Map</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#more-detials">Property Agent Detials</a>
                        </li>       
                    </ul>

                    <div class="spacer-20"></div>                       

                    <div class="tab-content listing-detail">
                        <div class="tab-pane active" id="property-description" role="tabpanel">

                            <div id="owl-single-slider" class="owl-carousel owl-theme">
                       
                                @foreach ($property->images as $image)
                                    <div> 
                                        <img src="/{{ $image->slider_path }}" class="img-thumbnail"> 
                                    </div>
                                @endforeach

                            </div>  

                            <div class="spacer-10"></div>                       

                            <div id="property-attribute">                                   
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5>LOCATION</h5>
                                        <span>{{ $property->address }} {{ $property->city->city_name }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>PROPERTY TYPE</h5>
                                        <span>House</span>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>PROPERTY PURPOSE</h5>
                                        <span>{{ $property->purpose->name }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>PRICE</h5>
                                        <span>{{ getcong('currency_sign') }}{{ $property->price }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="spacer-20"></div>                       

                            <div id="property-attribute">                                   
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>SHORT DESCRIPTION</h5>
                                        <span>
                                            {!! nl2br($property->description) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="property-amenities" role="tabpanel">
                            <h2>Amenities</h2>
                                <div class="col-md-12">
                                    <ul class="property-features">
                                        @foreach($property->features->chunk(4) as $set)
                                            <div class="row">

                                                <ul class="amenities">
                                                    @foreach ($set as $feature)
                                                        <div class="col-md-3 col-sm-3 col-xm-3">
                                                            <li class="{{ property_feature_status( $feature->is_present  ) }}"> {{ $feature->feature->name }} </li>
                                                        </div>                                                                                                
                                                    @endforeach                                                                                                
                                                </ul>                                                    

                                            </div>
                                        @endforeach
                                    </ul>
                                </div>
                        </div>

                        <div class="tab-pane" id="property-map" role="tabpanel">

                            <div class="property-single-map">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="map-canvas"></div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>

                        <div class="tab-pane" id="more-detials" role="tabpanel">

                        <h2>agent detials</h2>

                        <div id="agents">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="agent">
                                        <div class="row">

                                            <div class="col-md-4 col-sm-4 col-xm-4">
                                                <div class="agent-image">
                                                    <img src="/{{ $property->agent->profile->image_icon }}" alt="">
                                                </div>
                                            </div>

                                            <div class="col-md-8 col-sm-8 col-xm-8">
                                                <div class="agent-feature">

                                                    <div class="col-md-7">
                                                        <div class="agent-name">{{ $property->agent->name }} - {{ $property->agent->role->name }}</div>
                                                    </div>


                                                    <div class="col-md-5">
                                                        <p>
                                                            <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i>
                                                        </p>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <span class="no-of-property-uploaded">{{ $property_count }} properties uploaded</span>
                                                    </div>

                                                    <div class="spacer-10"></div>

                                                    <div class="col-md-12">
                                                        <p>
                                                            {{ $property->agent->profile->about }}
                                                        </p>
                                                    </div>

                                                <!-- <div class="spacer-5"></div> -->


                                                    <div class="col-md-12">
                                                        <ul class="social-links">
                                                            <li><a href="{{ $property->agent->profile->facebook }}"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="{{ $property->agent->profile->twitter }}"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="{{ $property->agent->profile->linkedin }}"><i class="fa fa-linkedin"></i></a></li>
                                                            <li><a href="{{ $property->agent->profile->gplus }}"><i class="fa fa-gplus"></i></a></li>
                                                        </ul>
                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>

                        <h2>similar properties uploaded</h2>

                            <div class="row">
                                @foreach ($similar_properties as $similar_property)
                                    <div class="col-xm-4 col-md-4 col-sm-4">
                                        <div id="property-item">
                                            <div class="panel panel-default">
                                                <div class="panel-image">
                                                    <div class="property-type" href="#">{{ $similar_property->type->type }}</div>

                                                    <img class="img-responsive img-hover" src="/{{ $similar_property->images[0]->property_homepage_path }}" alt="">
                                                    <div class="img_hov_eff">
                                                        <a class="btn btn-default btn_trans" href="{{ route('property_detial', $similar_property->slug) }}"> More Detials </a>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <h4><a href="#">{{ $similar_property->address }}</a></h4>
                                                    <p>{{ $similar_property->city->city_name }}</p>

                                                    <div id="property-divider"></div>
                                                    <p id="property-purpose" class="pull-left">{{ $similar_property->purpose->name }}</p>
                                                    <p id="property-price" class="pull-right">{{ $similar_property->price }}</p>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                
                                @endforeach
                            </div>

                        </div>
                    </div>

                </div>



                <div class="col-md-4">

                    @include('partials.property_search_form')

                    <div class="spacer-30"></div>

                    @include('partials.enquire_form')

                </div>
            </div>
        </div>
    </div>

</section>



@endsection
@section('footer')
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="/js/vendor/mapper.js"></script>
@endsection