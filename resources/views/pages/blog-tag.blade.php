@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Our Blog'])

    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8">                    
                    <div class="content">                  
                        <div class="posts"> 
                            @foreach ($blogTags as $blogTag)
                                <div class="post">
                                    <div class="post-date">
                                        <strong>{{ blog_published_date($blogTag->blog->published_at) }}</strong>
                                        <span>{{ blog_published_month($blogTag->blog->published_at) }}</span>
                                    </div>

                                    <div class="post-image-block">
                                        <a href="{{ route('blog-single', $blogTag->blog->slug) }}"><img src="{{ url($blogTag->blog->image) }}" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="blog-dis">
                                        <h2 class="entry-title"><a href="#">{{ $blogTag->blog->title }}</a></h2>
                                        <p class="post-meta">
                                            By <span class="author vcard"><a title="Posts By {{ $blogTag->blog->authur->name }}" href="#">{{ $blogTag->blog->authur->name }}</a></span>  |  <span class="published">{{ $blogTag->blog->created_at }}</span>  |  <a rel="category tag" href="{{ route('category-blog', $blogTag->blog->category->slug) }}">{{ $blogTag->blog->category->name }}</a>  
                                        </p>
                                        <p>
                                            {{ str_limit($blogTag->blog->description, 256) }}
                                        </p>
                                        <div class="post-read-more">
                                            <a href="{{ route('blog-single', $blogTag->blog->slug) }}" class="read-more">Read More <i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div><!-- /.post-->
                            @endforeach
                        </div>
                    </div>

                    {!! $blogTags->render() !!}

                </div>
                <div class="col-md-4">

                    @include('partials.property_search_form')

                    <div class="spacer-30"></div>

                    @include('partials.category_tag')
                </div>
            </div>
        </div>
    </section>


@endsection