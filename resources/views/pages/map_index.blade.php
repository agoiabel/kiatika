@extends('front_layout')
@section('content')

<div id="longitude" style="display:none;">{!! $longLat['longitude'] !!}</div>
<div id="latitude" style="display:none;">{!! $longLat['latitude'] !!}</div>


<section class="index-search-form">
    <div id="map-canvas"></div>
</section>

<section id="horizontal-index-search-form">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                @include('partials.property_horizontal_search_form')
            </div>
        </div>
    </div>
</section>

<div class="spacer-30"></div>

@include('partials.services')

<div class="spacer-30"></div>

<section id="property-display">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h3>{{ trans('words.recent_properties') }}</h3>
                <div id="divider"></div>
                <div class="row">

                    @foreach($latest_properties as $property)
                        <div class="col-xm-6 col-md-4 col-sm-6">
                            <div id="property-item">
                                <div class="panel panel-default">
                                    <div class="panel-image">
                                        <div class="property-type" href="#">{{ $property->type->type }}</div>

                                        <img class="img-responsive img-hover" src="/{{ $property->images[0]->property_homepage_path }}" alt="">
                                        <div class="img_hov_eff">
                                            <a class="btn btn-default btn_trans" href="{{ route('property_detial',$property->slug) }}"> More Detials </a>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <h4><a href="#" class="property-address">{{ $property->address }}</a></h4>
                                        <p>{{ $property->city->city_name }}</p>

                                        <div id="property-divider"></div>
                                        <p id="property-purpose" class="pull-left">{{ $property->purpose->name }}</p>
                                        <p id="property-price" class="pull-right">{{ getcong('currency_sign') }} {{ $property->price }}</p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                
                </div>
                <a href="#" class="pull-right show-all">show all <i class="glyphicon glyphicon-chevron-right"></i></a>
            </div>
            <div class="col-md-3">
                <h3>{{ trans('words.top_agents') }}</h3>
                <div id="divider"></div>


                @foreach ($experiences as $experience)

                    <div class="agent">
                        <div class="media">
                          <div class="media-left">
                            <a href="#">
                              <img src="{{ asset($experience->user->profile->front_image_icon) }}" class="media-object" alt="">
                            </a>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">{{ $experience->user->name }}</h4>
                                <div class="agent-feature">
                                    {!! agent_experience_rate($experience->points)  !!}
                                </div>
                            <div class="phone"><i class="fa fa-phone"></i> {{ $experience->user->profile->phone }}</div>
                            <div class="email"></i><a href="mailto:victoria@example.com">{{ $experience->user->email }}</a></div>
                          </div>
                        </div>
                    </div>

                @endforeach


                <a href="#" class="pull-right show-all">show all <i class="glyphicon glyphicon-chevron-right"></i></a>

            </div>
        </div>
    </div>
</section>

<div class="spacer-30"></div>

<section id="property-display">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h3>{{ trans('words.featured_properties') }}</h3>
                <div id="divider"></div>

                @include('partials.featured-property')

            </div>
        </div>
    </div>
</section>

<div class="spacer-30"></div>

@include('partials.our-partner')


@endsection

@section('footer')
    
    <script type="text/javascript" src="/js/vendor/ajax.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="/js/vendor/mapper.js"></script>

@endsection