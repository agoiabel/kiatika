@extends('front_layout')
@section('content')

@include('partials.breadcrumb', ['page' => 'Featured Properties'])

<section id="property-display">
    <div class="container">
        <div class="row">
            @foreach ($properties->chunk(3) as $set)
                @foreach ($set as $property)
                    <div class="col-xm-6 col-md-4 col-sm-6">
                        <div id="property-item">
                            <div class="panel panel-default">
                                <div class="panel-image">
                                    <div class="property-type" href="#">{{ $property->type->type }}</div>

                                    <img class="img-responsive img-hover" src="/{{ $property->images[0]->property_homepage_path }}" alt="">
                                    <div class="img_hov_eff">
                                        <a class="btn btn-default btn_trans" href="{{ route('property_detial', $property->slug) }}"> More Detials </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <h4><a href="#">{{ $property->address }}</a></h4>
                                    <p>{{ $property->city->city_name }}</p>

                                    <div id="property-divider"></div>
                                    <p id="property-purpose" class="pull-left">{{ $property->purpose->name }}</p>
                                    <p id="property-price" class="pull-right">{{ getcong('currency_sign') }} {{ $property->price }}</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endforeach

            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! $properties->render() !!}
            </div>

        </div>
    </div>
</section>


@endsection