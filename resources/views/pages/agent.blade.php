@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Our Agents'])

    <section id="agents">
        <div class="container">

            <div class="row">
                @foreach ($agents as $agent)
                    <div class="col-md-6">
                        <div id="agent">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="agent-image">
                                        <img src="/{{ $agent->profile->image_icon }}" alt="">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="agent-feature">

                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="agent-name">{{ $agent->name }}</div>
                                            </div>


                                            <div class="col-md-5">
                                                <p>
                                                    {!! agent_experience_rate($agent->experience->points)  !!}
                                                </p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="no-of-property-uploaded">{{ $agent->experience->points }} properties uploaded</span>
                                            </div>
                                        </div>

                                        <div class="spacer-10"></div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>
                                                    {{ $agent->profile->about }}
                                                </p>
                                            </div>
                                        </div>

                                        <!-- <div class="spacer-5"></div> -->

                                        <div class="row">
                                            <div class="col-md-8">
                                                <ul class="social-links">
                                                    <li><a href="{{ $agent->profile->facebook }}"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="{{ $agent->profile->twitter }}"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="{{ $agent->profile->linkedin }}"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a href="{{ $agent->profile->gplus }}"><i class="fa fa-gplus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>                  
                        </div>                  
                    </div>
                @endforeach


                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! $agents->render() !!}
                </div>

            </div>

        </div>
    </section>


@endsection