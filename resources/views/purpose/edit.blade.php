@extends("back_layout")
@section("content")
        
<!-- page start-->
<div align="right">
  <a href="{{ route('purpose.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>

@include('errors.list')
@include('flash')

<div class="panel panel-default">
    <div class="panel-body">

        {!! Form::model($property_purpose, ['route' => 'purpose.update','class'=>'form-horizontal padding-15','name'=>'type_form','id'=>'type_form','role'=>'form']) !!} 
            <input type="hidden" name="slug" value="{{ $property_purpose->slug }}">
            @include('purpose._form', ['btn' => 'Update'])
        {!! Form::close() !!} 

    </div>
</div>
   
@endsection