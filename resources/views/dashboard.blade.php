@extends('back_layout')
@section('content')
<div class="row state-overview">
  
  @if( $currentUser->isAdmin() )
    <div class="col-lg-3 col-sm-6">
      <div class="panel panel-primary">
          <div class="symbol terques">
            <i class="fa fa-map-marker"></i>
          </div>
          <div class="value">
            <h1>{{ $allPropertyCount }}</h1>
            <h5>Total Property</h5>
          </div>      
      </div>
    </div>

    <div class="col-lg-3 col-sm-6">
      <div class="panel panel-primary">
          <div class="symbol yellow">
            <i class="fa fa-star-half-o"></i>
          </div>
          <div class="value">
            <h1>{{ $allFeaturedProperty }}</h1>
            <h5>Featured Property</h5>
          </div>      
      </div>
    </div>

    <div class="col-lg-3 col-sm-6">
      <div class="panel panel-primary">
          <div class="symbol blue">
            <i class="fa fa-map-marker"></i>
          </div>
          <div class="value">
            <h1>{{ $publishedPropertyCount }}</h1>
            <h5>Published Property</h5>
          </div>      
      </div>
    </div>

    <div class="col-lg-3 col-sm-6">
      <div class="panel panel-primary">
          <div class="symbol yellow">
            <i class="fa fa-users"></i>
          </div>
          <div class="value">
            <h1>{{ $agentCount }}</h1>
            <h5>Agent Count</h5>
          </div>      
      </div>
    </div>

    <div class="col-lg-3 col-sm-6">
      <div class="panel panel-primary">
          <div class="symbol terques">
            <i class="fa fa-users"></i>
          </div>
          <div class="value">
            <h1>{{ $customerCount }}</h1>
            <h5>Customer Count</h5>
          </div>      
      </div>
    </div>

    <div class="col-lg-3 col-sm-6">
      <div class="panel panel-primary">
          <div class="symbol blue">
            <i class="fa fa-users"></i>
          </div>
          <div class="value">
            <h1>{{ $allClientCount }}</h1>
            <h5>Client Count</h5>
          </div>      
      </div>
    </div>

    <div class="col-lg-3 col-sm-6">
      <div class="panel panel-primary">
          <div class="symbol terques">
            <i class="fa fa-thumbs-up"></i>
          </div>
          <div class="value">
            <h1>{{ $allTestimonialCount }}</h1>
            <h5>Testimonial Count</h5>
          </div>      
      </div>
    </div>

  @endif

  <div class="col-lg-3 col-sm-6">
    <div class="panel panel-primary">
        <div class="symbol red">
          <i class="fa fa-map-marker"></i>
        </div>
        <div class="value">
          <h1>{{ $ownPropertyCount }}</h1>
          <h5>Property Upload By You</h5>
        </div>      
    </div>
  </div>

  <div class="col-lg-3 col-sm-6">
    <div class="panel panel-primary">
        <div class="symbol terques">
          <i class="fa fa-map-marker"></i>
        </div>
        <div class="value">
          <h1>{{ $ownPublishedPropertyCount }}</h1>
          <h5>Unpublished Property By You</h5>
        </div>      
    </div>
  </div>

  <div class="col-lg-3 col-sm-6">
    <div class="panel panel-primary">
        <div class="symbol red">
          <i class="fa fa-envelope"></i>
        </div>
        <div class="value">
          <h1>{{ $ownMessageInbox }}</h1>
          <h5>Message Inbox</h5>
        </div>      
    </div>
  </div>

  <div class="col-lg-3 col-sm-6">
    <div class="panel panel-primary">
        <div class="symbol yellow">
          <i class="fa fa-envelope"></i>
        </div>
        <div class="value">
          <h1>{{ $ownSentMessage }}</h1>
          <h5>Message Sent</h5>
        </div>      
    </div>
  </div>

</div>
@endsection

