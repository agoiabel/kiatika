@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Reset Password'])

  <section id="login">
    <div class="container">
      <div class="col-md-6 col-md-offset-3">

          
          @include('errors.list')
          @include('flash')

        <div id="white-form-background" class="clearfix">

        {!! Form::open(['route' => 'passwordReset.postEmail', 'role'=>'form']) !!}
          <div class="form-group">
                <label>Email </label>
                <input type="email" name="email" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary btn-block">Send Reset Link</button>
          </div>
        {!! Form::close() !!}

        <div class="spacer-20"></div>

        <div class="forgot-password text-center">
          <a href="{{ route('auth.login') }}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>  Return back to login </a>
        </div>
      </div>
    </div>
  </section>


@endsection
