@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Customer Login'])

<section id="login">
  <div class="container">

    <div class="col-md-6 col-md-offset-3">


        @include('errors.list')
        @include('flash')

      <div id="white-form-background" class="clearfix">

        {!! Form::open(['route' => 'customer.postLogin', 'role'=>'form']) !!}

          <div class="form-group">
            <label>Email </label>
            <input type="email" name="email" class="form-control">
          </div>

          <div class="pull-left remember-me"><input type="checkbox"> Remember me</div>
            <button type="submit" class="btn btn-primary pull-right">SIGN IN</button>
          </div>

        {!! Form::close() !!}                         

    </div>

  </div>
</section>



@endsection
