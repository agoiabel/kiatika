@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Reset Password'])

<section id="login">
  <div class="container">

    <div class="col-md-6 col-md-offset-3">

        @include('flash')
        @include('errors.list')

      <div id="white-form-background" class="clearfix">

          {!! Form::open(['route' => 'passwordReset.postReset', 'role'=>'form']) !!}

                <input type="hidden" name="token" value="{{$token}}">

                <div class="form-group">
                  <label>Password </label>
                  <input type="password" name="password" class="form-control" id="u-password" placeholder="new password" required>
                </div>


                <div class="form-group">
                  <label>Password </label>
                  <input type="password" name="password_confirmation" class="form-control" id="confirm password" placeholder="confirm new Password" required>
                </div>

                  <button type="submit" class="btn btn-primary pull-right">Reset Password</button>
                </div>

        {!! Form::close() !!}                         

    </div>

  </div>
</section>

@endsection