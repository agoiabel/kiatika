@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Registration'])

    <section id="register">
        <div class="container">
            <div class="col-md-8 col-md-offset-2">

                    @include('errors.list')
                    @include('flash')


                <div id="white-form-background" class="clearfix">

                {!! Form::open(['route' => 'auth.postRegister', 'id'=>'regForm', 'role'=>'form']) !!}
                
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name </label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Profile type </label>
                                <select name="role_id" class="form-control">
                                    <option value="" disabled selected hidden> Chose profile type </option>
                                    <option value="2">Owner</option>
                                    <option value="2">Agent</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Password </label>
                                <input type="password" name="password" class="form-control">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email </label>
                                <input type="email" name="email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>City </label>
                                <select name="city_id" class="form-control">
                                        <option value="" disabled selected hidden> Chose city </option>
                                    @foreach($cities as $city)  
                                        <option value="{{$city->id}}">{{$city->city_name}}</option>         
                                    @endforeach                                       
                                </select>
                            </div>  

                            <div class="form-group">
                                <label>Confirm Password </label>
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>

                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Create Account</button>
                
                    {!! Form::close() !!}
                </div>

                <div class="spacer-20"></div>

                <div class="forgot-password text-center">
                    <a href="{{ route('auth.login') }}">I already have an account <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>

            </div>
        </div>
    </section>



@endsection
