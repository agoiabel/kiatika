@extends('front_layout')
@section('content')

    @include('partials.breadcrumb', ['page' => 'Login'])

<section id="login">
  <div class="container">

    <div class="col-md-6 col-md-offset-3">

        @include('flash')
        @include('errors.list')

      <div id="white-form-background" class="clearfix">

        {!! Form::open(['route' => 'auth.postLogin', 'role'=>'form']) !!}

        <div class="form-group">
          <label>Email </label>
          <input type="email" name="email" class="form-control">
        </div>

        <div class="form-group">
          <label>Password </label>
          <input type="password" name="password" class="form-control">
        </div>

        <div class="pull-left remember-me"><input type="checkbox"> Remember me</div>
          <button type="submit" class="btn btn-primary pull-right">SIGN IN</button>
        </div>

        {!! Form::close() !!}                         


      <div class="spacer-20"></div>

      <div class="forgot-password text-center">
        <a href="{{ route('passwordReset.getEmail') }}">I forgot my password <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
      </div>
    </div>

  </div>
</section>



@endsection
