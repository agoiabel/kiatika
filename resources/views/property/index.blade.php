@extends('back_layout')

@section('header')
  <link rel="stylesheet" type="text/css" href="/css/vendor/jquery.dataTables.min.css">
@endsection

@section('content')
    
<!-- page start-->
<div align="right">
  <a href="{{ route('upload.create') }}" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> ADD NEW </a>
</div>
<hr>

@include('flash')

<div class="row">
    <div class="col-sm-12">
        
      <div class="row">
          <div class="col-lg-12">
              <section class="panel">

                <table class="table table-striped table-bordered datatable-responsive">
                    <thead>
                      <tr>
                          @if ( $currentUser->isAdmin() ) 
                          <th>Featured</th>
                          @endif
                          <th>Date Created</th>
                          <th>Title</th>
                          <th>City</th>
                          <th>Type</th>
                          <th>Purpose</th>
                          <th align="center">Published status</th>
                          <th align="center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($properties as $property)
                        <tr>
                            @if ( $currentUser->isAdmin() )
                            <td>
                                {!! Form::open(['data-remote', 'data-remote-success-message' => 'Property feature updated successfully', 'route' => 'upload.featured']) !!}
                                  <input type="hidden" name="slug" value="{{ $property->slug }}">
                                  {!! Form::checkbox('featured', $property->featured, $property->featured, ['data-click-submits-form']) !!}
                                {!! Form::close() !!}
                            </td>
                            @endif
                            <td>{{ $property->created_at }}</td>
                            <td>{{ $property->title }}</td>
                            <td>{{ $property->city->city_name }}</td>
                            <td>{{ $property->type->type }}</td>
                            <td>{{ $property->purpose->name }}</td>
                            <td>
                                {!! publish_status_action_on($property) !!}                                
                            </td>
                            <td>
                                <a href="{{ route('upload.show', $property->slug) }}" class="btn btn-primary btn-xs btn-circle"><i class="fa fa-file-o" aria-hidden="true"></i></a>
                                <a href="{{ route('upload.destroy', $property->slug) }}" class="btn btn-danger btn-xs btn-circle"><i class="fa fa-trash-o "></i></a>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>

              </section>
          </div>
      </div>

    </div>
</div>

@endsection


@section('footer')
  <script type="text/javascript" src="/js/vendor/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $('.datatable-responsive').DataTable({
      select: true,
    })
  </script>
  <script src="/js/vendor/ajax.js"></script>

@endsection