@extends('back_layout')
@section('content')

    <div align="right">
        <a href="{{ route('upload.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> back </a>
    </div><hr>

    @include('flash')

    <div class="col-md-12">

        <div class="col-md-4">
            <table class="table table-bordered table-striped">
                <thead>
                    <th class="info">Type</th>
                </thead>
                <tbody>
                    <td>{{ $property->type->type }}</td>
                </tbody>
            </table>
        </div>

        <div class="col-md-4">
            <table class="table table-bordered table-striped">
                <thead>
                    <th class="info">Price</th>
                </thead>
                <tbody>
                    <td> {{ getcong('currency_sign') }} {{ number_format($property->price) }}</td>
                </tbody>
            </table>
        </div>

        <div class="col-md-4">
            <table class="table table-bordered table-striped">
                <thead>
                    <th class="info">Location</th>
                </thead>
                <tbody>
                    <td>
                        {{ $property->address }}, {{ $property->city->city_name }} 
                    </td>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3">
            <table class="table table-bordered table-striped">
                <thead>
                    <th class="info">Features</th>
                </thead>
                @foreach($property->features as $feature)
                <tbody>
                    <td>{{ $feature ->feature->name }}</td>
                </tbody>
                @endforeach
            </table>            
        </div>
        <div class="col-md-9">

            <table class="table table-bordered table-striped">

                <div class="panel panel-info">
                    <div class="panel-heading">Description</div>

                    <div class="panel panel-body">
                        {!! nl2br($property->description) !!}                    
                    </div>
                </div>
            </table>            


            <div class="panel panel-info">
                <div class="panel-heading">Property Images</div>

                <div class="panel panel-body">
                    @foreach ($property->images->chunk(3) as $set)
                        <div class="row">
                            @foreach ($set as $image)
                                <div class="col-md-4 property-image-display">
                                    {!! Form::open(['route'=>'upload.image.delete']) !!}
                                        <input type="hidden" name="image_id" value="{{ $image->id }}">
                                        <button type="submit" class="btn btn-danger btn-xs property-image-delete-btn"><i class="fa fa-trash-o "></i></button>
                                    {!! Form::close() !!}
                                    <img src="/{{ $image->property_single_path }}" class="thumbnail" width="200" height="200">                        
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>

        </div> 
    </div>



@endsection
