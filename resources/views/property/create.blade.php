@extends("back_layout")
@section("content")


<!-- page start-->
<div align="right">
  <a href="{{ route('upload.index') }}" class="btn btn-primary btn-sm"> <i class="fa fa-arrow-left"></i> Back </a>
</div>
<hr>
    
	@include('errors.list')
    @include('flash')

    {!! Form::open(['route' => 'upload.store', 'class'=>'form-horizontal padding-15', 'enctype' => 'multipart/form-data']) !!} 

        <fieldset>
            <legend>Basic Property</legend>
               	<div class="panel panel-default">
                    <div class="panel-body">
                        
                        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select name="type_id" id="basic" class="form-control">
                                                <option value="" selected="selected">Chose property type</option>
                                            @foreach($types as $type)  
                                                <option value="{{$type->id}}">{{$type->type}}</option>
                                            @endforeach
                                                 
                                        </select>
                                    </div>
                                </div>    
                            </div>

                            <div class="col-md-6">

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select name="purpose_id" class="form-control">

                                            <option value="" selected="selected">Chose property purpose</option>
                                                 
                                            @foreach($purposes as $purpose)  
                                                <option value="{{$purpose->id}}">{{$purpose->name}}</option>
                                            @endforeach
                                                 
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                    
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-addon">{{ getcong('currency_sign') }}</div>
                                            {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'price']) !!}
                                        </div>
                                    </div>
                                </div>                    
                    
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">                    
                                    <div class="col-sm-12">
                                        <select name="city_id" id="basic" class="form-control">
                                                
                                                <option value="" selected="selected">chose city</option>
                                            @foreach($cities as $city)  
                                                <option value="{{$city->id}}">{{$city->city_name}}</option>
                                            @endforeach
                                                 
                                        </select>
                                    </div>                           
                                </div>                     
                            
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        {!! Form::textarea('address', null, ['class' => 'form-control', 'id' => 'address', 'rows' => '3', 'placeholder' => 'Property Address']) !!}                                            
                                    </div>
                                </div>                    
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '3', 'placeholder' => 'Property description']) !!}                    
                                    </div>
                                </div>
                            </div>


                    </div>
                </div>
        </fieldset>

        <fieldset>
            <legend>Property Images</legend>
                <div class="panel panel-default">
                    <div class="panel-body">
                        
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="box">
                                    <input type="file" id="input-file" name="property_images[]" multiple="true">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        </fieldset>

        <fieldset>
            <legend>Property Features</legend>
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    <div class="col-sm-12">
                        <ul class="property-features">
                            @forelse($features->chunk(4) as $set)
                                <div class="row">
                                    @foreach ($set as $feature)
                                        <div class="col-md-3 col-sm-2 col-xm-3">
                                            <li><label><input type="checkbox" name="features_id[]" value="{{$feature->id}}"> {{$feature->name}} </label></li>                                        
                                        </div>
                                    @endforeach                                    
                                </div>
                            @empty
                                <p class="text-center"><b>Please You need to first create property features </b><a href="{{ route('propertyFeature.create') }}">here</a></p>
                            @endforelse
                        </ul>
                    </div>

                </div>
            </div>
        </fieldset>


        <div class="col-md-4 col-md-offset-4">
            <div class="row">
                <button type="submit" class="btn btn-primary btn-block btn-lg">upload</button>
            </div>
        </div>

    {!! Form::close() !!}

@endsection

@section('footer')
    <script type="text/javascript">

            $('#input-file').fileinput({
                'showUpload': false
            });    

    </script>
@endsection