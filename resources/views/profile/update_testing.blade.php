@extends('back_layout')

@section('header')
    <link rel="stylesheet" type="text/css" href="/css/vendor/jasny-bootstrap.min.css">
@endsection

@section('content')

      <!--main content start-->

              <!-- page start-->


                  <aside class="profile-info col-lg-12">
                      <section class="panel">
                          <div class="bio-graph-heading">
                              Hi {{ $profile->user->name }}, you can view your profile info here
                              and the same time make use of the edit button to edit profile.
                          </div>
                          <div class="panel-body bio-graph-info">

                              <h1> Profile Info</h1>
					            {!! Form::model($profile, ['route' => 'profile.postUpdate','class'=>'form-horizontal padding-15','name'=>'account_form','id'=>'account_form','role'=>'form','files' => true]) !!}
					                
					                <input type="hidden" name="user_id" value="{{$profile->user_id}}">
					                <div class="row">
					                    <div class="col-md-8">
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Name</label>
					                        <div class="col-sm-9">

					                            <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" value="">
					                            
					                        </div>
					                    </div>
					                     <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Email</label>
					                        <div class="col-sm-9">
					                            <input type="email" name="email" value="{{ Auth::user()->email }}" class="form-control">
					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Phone</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('phone', null, ['class' => 'form-control']) !!}                
					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Fax</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('fax', null, ['class' => 'form-control']) !!}                
					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">City</label>
					                        <div class="col-sm-9">
					                            <select name="city_id" id="basic" class="selectpicker show-tick form-control form-control" data-live-search="true">
					                                             
					                                @foreach($city_list as $city)  
					                                    <option value="{{$city->id}}" @if($city->city_name==Auth::user()->city) selected @endif>{{$city->city_name}}</option>                                            
					                                @endforeach
					                                                  
					                            </select>

					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">About</label>
					                        <div class="col-sm-9">
					                            {!! Form::textarea('about', null, ['class' => 'form-control', 'cols' => '50', 'rows' => '5']) !!}                    
					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Facebook</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('facebook', null, ['class' => 'form-control']) !!}                        
					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Twitter</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('twitter', null, ['class' => 'form-control']) !!}                        
					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Google Plus</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('gplus', null, ['class' => 'form-control']) !!}                        
					                        </div>
					                    </div>
					                    <div class="form-group">
					                        <label for="" class="col-sm-3 control-label">Linkedin</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}                        
					                        </div>
					                    </div>

					                    </div>
					                    <div class="col-md-4">
					                        
					                        <div class="form-group">
					                            <div class="col-sm-12">
					                                <div class="media">

					                                    <div class="fileinput fileinput-new" data-provides="fileinput">
					                                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
					                                        <img src="/{{ $profile->image_icon }}" alt="Photo" style="width: 200px;">
					                                      </div>
					                                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
					                                      <div class="text-center">
					                                        <span class="btn btn-primary btn-file"><span class="fileinput-new">Choose Photo</span>
					                                        <span class="fileinput-exists">Change</span><input type="file" name="image_icon" required></span>
					                                      </div>
					                                    </div>

					                                </div>
					            
					                            </div>
					                        </div>

					                    </div>

					                </div>


					                <div class="form-group">
					                    <div class="col-md-offset-2 col-sm-10">
					                        <button type="submit" class="btn btn-primary">Save Changes <i class="md md-lock-open"></i></button>
					                    </div>
					                </div>                        

					            {!! Form::close() !!} 
                          </div>
                      </section>

                      <section>
                      		{!! Form::open(['route' => 'password_change', 'method'=>'POST']) !!}
                          <div class="panel panel-primary">
                              <div class="panel-heading"> Sets New Password</div>
                              <div class="panel-body">
                              		                              		
                              		<input type="hidden" name="user_id" value="{{ $currentUser->id }}">

					                <div class="form-group">
					                    <div class="row">
					                        <label for="" class="col-sm-3 control-label">Current Password</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('password', null, ['class' => 'form-control']) !!}                    
					                        </div>
					                    </div>
				                    </div>

				                    <div class="form-group">
										<div class="row">
					                        <label for="" class="col-sm-3 control-label">New Password</label>
					                        <div class="col-sm-9">
					                            {!! Form::text('password_confirmation', null, ['class' => 'form-control']) !!}                        
					                        </div>
					                    </div>
				                    </div>


					                <div class="form-group">
					                	<div class="row">
						                    <div class="col-md-offset-3 col-sm-9">
						                        <button type="submit" class="btn btn-primary">Save Changes <i class="md md-lock-open"></i></button>
						                    </div>
					                	</div>
					                </div>                        
                              			
                              		</div>

                              </div>
                          </div>
                      		{!! Form::close() !!}
                      </section>
                  </aside>
              </div>

              <!-- page end-->
	

@endsection

@section('footer')
    <script src="/js/vendor/jasny-bootstrap.min.js"></script>
@endsection