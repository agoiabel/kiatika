@extends('back_layout')
@section('content')

  <!-- page start-->
  <div class="row">
      <aside class="profile-nav col-lg-3">
          <section class="panel">
              <div class="user-heading round">
                  <a href="#">
                      <img src="/{{ $profile->image_icon }}" alt="">
                  </a>
                  <h1>{{ $profile->user->name }}</h1>
                  <p>{{ $profile->user->email }}</p>
              </div>

              <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href="{{ route('profile.index') }}"> <i class="fa fa-user"></i> Profile</a></li>
                  <li><a href="{{ route('profile.update') }}"> <i class="fa fa-edit"></i> Edit profile</a></li>
              </ul>

          </section>
      </aside>
      <aside class="profile-info col-lg-9">

          <section class="panel">
              <div class="bio-graph-heading">
                  Hi {{ $profile->user->name }}, you can view your profile info here
                  and the same time make use of the edit button to edit profile.
              </div>
              <div class="panel-body bio-graph-info">
                  <h1>Bio Graph</h1>
                  <div class="row">
                      <div class="bio-row">
                          <p><span>Role </span>: {{ $profile->user->role->name }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Name </span>: {{ $profile->user->name }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>City </span>: {{ $profile->user->city->city_name }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Email </span>: {{ $profile->user->email }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Mobile </span>: {{ $profile->phone }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Fax </span>: {{ $profile->fax }}</p>
                      </div>
                  </div>
              </div>
          </section>

          <section>
              <div class="row">
                  <div class="col-lg-6">
                      <div class="panel">
                          <div class="panel-body">
                              <div class="bio-chart">
                                  <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="100" data-fgColor="#e06b7d" data-bgColor="#e8e8e8">
                              </div>
                              <div class="bio-desk">
                                  <h4 class="red">Joined Date</h4>
                                  <p>{{ $profile->user->created_at }}</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="panel">
                          <div class="panel-body">
                              <div class="bio-chart">
                                  <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="63" data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                              </div>
                              <div class="bio-desk">
                                  <h4 class="terques">Property Uploaded </h4>
                                  <p>Number : 15</p>
                              </div>
                          </div>
                      </div>
                  </div>


              </div>
          </section>
      </aside>
  </div>

<!--main content end-->


@endsection

@section('footer')
	<script type="text/javascript" src="/js/vendor/jquery.knob.js"></script>
	<script type="text/javascript">
      $(".knob").knob();
	</script>
@endsection