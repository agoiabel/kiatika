@extends("admin.admin_app")

@section('header')
    <link rel="stylesheet" type="text/css" href="/assets/css/jasny-bootstrap.min.css">
@endsection

@section("content")

<div id="main">
	<div class="page-header">
		<h2> {{ Auth::user()->name }}</h2>	  
	</div>
        @include('errors.list')
        @include('flash')
    <div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#account" aria-controls="account" role="tab" data-toggle="tab">Account</a>
        </li>
        <li role="presentation">
            <a href="#ac_password" aria-controls="ac_password" role="tab" data-toggle="tab">Password</a>
        </li>        
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content tab-content-default">
        <div role="tabpanel" class="tab-pane active" id="account">             

            {!! Form::model($profile, ['route' => 'profile.postUpdate','class'=>'form-horizontal padding-15','name'=>'account_form','id'=>'account_form','role'=>'form','files' => true]) !!}
                
                <input type="hidden" name="user_id" value="{{$profile->user_id}}">
                <div class="row">
                    <div class="col-md-8">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">

                            <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" value="">
                            
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" value="{{ Auth::user()->email }}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9">
                            {!! Form::text('phone', null, ['class' => 'form-control']) !!}                
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Fax</label>
                        <div class="col-sm-9">
                            {!! Form::text('fax', null, ['class' => 'form-control']) !!}                
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">City</label>
                        <div class="col-sm-9">
                            <select name="city_id" id="basic" class="selectpicker show-tick form-control form-control" data-live-search="true">
                                             
                                @foreach($city_list as $city)  
                                    <option value="{{$city->id}}" @if($city->city_name==Auth::user()->city) selected @endif>{{$city->city_name}}</option>                                            
                                @endforeach
                                                  
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">About</label>
                        <div class="col-sm-9">
                            {!! Form::textarea('about', null, ['class' => 'form-control', 'cols' => '50', 'rows' => '5']) !!}                    
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Facebook</label>
                        <div class="col-sm-9">
                            {!! Form::text('facebook', null, ['class' => 'form-control']) !!}                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Twitter</label>
                        <div class="col-sm-9">
                            {!! Form::text('twitter', null, ['class' => 'form-control']) !!}                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Google Plus</label>
                        <div class="col-sm-9">
                            {!! Form::text('gplus', null, ['class' => 'form-control']) !!}                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Linkedin</label>
                        <div class="col-sm-9">
                            {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}                        
                        </div>
                    </div>

                    </div>
                    <div class="col-md-4">
                        
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="media">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="/{{ $profile->image_icon }}" alt="Photo" style="width: 200px;">
                                      </div>
                                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                      <div class="text-center">
                                        <span class="btn btn-primary btn-file"><span class="fileinput-new">Choose Photo</span>
                                        <span class="fileinput-exists">Change</span><input type="file" name="image_icon" required></span>
                                      </div>
                                    </div>

                                </div>
            
                            </div>
                        </div>

                    </div>

                </div>


                <div class="form-group">
                    <div class="col-md-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Save Changes <i class="md md-lock-open"></i></button>
                    </div>
                </div>                        

            {!! Form::close() !!} 
        </div>


        <div role="tabpanel" class="tab-pane" id="ac_password">
            
                
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">New Password</label>
                    <div class="col-sm-9">
                        <input type="password" name="password" value="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Confirm Password</label>
                    <div class="col-sm-9">
                        <input type="password" name="password_confirmation" value="" class="form-control" value="">
                    </div>
                </div>
                 
                <hr>
                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-9 ">
                        <button type="submit" class="btn btn-primary">Save Changes <i class="md md-lock-open"></i></button>
                    </div>
                </div>

            {!! Form::close() !!} 
        </div>
         
    </div>
</div>
</div>

@endsection

@section('footer')
    <script src="/assets/js/jasny-bootstrap.min.js"></script>
@endsection