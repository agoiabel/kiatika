<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel Installer',
    'next' => 'Próximo',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Wedoor Listagem de Propriedade Installer',
        'message' => 'Bem-vindo ao assistente de configuração',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requerimento..',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'permissões',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Environment Settings',
        'save' => 'salve .env',
        'success' => 'Suas configurações de arquivo .env foram salvas.',
        'errors' => 'Não foi possível salvar o arquivo .env , por favor crie -lo manualmente.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Finished',
        'finished' => 'Aplicação foi instalado com sucesso.',
        'exit' => 'Clique aqui para sair',
    ],

    'login' => 'ENTRAR',
    'signup' => 'INSCREVER-SE',
    'call_us_monday_to_friday' => 'Ligue-nos de segunda a sexta',
    'ask_us_anything' => 'Pergunte-nos qualquer coisa',
    'home' => 'Casa',
    'property_list' => 'lista de propriedades',
    'featured_property' => 'propriedade em Destaque',
    'agents' => 'agentes',
    'contact' => 'contato',
    'faq' => 'Perguntas frequentes',
    'quick_search' => 'Pesquisa rápida',
    'location' => ' Localização',
    'chose_city' => 'escolha a cidade',
    'type' => 'Digitar',
    'chose_property_type' => 'escolher o tipo de imóvel',
    'actions' => 'Ações',
    'chose_action' => 'escolher ação',
    'min_price' => 'Min. Preço',
    'chose_min' => 'escolher min',
    'max_price' => 'Max. Preço',
    'chose_max' => 'escolher max',
    'filter_now' => 'filtrar Agora',
    'recent_properties' => 'Propriedades recentes',
    'top_agents' => 'Principais agentes',
    'show_all' => 'mostre tudo',
    'featured_properties' => 'Propriedades em destaque',
    'subscribe_to_newsletter' => 'Subscrever a newsletter',
    'subscribe' => 'se inscrever',
    'we_promise_that_we_will_not_send_you_spam_messages' => 'Prometemos que não vamos enviar-lhe mensagens de spam.',
    'copyright_2016_all_rights_reserved_by' => 'Copyright 2016. Todos os Direitos Reservados por',
    'we_are_on_24_7' => 'Estamos em 24/7',
    'local' => 'Local',
    'send_us_email_on' => 'Envie e-mail ON',
    'meet_us_at' => 'Nos encontrar',
    'send_us_a_mail' => 'Envie-nos um e-mail',
    'your_name' => 'SEU NOME',
    'your_email' => 'SEU EMAIL',
    'your_subject' => 'Seu assunto',
    'your_message' => 'SUA MENSAGEM',
    'send_message' => 'ENVIAR MENSAGEM',
    'dashboard' => 'painel de instrumentos',
    'property' => 'Propriedade',
    'message' => 'Mensagem',
    'user' => 'Do utilizador',
    'site_settings' => 'Definições do site',
    'city' => 'Cidade',
    'type' => 'Digitar',
    'feature' => 'Característica',
    'purpose' => 'propósito',
    'testimony' => 'Testemunho',
    'client' => 'Cliente',
    'faq' => 'Perguntas frequentes',
    'about-us' => 'to work on',

];
