<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Kiatiaka Property Listing Installer',
    'next' => 'Next Step',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Kiatiaka Property Listing Installer',
        'message' => 'Welcome to the setup wizard.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requirements',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissions',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Environment Settings',
        'save' => 'Save .env',
        'success' => 'Your .env file settings have been saved.',
        'errors' => 'Unable to save the .env file, Please create it manually.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Finished',
        'finished' => 'Application has been successfully installed.',
        'exit' => 'Click here to exit',
    ],


    'login' => 'LOGIN',
    'signup' => 'SIGNUP',
    'call_us_monday_to_friday' => 'Call Us Monday To Friday',
    'ask_us_anything' => 'Ask Us Anything',
    'home' => 'Home',
    'property_list' => 'Property list',
    'featured_property' => 'Featured Property',
    'agents' => 'Agents',
    'contact' => 'contact',
    'faq' => 'faq',
    'quick_search' => 'Quick Search',
    'location' => 'Location',
    'chose_city' => 'chose city',
    'type' => 'Type',
    'chose_property_type' => 'chose property type',
    'actions' => 'Actions',
    'chose_action' => 'chose action',
    'min_price' => 'Min. Price',
    'chose_min' => 'chose min',
    'max_price' => 'Max. Price',
    'chose_max' => 'chose max',
    'filter_now' => 'Filter Now',
    'recent_properties' => 'Recent Properties',
    'top_agents' => 'Top Agents',
    'show_all' => 'show all',
    'featured_properties' => 'Featured Properties',
    'subscribe_to_newsletter' => 'Subscribe To Newsletter',
    'subscribe' => 'subscribe',
    'we_promise_that_we_will_not_send_you_spam_messages' => 'We promise that we will not send you spam messages.',
    'copyright_2016_all_rights_reserved_by' => 'Copyright 2016. All Rights Reserved by',
    'we_are_on_24_7' => 'WE ARE ON 24/7',
    'local' => 'Local',
    'send_us_email_on' => 'SEND US EMAIL ON',
    'meet_us_at' => 'MEET US AT',
    'send_us_a_mail' => 'Send Us a mail',
    'your_name' => 'YOUR NAME',
    'your_email' => 'YOUR EMAIL',
    'your_subject' => 'YOUR SUBJECT',
    'your_message' => 'YOUR MESSAGE',
    'send_message' => 'SEND MESSAGE',
    'dashboard' => 'Dashboard',
    'property' => 'Property',
    'message' => 'Message',
    'user' => 'User',
    'site_settings' => 'Site Settings',
    'city' => 'City',
    'type' => 'Type',
    'feature' => 'Feature',
    'purpose' => 'Purpose',
    'testimony' => 'Testimony',
    'client' => 'Client',
    'about-us' => 'about us',

];
