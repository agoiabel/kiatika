<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Installateur de Laravel',
    'next' => 'Suivant',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Bienvenue dans l’installateur...',
        'message' => 'Bienvenue dans le programme d’installation.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Prérequis',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissions',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Paramètres d’environment',
        'save' => 'Enregistrer .env',
        'success' => 'Votre fichier .env a été sauvegardé.',
        'errors' => 'Impossible d’enregistrer le fichier .env, il faut que vous le créiez manuellement.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Terminé',
        'finished' => 'L’application a été installée avec succès.',
        'exit' => 'Cliquez ici pour quitter',
    ],


    'login' => 'S\'IDENTIFIER',
    'signup' => 'S\'INSCRIRE',
    'call_us_monday_to_friday' => 'Appelez-nous du lundi au vendredi',
    'ask_us_anything' => 'Demandez-nous n\'importe quoi',
    'home' => 'Accueil',
    'property_list' => 'Liste des propriétés',
    'featured_property' => 'propriété vedette',
    'agents' => 'Agents',
    'contact' => 'contact',
    'faq' => 'FAQ',
    'quick_search' => 'Recherche rapide',
    'location' => 'Emplacement',
    'chose_city' => 'choisissez la ville',
    'type' => 'Type',
    'chose_property_type' => 'choisir le type de propriété',
    'actions' => 'actes',
    'chose_action' => 'choisir une action',
    'min_price' => 'Min. Prix',
    'chose_min' => 'choisissez min',
    'max_price' => 'Max. Prix',
    'chose_max' => 'choisir max',
    'filter_now' => 'Filtrez maintenant',
    'recent_properties' => 'Récent',
    'top_agents' => 'Top Agents',
    'show_all' => 'montre tout',
    'featured_properties' => 'Propriétés en vedette',
    'subscribe_to_newsletter' => 'S\'inscrire à la Newsletter',
    'subscribe' => 'souscrire',
    'we_promise_that_we_will_not_send_you_spam_messages' => 'Nous promettons que nous ne pourrons pas vous envoyer des messages spam.',
    'copyright_2016_all_rights_reserved_by' => 'Droit d\'auteur 2016. Tous droits réservés par',
    'we_are_on_24_7' => 'NOUS SOMMES SUR 24/7',
    'local' => 'locale',
    'send_us_email_on' => 'Nous envoyer un mail ON',
    'meet_us_at' => 'MEET US AT',
    'send_us_a_mail' => 'Envoyez-nous un e-mail',
    'your_name' => 'VOTRE NOM',
    'your_email' => 'VOTRE EMAIL',
    'your_subject' => 'VOTRE SUJET',
    'your_message' => 'VOTRE MESSAGE',
    'send_message' => 'ENVOYER LE MESSAGE',
    'dashboard' => 'Tableau de bord',
    'property' => 'Propriété',
    'message' => 'Message',
    'user' => 'Utilisateur',
    'site_settings' => 'Paramètres du site',
    'city' => 'Ville',
    'type' => 'Type',
    'feature' => 'Fonctionnalité',
    'purpose' => 'Objectif',
    'testimony' => 'Témoignage',
    'client' => 'Client',
    'about-us' => 'to work on',
];
