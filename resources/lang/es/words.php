<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Kiatika listado de propiedades del instalador',
    'next' => 'Próximo paso',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Kiatika listado de propiedades del instalador',
        'message' => 'Bienvenido al asistente de configuración.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'requisitos',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'permisos',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Configuración del entorno',
        'save' => 'guardar .env',
        'success' => 'Sus configuraciones de archivo .env se han guardado.',
        'errors' => 'No se puede guardar el archivo .env, por favor crear de forma manual.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Terminado',
        'finished' => 'La aplicación se ha instalado correctamente.',
        'exit' => 'Haga clic aquí para salir',
    ],


    'login' => 'INICIAR SESIÓN',
    'signup' => 'REGÍSTRATE',
    'call_us_monday_to_friday' => 'Llamanos lunes a viernes',
    'ask_us_anything' => 'Pregúntenos cualquier cosa',
    'home' => 'Casa',
    'property_list' => 'lista de propiedades',
    'featured_property' => 'Propiedad Destacada',
    'agents' => 'agentes',
    'contact' => 'contacto',
    'faq' => 'Preguntas más frecuentes',
    'quick_search' => 'Búsqueda rápida',
    'location' => 'Ubicación',
    'chose_city' => 'elige ciudad',
    'type' => 'Tipo',
    'chose_property_type' => 'elegir el tipo de propiedad',
    'actions' => 'Comportamiento',
    'chose_action' => 'Elige Acción',
    'min_price' => 'Min. Precio',
    'chose_min' => 'elegir min',
    'max_price' => 'Max. Precio',
    'chose_max' => 'elegir máx',
    'filter_now' => 'Ahora filtrar',
    'recent_properties' => 'Propiedades recientes',
    'top_agents' => 'Agentes superiores',
    'show_all' => 'mostrar todo',
    'featured_properties' => 'Propiedades Destacadas',
    'subscribe_to_newsletter' => 'Suscribirse al boletín',
    'subscribe' => 'suscribir',
    'we_promise_that_we_will_not_send_you_spam_messages' => 'Prometemos que no le envíe mensajes de correo basura.',
    'copyright_2016_all_rights_reserved_by' => 'Derechos de autor 2016. Todos los derechos reservados',
    'we_are_on_24_7' => 'ESTAMOS EN 24/7',
    'local' => 'Local',
    'send_us_email_on' => 'Nos envía el email EN',
    'meet_us_at' => 'Reunirse con nosotros en',
    'send_us_a_mail' => 'Envíenos un e-mail',
    'your_name' => 'TU NOMBRE',
    'your_email' => 'TU CORREO ELECTRÓNICO',
    'your_subject' => 'TU ASUNTO',
    'your_message' => 'TU MENSAJE',
    'send_message' => 'ENVIAR MENSAJE',
    'dashboard' => 'Tablero',
    'property' => 'Propiedad',
    'message' => 'Mensaje',
    'user' => 'Usuario',
    'site_settings' => 'Configuración del sitio',
    'city' => 'Ciudad',
    'type' => 'Tipo',
    'feature' => 'Característica',
    'purpose' => 'Propósito',
    'testimony' => 'Testimonio',
    'client' => 'Cliente',
    'about-us' => 'to work on',
];
