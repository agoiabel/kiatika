<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'تنصيب Laravel',
    'next' => 'متابعة',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'تنصيب Laravel',
        'message' => 'أهلا بك في صفحة تنصيب Laravel',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'المتطلبات',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'تراخيص المجلدات',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'الإعدادات',
        'save' => 'حفظ ملف .env',
        'success' => 'تم حفظ الإعدادات بنجاح',
        'errors' => 'حدث خطأ أثناء إنشاء ملف .env. رجاءا قم بإنشاءه يدويا',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'النهاية',
        'finished' => 'تم تنصيب البرنامج بنجاح...',
        'exit' => 'إضغط هنا للخروج',
    ],

    'login' => 'تسجيل الدخول',
    'signup' => 'سجل',
    'call_us_monday_to_friday' => 'اتصل بنا من الاثنين إلى الجمعة',
    'ask_us_anything' => 'أسألنا أي شيء',
    'home' => 'الصفحة الرئيسية',
    'property_list' => 'قائمة الممتلكات',
    'featured_property' => 'الملكية متميز',
    'agents' => 'وكلاء',
    'contact' => 'اتصال',
    'faq' => 'التعليمات',
    'quick_search' => 'بحث سريع',
    'location' => 'موقع',
    'chose_city' => 'اختر مدينة',
    'type' => 'اكتب',
    'chose_property_type' => 'اختيار نوع الملكية',
    'actions' => 'الإجراءات',
    'chose_action' => 'اختيار العمل',
    'min_price' => 'دقيقة. السعر',
    'chose_min' => 'اختيار دقيقة',
    'max_price' => 'ماكس. السعر',
    'chose_max' => 'اختيار ماكس',
    'filter_now' => 'ابحث الآن',
    'recent_properties' => 'خصائص الأخيرة',
    'top_agents' => 'أعلى وكلاء',
    'show_all' => 'عرض الكل',
    'featured_properties' => 'خصائص مميزة',
    'subscribe_to_newsletter' => 'اشترك في النشرة الإخبارية',
    'subscribe' => 'الاشتراك',
    'we_promise_that_we_will_not_send_you_spam_messages' => 'ونعد بأننا لن نرسل لك رسائل البريد المزعج.',
    'copyright_2016_all_rights_reserved_by' => 'حقوق الطبع والنشر 2016. جميع الحقوق محفوظة ل',
    'we_are_on_24_7' => 'نحن في 24/7',
    'local' => 'محلي',
    'send_us_email_on' => 'أرسل رسالة تابعنا على',
    'meet_us_at' => 'يجتمع لنا في',
    'send_us_a_mail' => 'أرسل لنا على البريد',
    'your_name' => 'اسمك',
    'your_email' => 'بريدك الالكتروني',
    'your_subject' => 'موضوعك',
    'your_message' => 'رسالتك',
    'send_message' => 'إرسال رسالة',
    'dashboard' => 'لوحة القيادة',
    'property' => 'الملكية',
    'message' => 'الرسالة',
    'user' => 'مستخدم',
    'site_settings' => 'إعدادات الموقع',
    'city' => 'مدينة',
    'type' => 'اكتب',
    'feature' => 'ميزة',
    'purpose' => 'غرض',
    'testimony' => 'شهادة',
    'client' => 'زبون',
    'about-us' => 'to work on',

];
