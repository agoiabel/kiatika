<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Procesul de instalare Laravel',
    'next' => 'Pasul următor',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Bun venit în procesul de instalare...',
        'message' => 'Bun venit în configurarea asistată.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Cerințe',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permisiuni',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Settări ale mediului',
        'save' => 'Salvează fișier .env',
        'success' => 'Setările tale au fost salvate în fișierul .env.',
        'errors' => 'Nu am putut salva fișierul .env, Te rugăm să-l creezi manual.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Am terminat',
        'finished' => 'Aplicația a fost instalată cu succes.',
        'exit' => 'Click aici pentru a ieși',
    ],

    'login' => 'LOGARE',
    'signup' => 'INSCRIE-TE',
    'call_us_monday_to_friday' => 'Suna-ne de luni până vineri',
    'ask_us_anything' => 'Intreaba-ne ceva',
    'home' => 'Acasă',
    'property_list' => 'lista de proprietate',
    'featured_property' => 'Proprietate Promovată',
    'agents' => 'agenţi',
    'contact' => 'a lua legatura',
    'faq' => 'FAQ',
    'quick_search' => 'Cautare rapida',
    'location' => 'Locație',
    'chose_city' => 'alege oras',
    'type' => 'Tip',
    'chose_property_type' => 'alege tipul de proprietate',
    'actions' => 'acţiuni',
    'chose_action' => 'alege o acțiune',
    'min_price' => 'Min. Preț',
    'chose_min' => 'alege min',
    'max_price' => 'Max. Preț',
    'chose_max' => 'alege max',
    'filter_now' => 'Se filtrează acum',
    'recent_properties' => 'Proprietăți recente',
    'top_agents' => 'agenţi Top',
    'show_all' => 'arata tot',
    'featured_properties' => 'Proprietăți recomandate',
    'subscribe_to_newsletter' => 'Aboneaza-te la Newsletter',
    'subscribe' => 'subscrie',
    'we_promise_that_we_will_not_send_you_spam_messages' => 'Noi promitem că nu vă vom trimite mesaje spam.',
    'copyright_2016_all_rights_reserved_by' => 'Toate drepturile de autor 2016. drepturile rezervate de',
    'we_are_on_24_7' => 'Suntem pe 24/7',
    'local' => 'Local',
    'send_us_email_on' => 'TRIMITE-NE PE EMAIL',
    'meet_us_at' => 'MEET american AT',
    'send_us_a_mail' => 'Trimite-ne un e-mail',
    'your_name' => 'NUMELE DUMNEAVOASTRĂ',
    'your_email' => 'EMAIL-UL TAU',
    'your_subject' => 'SUBIECTUL',
    'your_message' => 'MESAJUL TAU',
    'send_message' => 'TRIMITE MESAJ',
    'dashboard' => 'Bord',
    'property' => 'Proprietate',
    'message' => 'Mesaj',
    'user' => 'Utilizator',
    'site_settings' => 'Setările site-ului',
    'city' => 'Oraș',
    'type' => 'Tip',
    'feature' => 'Caracteristică',
    'purpose' => 'Scop',
    'testimony' => 'Mărturie',
    'client' => 'Client',
    'faq' => 'FAQ',
    'about-us' => 'to work on',
];
