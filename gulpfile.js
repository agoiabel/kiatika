var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass('app.scss' );

    /**
     * front end styles and scripts
     */
    mix.styles([
    	'vendor/bootstrap.css',
    	'vendor/owl.carousel.css',
    	'vendor/owl.theme.css',
    	'vendor/font-awesome.min.css',
        'vendor/flag-icon.css',
        'vendor/flexslider.css',
        'custom.css',
    ], null, 'public/css');

    mix.scripts([
        'vendor/jquery.js',
        'vendor/bootstrap.min.js', 
        'vendor/owl.carousel.min.js',
        'vendor/jquery.flexslider.js',
        'vendor/ajax.js',
        'vendor/custom.js',
    ], null, 'public/js');


    /**
     * backend layout
     */
    mix.styles([
        'vendor/bootstrap.css',
        'vendor/bootstrap-reset.css',
        'vendor/font-awesome.css',
        'vendor/owl.carousel.css',
        'vendor/slidebars.css',
        'vendor/style.css',
        'vendor/style-responsive.css',
        'vendor/fileinput.min.css',
        'vendor/flexslider.css',
    ], 'public/css/backend.css', 'public/css');


    mix.scripts([
        'vendor/jquery.js',
        'vendor/bootstrap.min.js',
        'vendor/jquery.dcjqaccordion.2.7.js',
        'vendor/jquery.scrollTo.min.js',
        'vendor/jquery.nicescroll.js',
        'vendor/jquery.sparkline.js',
        'vendor/owl.carousel.js',
        'vendor/jquery.customSelect.min.js',
        'vendor/respond.min.js',
        'vendor/common-scripts.js',
        'vendor/fileinput.min.js',

    ], 'public/js/backend.js', 'public/js');

});
