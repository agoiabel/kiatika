<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class PropertyImage extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['property_id', 'slider_path', 'property_homepage_path', 'property_single_path'];

    /**
     * get all features
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->get();
    }


    /**
     * an image belongs to a property
     * 
     * @return 
     */
    public function property()
    {
        return $this->belongsTo('App\Upload', 'property_id');
    }

    /**
     * Delete property from folder
     * 
     * @return 
     */
    public function remove_from_folder()
    {
        unlink(public_path($this->slider_path));
        unlink(public_path($this->property_homepage_path));
        unlink(public_path($this->property_single_path));
    }

    /**
     * Delete Image
     * 
     * @return 
     */
    public function deletePropertyImage()
    {
        $this->remove_from_folder();

        $this->delete();
    }
}
