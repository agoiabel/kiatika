<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['user_id', 'blog_id', 'comment'];

    /**
     * a comment belong to a user
     * 
     * @return 
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    /**
     * a comment belong to a blog
     * 
     * @return 
     */
    public function blog()
    {
    	return $this->belongsTo('App\Blog');
    }
    
}
