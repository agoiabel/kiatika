<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['title', 'icon', 'description'];

    /**
     * Get all
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->get();
    }

    /**
     * Get all
     * 
     * @return 
     */
    public function getLimit($value)
    {
        return $this->limit($value)->latest()->get();
    }
}
