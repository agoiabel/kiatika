<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
	use ModelHelper;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['question', 'answer'];

    /**
     * get all features
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->get();
    }

    /**
     * paginate faq
     * 
     * @param  $value 
     * @return         
     */
    public function paginateBy($value)
    {
        return $this->paginate($value);        
    }

    /**
     * Count all 
     * 
     * @return 
     */
    public function countAll()
    {
        return $this->count();
    }
}
