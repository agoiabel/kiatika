<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    const ADMIN = '1';
    const AGENT = '2';
    const CUSTOMER = '3';

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['name'];

    /**
     * a role has many users
     * 
     * @return 
     */
    public function users()
    {
        return $this->hasMany('App\User', 'role_id');
    }
}
