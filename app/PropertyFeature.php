<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class PropertyFeature extends Model
{
	use ModelHelper;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['upload_id', 'feature_id', 'is_present'];

    /**
     * a propertyfeature belongs to a feature
     * 
     * @return 
     */
    public function feature()
    {
    	return $this->belongsTo('App\Feature', 'feature_id');	
    }

    /**
     * a propertyfeature belongs to an upload
     * 
     * @return 
     */
    public function upload()
    {
    	return $this->belongsTo('App\Upload', 'upload_id');
    }

}
