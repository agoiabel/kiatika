<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['user_id', 'phone', 'fax', 'city', 'about', 'facebook', 'twitter', 'gplus', 'linkedin', 'image_icon', 'front_image_icon'];

    /**
     * a profile belongs to a profile
     * 
     * @return 
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * initialize type
     * 
     * @return 
     */
    public static function initModel($params)
    {
        $array = [];

        foreach ($params as $key => $value) {
            $array[$key] = $value;
        }

        return new static($array);
    }

    /**
     * create new profile
     * 
     * @return 
     */
    public function saveModel()
    {
        $this->save();

        return $this;
    }

    /**
     * find profile 
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }
    
    /**
     * update profile
     * 
     * @param  User $user   
     * @param  [] $params 
     * @return          
     */
    public function updateProfile($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        $this->saveModel();
    }
}
