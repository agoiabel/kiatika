<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['user_id', 'points'];

    /**
     * an experience belong to a user
     * 
     * @return 
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * get top four agents
     * 
     * @return 
     */
    public function getTop()
    {
        return $this->with(['user'])
                    ->orderBy('points', 'desc')
                    ->limit(4)
                    ->get();
    }


    /**
     * save new property type
     * 
     * @return 
     */
    public function saveModel()
    {
        $this->save();

        return $this;
    }

    /**
     * initialize type
     * 
     * @return 
     */
    public static function initModel($params)
    {
        $array = [];

        foreach ($params as $key => $value) {
            $array[$key] = $value;
        }

        return new static($array);
    }

    /**
     * query table 
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * update profile
     * 
     * @param  User $user   
     * @param  [] $params 
     * @return          
     */
    public function updateModel($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        $this->saveModel();
    }

    /**
     * award points
     * 
     * @return 
     */
    public function award($points)
    {
        $this->increment('points', $points);

        return $this;
    }
}
