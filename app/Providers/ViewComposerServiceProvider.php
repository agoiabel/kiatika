<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->searchFormViewComposer();

        $this->horizontalSearchFormViewComposer();

        $this->createPropertyViewComposer();

        $this->indexPageViewComposer();

        $this->sliderIndexPageViewComposer();

        $this->mapIndexPageViewComposer();

        $this->createMessageViewComposer();

        $this->messageInboxViewComposer();

        $this->messageSideMenuViewComposer();

        $this->messageSentViewComposer();

        $this->trashMessageViewComposer();

        $this->dashboardViewComposer();
     
        $this->serviceViewComposer();

        $this->service2ViewComposer();

        $this->ourPartnerViewComposer();

        $this->testimonialViewComposer();

        $this->featuredPropertyViewComposer();

        $this->serviceCountViewComposer();
        
        $this->blogViewComposer();

        $this->categoryTagViewComposer();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * search form view composer
     * 
     * @return 
     */
    public function searchFormViewComposer()
    {
        view()->composer('partials.property_search_form', 'App\Http\Composers\SearchFormViewComposer');
    }

    /**
     * Horizontal search form view composer
     * 
     * @return
     */
    public function horizontalSearchFormViewComposer()
    {
        view()->composer('partials.property_horizontal_search_form', 'App\Http\Composers\HorizontalSearchFormViewComposer');
    }

    /**
     * display property creation view composer
     * 
     * @return 
     */
    public function createPropertyViewComposer()
    {
        view()->composer('property.create', 'App\Http\Composers\CreatePropertyViewComposer');
    }

    /**
     * display index view composer
     * 
     * @return 
     */
    public function indexPageViewComposer()
    {
        view()->composer('pages.testing_index', 'App\Http\Composers\IndexPageViewComposer');
    }

    /**
     * display slider view composer
     * 
     * @return 
     */
    public function sliderIndexPageViewComposer()
    {
        view()->composer('pages.fullslider_index', 'App\Http\Composers\SliderIndexPageViewComposer');
    }

    /**
     * display index view composer
     * 
     * @return 
     */
    public function mapIndexPageViewComposer()
    {
        view()->composer('pages.map_index', 'App\Http\Composers\MapIndexPageViewComposer');
    }

    /**
     * display index view composer
     * 
     * @return 
     */
    public function createMessageViewComposer()
    {
        view()->composer('message._create', 'App\Http\Composers\CreateMessageViewComposer');
    }

    /**
     * display inbox messages view composer
     * 
     * @return 
     */
    public function messageInboxViewComposer()
    {
        view()->composer('message._inbox', 'App\Http\Composers\MessageInboxViewComposer');
    }
    
    /**
     * view message link composer
     * 
     * @return 
     */
    public function messageSideMenuViewComposer()
    {
        view()->composer('message._sideMenu', 'App\Http\Composers\MessageSideMenuViewComposer');
    }
    
    /**
     * inject view for sent messages
     * 
     * @return 
     */
    public function messageSentViewComposer()
    {
        view()->composer('message._sent', 'App\Http\Composers\MessageSentViewComposer');
    }

    /**
     * Inject trash messages
     * 
     * @return 
     */
    public function trashMessageViewComposer()
    {
        view()->composer('message._trash', 'App\Http\Composers\TrashMessageViewComposer');
    }
        
    /**
     * Inject dashboard dependencies
     * 
     * @return 
     */
    public function dashboardViewComposer()
    {
        view()->composer('dashboard', 'App\Http\Composers\DashboardViewComposer');
    }


    /**
     * Inject services 
     * 
     * @return 
     */
    public function serviceViewComposer()
    {
        view()->composer('partials.services', 'App\Http\Composers\ServiceViewComposer');
    }

    /**
     * Inject chunk services 
     * 
     * @return 
     */    
    public function service2ViewComposer()
    {
        view()->composer('partials.services_2', 'App\Http\Composers\Service2ViewComposer');
    }

    /**
     * Inject Partners 
     * 
     * @return 
     */    
    public function ourPartnerViewComposer()
    {
        view()->composer('partials.our-partner', 'App\Http\Composers\OurPartnerViewComposer');
    }

    /**
     * Inject Testimonial 
     * 
     * @return 
     */    
    public function testimonialViewComposer()
    {
        view()->composer('partials.testimonial', 'App\Http\Composers\TestimonialViewComposer');
    }

    /**
     * inject featured property
     * 
     * @return 
     */
    public function featuredPropertyViewComposer()
    {
        view()->composer('partials.featured-property', 'App\Http\Composers\FeaturedPropertyViewComposer');   
    }

    /**
     * Inject service count
     * 
     * @return 
     */
    public function serviceCountViewComposer()
    {
        view()->composer('partials.service-counter', 'App\Http\Composers\ServiceCountViewComposer');       
    }

    /**
     * Inject blog creation
     * 
     * @return 
     */
    public function blogViewComposer()
    {
        view()->composer('blog._form', 'App\Http\Composers\BlogViewComposer');       
    }


    public function categoryTagViewComposer()
    {
        view()->composer('partials.category_tag', 'App\Http\Composers\CategoryTagViewComposer');           
    }
}
