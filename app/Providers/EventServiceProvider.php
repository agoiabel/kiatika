<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserWasCreated' => [
            'App\Listeners\CreateUserProfile',
            'App\Listeners\CreateUserExperience',
            'App\Listeners\EmailConfirmationCodeToNewUser',
        ],

        'App\Events\CustomerUserWasCreated' => [
            'App\Listeners\CreateCustomerProfile',
        ],

        'App\Events\PropertyWasUploadedCompletely' => [
            'App\Listeners\UploadPropertyImage',        
            'App\Listeners\UploadPropertyFeature',
            'App\Listeners\NotifyAdminOnPropertyUpload',
            'App\Listeners\NotifySubscriber',
            'App\Listeners\AwardPointOnPropertyUpload',
        ],

        'App\Events\MessageWasCreated' => [
            'App\Listeners\PopulateSentMessage',
            'App\Listeners\PopulateReceivedMessage',
            'App\Listeners\NotifyReceiver',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
