<?php

namespace App;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    use Sluggable;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['slug', 'icon', 'name'];

    /**
     * Generate slug from 
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->name;
    }

    /**
     * get all features
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->get();
    }

    /**
     * save new property type
     * 
     * @return 
     */
    public function saveModel()
    {
        $this->save();

        return $this;
    }

    /**
     * initialize type
     * 
     * @return 
     */
    public static function initModel($params)
    {
        $array = [];

        foreach ($params as $key => $value) {
            $array[$key] = $value;
        }

        return new static($array);
    }

    /**
     * query table 
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * update profile
     * 
     * @param  User $user   
     * @param  [] $params 
     * @return          
     */
    public function updateModel($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        $this->saveModel();
    }

    /**
     * a feature has many property features
     * 
     * @return 
     */
    public function uploads()
    {
        return $this->hasMany('App\PropertyFeature', 'feature_id');
    }

}
