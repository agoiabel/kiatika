<?php 

namespace App\Services;

use App\City;

class CityStatusPublished
{
    public function __construct(City $city)
    {
        $city->status = City::Published;

        $city->save();
    }
}
