<?php 

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Auth;

class PasswordCorrect
{
    public function __construct(User $user)
    {
        Auth::login($user);
    }
}
