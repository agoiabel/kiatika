<?php 

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Hash;

class VerifyUserPassword
{

    /**
     * look up
     * 
     * @var []
     */
    public static $lookUp = [
        true => PasswordCorrect::class,
        false => PasswordNotCorrect::class,
    ];


    public static function onLogin($password, User $user)
    {
        $status = Hash::check($password, $user->password);

        $class = static::$lookUp[$status];
    
        return new $class($user);
    }
}
