<?php 

namespace App\Services;

use App\User;
use App\Exceptions\PasswordNotCorrectException;

class PasswordNotCorrect
{
    public function __construct(User $user)
    {
        throw new PasswordNotCorrectException();
    }
}
