<?php 

namespace App\Services;

class TurnToArray
{
    public function transform($command)
    {
        $input = [];

        foreach ($command as $key => $value) {
            $input[$key] = $value;
        }

        return $input;
    }

    public function from($command)
    {
    }
}
