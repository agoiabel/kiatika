<?php 

namespace App\Services;

use App\SentMessage;

class DeleteSingleSentMessage
{
    public function __construct($slug)
    {
        (new SentMessage())->findBy('slug', $slug)->delete();
    }
}
