<?php 

namespace App\Services;

use Illuminate\Http\Request;

class RestoreMessageHelper
{
    /**
     * Lookup action to take
     * 
     * @var 
     */
    public static $lookup = [
        '3' => RestoreDeleteMessage::class,
    ];


    public function __construct(Request $request)
    {
        $class = static::$lookup[$request->action];

        return ((new $class($request)));
    }
}
