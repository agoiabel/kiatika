<?php 

namespace App\Services;

use App\User;
use App\Message;
use App\Events\MessageWasCreated;
use App\Commands\SendMessageCommand;

class SendMessageToAllCustomer extends SendGroupMessage
{
    /**
     * Send message to all agent
     * 
     * @param  SendMessageCommand $command 
     * @return                       
     */
    public function sendMessageToAll(SendMessageCommand $command)
    {
        $this->command = $command;

        $this->sendGroupMessage($this->getAllCustomers());
    }

    /**
     * Get all agents
     * 
     * @return 
     */
    public function getAllCustomers()
    {
        return (new User())->getAllCustomer();
    }
}
