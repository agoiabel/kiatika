<?php 

namespace App\Services;

use App\Commands\SendMessageCommand;

class SendMessageBaseOnStringValue
{
    /**
     * return value based on lookup
     * 
     * @var []
     */
    public static $lookUp = [
        'a' => SendMessageToAllAgent::class,
        'b' => SendMessageToAllCustomer::class,
    ];

    /**
     * return redirect based on recipient
     * 
     * @param SendMessageCommand $command 
     */
    public function __construct(SendMessageCommand $command)
    {
        $class = static::$lookUp[ $command->recipient ];

        return (new $class())->sendMessageToAll($command);
    }
}
