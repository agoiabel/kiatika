<?php 

namespace App\Services;

use Illuminate\Http\Request;

class SentMessageHelper
{
    /**
     * Lookup action to take
     * 
     * @var 
     */
    public static $lookup = [
        '0' => UpdateMessageReadStatus::class,
        '1' => DeleteMessage::class,
    ];


    public function __construct(Request $request)
    {
        $class = static::$lookup[$request->action];

        return ((new $class())->sent($request));
    }
}
