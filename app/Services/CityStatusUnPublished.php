<?php 

namespace App\Services;

use App\City;

class CityStatusUnPublished
{
    public function __construct(City $city)
    {
        $city->status = City::Unpublished;

        $city->save();
    }
}
