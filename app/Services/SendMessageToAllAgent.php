<?php 

namespace App\Services;

use App\User;
use App\Commands\SendMessageCommand;

class SendMessageToAllAgent extends SendGroupMessage
{
    /**
     * Send message to all agent
     * 
     * @param  SendMessageCommand $command 
     * @return                       
     */
    public function sendMessageToAll(SendMessageCommand $command)
    {
        $this->command = $command;

        $this->sendGroupMessage($this->getAllAgents());
    }


    /**
     * Get all agents
     * 
     * @return 
     */
    public function getAllAgents()
    {
        return (new User())->getAllAgent();
    }
}
