<?php 

namespace App\Services;

use App\Blog;
use App\Commands\CreateCommentCommand;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\CustomerEmailWithDifferentNameException;

class CreateNewCustomerFromCommentPage extends CreateNewCustomer
{
	public function __construct(Blog $blog)
	{
		$this->blog = $blog;
	}

	/**
	 * create customer
	 * 
	 * @return 
	 */
	public function from(CreateCommentCommand $command)
	{
		$this->command = $command;

		$url = route('blog-single', $this->blog->findBy('id', $this->command->blog_id)->slug);

		return $this->findEmailAndCompareName($url);
	}

}