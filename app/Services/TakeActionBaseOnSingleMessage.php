<?php 

namespace App\Services;

class TakeActionBaseOnSingleMessage
{
    protected static $lookup = [
        'inbox' => 'App\Services\DeleteSingleInboxMessage',
        'sent' => 'App\Services\DeleteSingleSentMessage',
    ];

    public function __construct($slug, $message_type)
    {
        $class = static::$lookup[$message_type];

        return (new $class($slug));
    }
}
