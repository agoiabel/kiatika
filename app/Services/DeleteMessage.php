<?php 

namespace App\Services;

use App\SentMessage;
use App\ReceivedMessage;
use Illuminate\Http\Request;

class DeleteMessage
{

    public function inbox(Request $request)
    {
        foreach ($request->message_ids as $message_id) {
            (new ReceivedMessage())->findBy('id', $message_id)->delete();
        }
    }

    public function sent(Request $request)
    {
        foreach ($request->message_ids as $message_id) {
            (new SentMessage())->findBy('id', $message_id)->delete();
        }
    }
}
