<?php 

namespace App\Services;

use Carbon\Carbon;
use App\SentMessage;
use App\ReceivedMessage;
use Illuminate\Http\Request;

class UpdateMessageReadStatus
{
    public function inbox(Request $request)
    {
        foreach ($request->message_ids as $message_id) {
            $message = (new ReceivedMessage())->findBy('id', $message_id)
                                              ->updateModel($this->turnToArray());
        }
    }

    public function sent(Request $request)
    {
        foreach ($request->message_ids as $message_id) {
            $message = (new SentMessage())->findBy('id', $message_id)
                                          ->updateModel($this->turnToArray());
        }
    }

    /**
     * Turn to array
     * 
     * @return 
     */
    public function turnToArray()
    {
        $input = [];

        $input['is_read'] = true;
        $input['read_date_time'] = Carbon::now();

        return $input;
    }
}
