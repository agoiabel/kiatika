<?php 

namespace App\Services;

use App\ReceivedMessage;
use Illuminate\Http\Request;

class RestoreDeleteMessage
{
    /**
     * Restore message
     * 
     * @param Request $request 
     */
    public function __construct(Request $request)
    {
        foreach ($request->message_ids as $message) {
            (new ReceivedMessage())->findTrashBy('id', $message)->restore();
        }
    }
}
