<?php 

namespace App\Services;

use App\SentMessage;
use App\ReceivedMessage;

class MessageHelper
{
    /**
     * get message sender
     * 
     * @return 
     */
    public function getMessageSender($command)
    {
        return $this->user->firstOrCreate($this->turnUserCommandToAnArray($command));
    }

    /**
     * turn command to an array
     * 
     * @param  Command $command 
     * @return           
     */
    public function turnUserCommandToAnArray($command)
    {
        $input = [];

        $input['name'] = $this->command->name;
        $input['email'] = $this->command->email;
        $input['role_id'] = $this->command->role_id;

        return $input;
    }
}
