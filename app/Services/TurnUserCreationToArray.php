<?php 

namespace App\Services;

class TurnUserCreationToArray extends TurnToArray
{
    /**
     * Transform Collection
     * 
     * @return 
     */
    public function from($command)
    {
        return [
            'role_id' => $command->role_id,
            'city_id' => $command->city_id,
            'name' => $command->name,
            'email' => $command->email,
            'password' => $command->password,
        ];
    }
}
