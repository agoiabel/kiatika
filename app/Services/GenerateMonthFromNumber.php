<?php 

namespace App\Services;

class GenerateMonthFromNumber
{
    /**
     * Look out for expected outcome
     * 
     * @var []
     */
    public static $lookUp = [
        '00' => 'JAN',
        '01' => 'FEB',
        '02' => 'MAR',
        '03' => 'APR',
        '04' => 'MAY',
        '05' => 'JUNE',
        '06' => 'JULY',
        '07' => 'SEPT',
        '08' => 'AUG',
        '09' => 'SEPT',
        '10' => 'OCT',
        '11' => 'NOV',
        '12' => 'DEC',
    ];


	public function getMonth($value)
	{
        $month = static::$lookUp[$value];
        
        return $month;
	}
}