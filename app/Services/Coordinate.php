<?php 

namespace App\Services;

use League\Flysystem\Exception;

class Coordinate
{

    public function getCoordinate($address)
    {
        $input = [];

        $prepAddr = str_replace(' ', '+', $address);

        $geocode = @file_get_contents("https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false");

        $output = json_decode($geocode);

        $input['latitude'] = $output->results[0]->geometry->location->lat;

        $input['longitude'] = $output->results[0]->geometry->location->lng;
        
        return $input;
    }
}
