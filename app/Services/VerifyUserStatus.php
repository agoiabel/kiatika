<?php 

namespace App\Services;

class VerifyUserStatus
{

    /**
     * look up
     * 
     * @var []
     */
    public static $lookUp = [
        '0' => UserStatusIsNotVerified::class,
        '1' => UserStatusIsVerified::class,
    ];

    /**
     * factory to redirect
     * @param  user status $status 
     * 
     * @return 
     */
    public static function onLogin($status)
    {
        $class = static::$lookUp[$status];
     
        return new $class();
    }
}
