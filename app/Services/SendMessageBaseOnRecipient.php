<?php 

namespace App\Services;

use App\Commands\SendMessageCommand;

class SendMessageBaseOnRecipient
{
    /**
     * return value based on lookup
     * 
     * @var []
     */
    public static $lookUp = [
        true => SendMessageBaseOnIntegerValue::class,
        false => SendMessageBaseOnStringValue::class,
    ];

    /**
     * return redirect based on recipient
     * 
     * @param SendMessageCommand $command 
     */
    public function __construct(SendMessageCommand $command)
    {
        $class = static::$lookUp[ is_numeric($command->recipient) ? true : false ];

        return (new $class($command));
    }
}
