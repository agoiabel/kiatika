<?php 

namespace App\Services;

use App\ReceivedMessage;

class DeleteSingleInboxMessage
{
    public function __construct($slug)
    {
        (new ReceivedMessage())->findBy('slug', $slug)->delete();
    }
}
