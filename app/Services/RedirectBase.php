<?php

namespace App\Services;

use App\User;
use App\ReceivedMessage;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RedirectBase
{
    /**
     * inject recieved message
     * 
     * @param ReceivedMessage $receivedMessage 
     */
    public function __construct(ReceivedMessage $receivedMessage)
    {
        $this->receivedMessage = $receivedMessage;
    }

    /**
     * Look out for expected outcome
     * 
     * @var []
     */
    public static $lookUp = [
        'admin' => Admin::class,
        'agent' => Agent::class,
        'customer' => Customer::class,
    ];

    /**
     * redirect base on role
     * 
     * @return 
     */
    public function baseOnRole($slug)
    {
        $receivedMessage = $this->receivedMessage->findBy('slug', $slug);

        $class = static::$lookUp[$receivedMessage->receiver->role->name];

        return ((new $class())->messageInbox($receivedMessage));
    }
    
    /**
     * Redirect on user login
     * 
     * @return 
     */
    public function customerLogin(User $user)
    {
        $class = static::$lookUp[$user->role->name];

        return ((new $class())->customerLogin($user));
    }
}
