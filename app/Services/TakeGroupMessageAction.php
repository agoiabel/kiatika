<?php 

namespace App\Services;

use Illuminate\Http\Request;

class TakeGroupMessageAction
{
    /**
     * Lookup action to take
     * 
     * @var 
     */
    public static $lookup = [
        'inbox' => InboxMessageHelper::class,
        'sent' => SentMessageHelper::class,
        'restore' => RestoreMessageHelper::class,
    ];


    public function __construct(Request $request)
    {
        $class = static::$lookup[$request->message_type];

        return ((new $class($request)));
    }
}
