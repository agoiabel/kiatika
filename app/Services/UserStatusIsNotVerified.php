<?php 

namespace App\Services;

use App\Exceptions\UserStatusNotVerifiedException;

class UserStatusIsNotVerified
{
    /**
     * throw password not found exception
     */
    public function __construct()
    {
        throw new UserStatusNotVerifiedException();
    }
}
