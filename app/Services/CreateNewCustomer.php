<?php 

namespace App\Services;

use App\User;
use App\Events\CustomerUserWasCreated;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\CustomerEmailWithDifferentNameException;

abstract class CreateNewCustomer {

	/**
	 * find user and compare email
	 * 
	 * @return 
	 */
	public function findEmailAndCompareName($url)
	{

		$this->url = $url;

		try {
			
			$user = (new User())->findBy('email', $this->command->email);

			if ($user->name != $this->command->name) {
				$this->returnErrorMessageToCustomer();
			}

			if ($user->name == $this->command->name) {
				return $user;
			}

		} catch (ModelNotFoundException $e) {

			$user = $this->createNewUser();	

			event(new CustomerUserWasCreated( $user ));

			return $user;
		
		}
	}

	/**
	 * send error message to customer
	 * 
	 * @return 
	 */
	protected function returnErrorMessageToCustomer()
	{        
        throw new CustomerEmailWithDifferentNameException($this->url);
	}

	/**
	 * create new user
	 * 
	 * @return 
	 */
	protected function createNewUser()
	{
		return (new User())->initModel($this->turnUserCommandToAnArray())->saveModel();
	}

    /**
     * turn command to an array
     * 
     * @param  Command $command 
     * @return           
     */
    protected function turnUserCommandToAnArray()
    {
        $input = [];

        $input['name'] = $this->command->name;
        $input['email'] = $this->command->email;
        $input['role_id'] = $this->command->role_id;

        return $input;
    }
}