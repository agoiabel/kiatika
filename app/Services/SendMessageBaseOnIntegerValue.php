<?php 

namespace App\Services;

use App\User;
use App\Message;
use App\Events\MessageWasCreated;
use App\Commands\SendMessageCommand;
use Illuminate\Support\Facades\Auth;

class SendMessageBaseOnIntegerValue
{
    /**
     * Send message to user
     * 
     * @param SendMessageCommand $command 
     */
    public function __construct(SendMessageCommand $command)
    {
        $this->command = $command;

        event(new MessageWasCreated($this->getRecipient(), $this->saveNewMessage()));
    }

    /**
     * Get message recepient
     * 
     * @return 
     */
    public function getRecipient()
    {
        return (new User())->findBy('id', $this->command->recipient);
    }

    /**
     * save new message
     * 
     * @return 
     */
    public function saveNewMessage()
    {
        return (new Message())->initModel($this->turnMessageToAnArray())->saveModel();
    }

    /**
     * turn message command to a array
     * 
     * @return 
     */
    public function turnMessageToAnArray()
    {
        $input = [];

        $input['sender_id'] = Auth::user()->id;
        $input['receiver_id'] = $this->getRecipient()->id;
        $input['subject'] = $this->command->subject;
        $input['body'] = $this->command->body;

        return $input;
    }
}
