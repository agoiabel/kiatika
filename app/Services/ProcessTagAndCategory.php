<?php 

namespace App\Services;

use App\Tag;
use App\Blog;
use App\BlogTag;
use App\Category;

class ProcessTagAndCategory
{
	public function __construct(Blog $blog, Tag $tag, BlogTag $blogTag, Category $category)
	{
		$this->tag = $tag;

		$this->blog = $blog;

		$this->blogTag = $blogTag;

		$this->category = $category;
	}

    /**
     * Process category
     * 
     * @return 
     */
    public function processCategory($request)
    {   
        if (in_array($request->category, $this->blog->lists('id')->toArray())) {
            return $this->category->findBy('id', $request->category)->id;
        }

        return $this->category->initModel(['name' => $request->category])->saveModel()->id; 
    }

    /**
     * process tag
     * 
     * @return 
     */
    public function processTags($request, $blog_id)
    {
	    $all_tag_id = $this->tag->lists('id')->toArray(); 

        $new_tag_list = array_diff($request->tag, $all_tag_id); 

        $old_tag_list_taken_by_user = array_diff($request->tag, $new_tag_list); 

        foreach ($new_tag_list as $new_tag) {
            $old_tag_list_taken_by_user[] = $this->tag->initModel($this->mapTag($new_tag))->saveModel()->id;
        }

    	foreach ($old_tag_list_taken_by_user as $tag_id) {
    		
    		$this->blogTag->initModel(['blog_id' => $blog_id, 'tag_id' => $tag_id])->saveModel();
    
    	}
    }

    /**
     * turn tag dependencies to an array
     * 
     * @return 
     */
    protected function mapTag($new_tag)
    {
    	$input = [];

    	$input['name'] = $new_tag;

    	return $input;
    }

}