<?php 

namespace App\Services;

use App\Upload;
use App\Commands\MakeEnquireCommand;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\CustomerEmailWithDifferentNameException;

class CreateNewCustomerUserFromEnquire extends CreateNewCustomer
{
	public function __construct(Upload $property)
	{
		$this->property = $property;
	}

	/**
	 * create customer
	 * 
	 * @return 
	 */
	public function from(MakeEnquireCommand $command)
	{
		$this->command = $command;

		$url = route('property_detial', $this->property->findBy('id', $this->command->property_id)->slug);

		return $this->findEmailAndCompareName($url);
	}

}