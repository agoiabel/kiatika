<?php

namespace App\Services;

use App\User;
use App\ReceivedMessage;

class Agent
{
    /**
     * display message inbox
     * 
     * @return 
     */
    public function messageInbox(ReceivedMessage $receivedMessage)
    {
        $this->updateRecievedMessageAsRead($receivedMessage);

        return view('message.inbox-detial', compact('receivedMessage'));
    }

    /**
     * Take action if agent wants to login as a customer
     * 
     * @return 
     */
    public function customerLogin(User $user)
    {
        flash('please you need to login through this channel');

        return redirect()->route('auth.login');
    }


    /**
     * Update received message as read
     * 
     * @param  RecieveMessage $receivedMessage 
     * @return                   
     */
    protected function updateRecievedMessageAsRead($receivedMessage)
    {
        $receivedMessage->updateModel($this->generateArrayFor($receivedMessage));
    }

    /**
     * Generate array to be use to process
     * 
     * @param  RecievedMessage $receivedMessage 
     * @return                   
     */
    protected function generateArrayFor($receivedMessage)
    {
        $input = [];

        $receivedMessage['is_read'] = true;

        return $input;
    }
}
