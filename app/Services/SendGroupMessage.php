<?php 

namespace App\Services;

use App\Message;
use App\Events\MessageWasCreated;
use Illuminate\Support\Facades\Auth;

class SendGroupMessage
{
    public function sendGroupMessage($recipients)
    {
        foreach ($recipients as $recipient) {
            $message = $this->saveNewMessage($recipient);

            event(new MessageWasCreated($recipient, $message));
        }
    }

    /**
     * save new message
     * 
     * @return 
     */
    public function saveNewMessage($recipient)
    {
        return (new Message())->initModel($this->turnMessageToAnArray($recipient))->saveModel();
    }

    /**
     * turn message command to a array
     * 
     * @return 
     */
    public function turnMessageToAnArray($recipient)
    {
        $input = [];

        $input['sender_id'] = Auth::user()->id;
        $input['receiver_id'] = $recipient->id;
        $input['subject'] = $this->command->subject;
        $input['body'] = $this->command->body;

        return $input;
    }
}
