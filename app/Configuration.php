<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = [
        'title',
        'logo_image_link', 'front_banner_image_link', 'email', 'about_us', 'office_address',
        'phone_number', 'info_text', 'main_title', 'sub_title', 'currency_sign', 'favicon',
        'twitter', 'facebook', 'youtube', 'instagram', 'linkedin', 'copy_right', 'mission', 
        'vision', 'country'
    ];
}
