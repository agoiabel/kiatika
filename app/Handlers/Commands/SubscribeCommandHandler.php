<?php

namespace App\Handlers\Commands;

use App\Subscriber;
use App\Commands\SubscribeCommand;
use Illuminate\Queue\InteractsWithQueue;

class SubscribeCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * Handle the command.
     *
     * @param  SubscribeCommand  $command
     * @return void
     */
    public function handle(SubscribeCommand $command)
    {
        $this->subscriber->initModel($this->turn_to_array($command))->saveModel();
    }


    /**
     * turn subscriber objects to an array
     * 
     * @param  Command $command 
     * @return           
     */
    public function turn_to_array($command)
    {
        $input = [];

        $input['ip'] = $command->ip;
        $input['email'] = $command->email;

        return $input;
    }
}
