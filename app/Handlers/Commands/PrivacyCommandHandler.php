<?php

namespace App\Handlers\Commands;

use App\Settings;
use App\Commands\PrivacyCommand;
use Illuminate\Queue\InteractsWithQueue;

class PrivacyCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Settings $settings)
    {
        $this->setting = $settings->findBy('id', 1);
    }

    /**
     * Handle the command.
     *
     * @param  PrivacyCommand  $command
     * @return void
     */
    public function handle(PrivacyCommand $command)
    {
        $this->setting->updateSetting($this->map($command));
    }

    /**
     * map command
     * @param  Command $command 
     * @return           
     */
    public function map($command)
    {
        $input = [];

        $input['privacy_policy_title'] = $command->privacy_policy_title;
        $input['privacy_policy_description'] = $command->privacy_policy_description;

        return $input;
    }
}
