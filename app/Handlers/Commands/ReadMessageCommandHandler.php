<?php

namespace App\Handlers\Commands;

use App\Services\RedirectBase;
use App\Commands\ReadMessageCommand;
use Illuminate\Queue\InteractsWithQueue;

class ReadMessageCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(RedirectBase $redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * Handle the command.
     *
     * @param  ReadMessageCommand  $command
     * @return void
     */
    public function handle(ReadMessageCommand $command)
    {
        return $this->redirect->baseOnRole($command->slug);
    }
}
