<?php

namespace App\Handlers\Commands;

use App\Message;
use App\Commands\SendMessageCommand;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\SendMessageBaseOnRecipient;

class SendMessageCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Handle the command.
     *
     * @param  SendMessageCommand  $command
     * @return void
     */
    public function handle(SendMessageCommand $command)
    {
        (new SendMessageBaseOnRecipient($command));
    }
}
