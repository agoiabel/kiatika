<?php

namespace App\Handlers\Commands;

use App\Settings;
use App\Commands\SocialSettingCommand;
use Illuminate\Queue\InteractsWithQueue;

class SocialSettingCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Settings $settings)
    {
        $this->setting = $settings->findBy('id', 1);
    }

    /**
     * Handle the command.
     *
     * @param  SocialSettingCommand  $command
     * @return void
     */
    public function handle(SocialSettingCommand $command)
    {
        $this->setting->updateSetting($this->map($command));
    }

    /**
     * map command
     * 
     * @param  Command $command 
     * @return           
     */
    public function map($command)
    {
        $input = [];

        $input['social_facebook'] = $command->social_facebook;
        $input['social_twitter'] = $command->social_twitter;
        $input['social_linkedin'] = $command->social_linkedin;
        $input['social_gplus'] = $command->social_gplus;

        return $input;
    }
}
