<?php

namespace App\Handlers\Commands;

use App\Settings;
use App\Commands\AboutUsCommand;
use Illuminate\Queue\InteractsWithQueue;

class AboutUsCommandHandler
{

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Settings $settings)
    {
        $this->setting = $settings->findBy('id', 1);
    }


    /**
     * Handle the command.
     *
     * @param  AboutUsCommand  $command
     * @return void
     */
    public function handle(AboutUsCommand $command)
    {
        $this->setting->updateSetting($this->map($command));
    }

    /**
     * map command
     * @param  Command $command 
     * @return           
     */
    public function map($command)
    {
        $input = [];

        $input['about_us_title'] = $command->about_us_title;
        $input['about_us_description'] = $command->about_us_description;

        return $input;
    }
}
