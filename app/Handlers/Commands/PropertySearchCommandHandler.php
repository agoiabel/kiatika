<?php

namespace App\Handlers\Commands;

use App\Upload;
use App\Commands\PropertySearchCommand;
use Illuminate\Queue\InteractsWithQueue;

class PropertySearchCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Upload $upload)
    {
        $this->upload = $upload;
    }

    /**
     * Handle the command.
     *
     * @param  PropertySearchCommand  $command
     * @return void
     */
    public function handle(PropertySearchCommand $command)
    {
        return $this->upload->searchBy($command);
    }
}
