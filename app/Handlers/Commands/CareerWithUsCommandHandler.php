<?php

namespace App\Handlers\Commands;

use App\Settings;
use App\Commands\CareerWithUsCommand;
use Illuminate\Queue\InteractsWithQueue;

class CareerWithUsCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Settings $settings)
    {
        $this->setting = $settings->findBy('id', 1);
    }

    /**
     * Handle the command.
     *
     * @param  CareerWithUsCommand  $command
     * @return void
     */
    public function handle(CareerWithUsCommand $command)
    {
        $this->setting->updateSetting($this->map($command));
    }

    /**
     * map command
     * @param  Command $command 
     * @return           
     */
    public function map($command)
    {
        $input = [];

        $input['careers_with_us_title'] = $command->careers_with_us_title;
        $input['careers_with_us_description'] = $command->careers_with_us_description;

        return $input;
    }
}
