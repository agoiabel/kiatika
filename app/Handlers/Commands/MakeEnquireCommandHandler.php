<?php

namespace App\Handlers\Commands;

use App\Upload;
use App\Message;
use App\Events\MessageWasCreated;
use App\Commands\MakeEnquireCommand;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\CreateNewCustomerUserFromEnquire;

class MakeEnquireCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Guard $auth, Message $message, Upload $property, CreateNewCustomerUserFromEnquire $findOrCreateNewCustomer)
    {
        $this->auth = $auth;

        $this->message = $message;

        $this->property = $property;

        $this->findOrCreateNewCustomer = $findOrCreateNewCustomer;
    }

    /**
     * Handle the command.
     *
     * @param  MakeEnquireCommand  $command
     * @return void
     */
    public function handle(MakeEnquireCommand $command)
    {
        $this->command = $command;

        $this->user = $this->findOrCreateNewCustomer->from($this->command);

        $this->authenticateMessageSender();

        event(new MessageWasCreated($this->getMessageRecipient(), $this->saveNewMessage()));
    }

    /**
     * authenticate and remember message sender
     * @return           
     */
    public function authenticateMessageSender()
    {
        return $this->auth->login($this->user, true);
    }

    /**
     * save new message 
     * @return 
     */
    public function saveNewMessage()
    {
        return $this->message->initModel($this->turnMessageCommandToAnArray())->saveModel();
    }

    /**
     * get message recepient
     *  
     * @return           
     */
    public function getMessageRecipient()
    {
        return $this->getProperty()->agent;
    }

    /**
     * get property
     * 
     * @return           
     */
    public function getProperty()
    {
        return $this->property->findBy('id', $this->command->property_id);
    }

    /**
     * get message subject
     * 
     * @return property
     */
    public function getMessageSubject()
    {
        return 'message from'. ' ' .$this->command->name;
    }

    /**
     * turn message command to an array
     * 
     * @return 
     */
    public function turnMessageCommandToAnArray()
    {
        $input = [];

        $input['sender_id'] = $this->user->id;
        $input['receiver_id'] = $this->getMessageRecipient()->id;
        $input['subject'] = $this->getMessageSubject();
        $input['body'] = $this->command->message;

        return $input;
    }

    /**
     * turn command to an array
     * 
     * @param  Command $command 
     * @return           
     */
    public function turnUserCommandToAnArray()
    {
        $input = [];

        $input['name'] = $this->command->name;
        $input['email'] = $this->command->email;
        $input['role_id'] = $this->command->role_id;

        return $input;
    }
}
