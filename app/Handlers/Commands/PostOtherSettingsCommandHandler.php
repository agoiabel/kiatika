<?php

namespace App\Handlers\Commands;

use App\Settings;
use App\Commands\PostOtherSettingsCommand;
use Illuminate\Queue\InteractsWithQueue;

class PostOtherSettingsCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Settings $settings)
    {
        $this->setting = $settings->findBy('id', 1);
    }

    /**
     * Handle the command.
     *
     * @param  PostOtherSettingsCommand  $command
     * @return void
     */
    public function handle(PostOtherSettingsCommand $command)
    {
        $this->setting->updateSetting($this->map($command));
    }

    /**
     * map command
     * @param  Command $command 
     * @return           
     */
    public function map($command)
    {
        $input = [];

        $input['site_header_code'] = $command->site_header_code;
        $input['site_footer_code'] = $command->site_footer_code;

        return $input;
    }
}
