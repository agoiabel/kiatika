<?php

namespace App\Handlers\Commands;

use App\Comment;
use App\Commands\CreateCommentCommand;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\CreateNewCustomerFromCommentPage;

class CreateCommentCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Comment $comment, CreateNewCustomerFromCommentPage $createNewUser)
    {
        $this->comment = $comment;

        $this->createNewUser = $createNewUser;
    }

    /**
     * Handle the command.
     *
     * @param  CreateCommentCommand  $command
     * @return void
     */
    public function handle(CreateCommentCommand $command)
    {
        $user = $this->createNewUser->from($command);

        $this->createNewCommentFrom($command, $user->id);
    }

    /**
     * create new comment
     * @return 
     */
    protected function createNewCommentFrom($command, $user_id)
    {
        $this->comment->initModel(['user_id' => $user_id, 'blog_id' => $command->blog_id, 'comment' => $command->comment])->saveModel();
    }

}
