<?php

namespace App\Handlers\Commands;

use App\User;
use App\Commands\ResetUserPasswordCommand;
use Illuminate\Queue\InteractsWithQueue;

class ResetUserPasswordCommandHandler
{
    public $user;

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the command.
     *
     * @param  ResetUserPasswordCommand  $command
     * @return void
     */
    public function handle(ResetUserPasswordCommand $command)
    {
        return $this->user->updatePasswordFor($this->user->findBy('confirmation_code', $command->token), $command->password);
    }
}
