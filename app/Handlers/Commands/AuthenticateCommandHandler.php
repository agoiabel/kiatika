<?php

namespace App\Handlers\Commands;

use App\User;
use App\Services\VerifyUserStatus;
use App\Services\VerifyUserPassword;
use Illuminate\Contracts\Auth\Guard;
use App\Commands\AuthenticateCommand;
use Illuminate\Queue\InteractsWithQueue;
use App\Exceptions\UserNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthenticateCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(User $user, VerifyUserStatus $verifyUserStatus, VerifyUserPassword $verifyUserPassword)
    {
        $this->user = $user;

        $this->verifyUserStatus = $verifyUserStatus;

        $this->verifyUserPassword = $verifyUserPassword;
    }

    /**
     * Handle the command.
     *
     * @param  AuthenticateCommand  $command
     * @return void
     */
    public function handle(AuthenticateCommand $command)
    {
        try {
            $user = $this->user->findBy('email', $command->email);

            $this->verifyUserStatus->onLogin($user->status);

            $this->verifyUserPassword->onLogin($command->password, $user);

            return $user;
        } catch (ModelNotFoundException $e) {
            throw new UserNotFoundException();
        }
    }
}
