<?php

namespace App\Handlers\Commands;

use App\User;
use App\Mailers\UserMailer;
use Illuminate\Queue\InteractsWithQueue;
use App\Commands\EmailPasswordResetCommand;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\EmailNotFoundOnPasswordResetException;

class EmailPasswordResetCommandHandler
{

    public $user, $mailer;

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(User $user, UserMailer $mailer)
    {
        $this->user = $user;

        $this->mailer = $mailer;
    }

    /**
     * Handle the command.
     *
     * @param  EmailPasswordResetCommand  $command
     * @return void
     */
    public function handle(EmailPasswordResetCommand $command)
    {
        try {
            $this->mailer->sendPasswordResetLinkTo($this->updateUserConfirmationCode($command));
        } catch (ModelNotFoundException $e) {
            throw new EmailNotFoundOnPasswordResetException();
        }
    }

    /**
     * update user confornation code
     * 
     * @param  Command $command 
     * @return           
     */
    public function updateUserConfirmationCode($command)
    {
        return $this->user->updateConfirmationOnPasswordReset($this->user->findBy('email', $command->email));
    }
}
