<?php

namespace App\Handlers\Commands;

use App\Configuration;
use App\Services\FileUploader;
use Illuminate\Queue\InteractsWithQueue;
use App\Commands\PrimaryConfigurationCommand;

class PrimaryConfigurationCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Configuration $config, FileUploader $uploader)
    {
        $this->config = $config;

        $this->uploader = $uploader;
    }

    /**
     * Handle the command.
     *
     * @param  PrimaryConfigurationCommand  $command
     * @return void
     */
    public function handle(PrimaryConfigurationCommand $command)
    {
        $this->command = $command;

        $this->config->findBy('id', '1')
                     ->updateModel($this->turn_to_array());
    }

    /**
     * Turn to array
     * 
     * @return 
     */
    public function turn_to_array()
    {
        $input = [];

        $input['logo_image_link'] = $this->uploader->makeThumbnail($this->command->logo, 152, 40);
        $input['favicon'] = $this->uploader->makeThumbnail($this->command->favicon, 32, 32);
        $input['header_title'] = $this->command->header_title;
        $input['main_title'] = $this->command->main_title;
        $input['color_picker'] = $this->command->color_picker;
        $input['index_page_view'] = $this->command->index_page_view;
        $input['localization'] = $this->command->localization;
        $input['currency_sign'] = $this->command->currency_sign;
        $input['about_us'] = $this->command->about_us;

        return $input;
    }

}
