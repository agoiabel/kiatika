<?php

namespace App\Handlers\Commands;

use App\Upload;
use Illuminate\Contracts\Auth\Guard;
use App\Commands\CreatePropertyCommand;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\PropertyWasUploadedCompletely;

class CreatePropertyCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Upload $upload, Guard $auth)
    {
        $this->auth = $auth;

        $this->upload = $upload;
    }

    /**
     * Handle the command.
     *
     * @param  CreatePropertyCommand  $command
     * @return void
     */
    public function handle(CreatePropertyCommand $command)
    {
        $this->command = $command;

        $property = $this->saveProperty();

        event( new PropertyWasUploadedCompletely($property, $this->command->property_images, $this->command->features_id) );
    }

    /**
     * save property and sync with features
     * 
     * @param  Command $command 
     * @return property id
     */
    public function saveProperty()
    {
        return $this->upload->initModel( $this->map() )->saveModel();
    }


    /**
     * turn command to an array
     * 
     * @param  Command $command 
     * @return           
     */
    public function map()
    {
        $input = [];

        $input['agent_id'] = $this->auth->user()->id;
        $input['type_id'] = $this->command->type_id;
        $input['purpose_id'] = $this->command->purpose_id;
        $input['price'] = $this->command->price;
        $input['description'] = $this->command->description;
        $input['city_id'] = $this->command->city_id;
        $input['address'] = $this->command->address;

        return $input;
    }
}
