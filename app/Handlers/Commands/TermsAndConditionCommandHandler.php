<?php

namespace App\Handlers\Commands;

use App\Settings;
use App\Commands\TermsAndConditionCommand;
use Illuminate\Queue\InteractsWithQueue;

class TermsAndConditionCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Settings $settings)
    {
        $this->setting = $settings->findBy('id', 1);
    }

    /**
     * Handle the command.
     *
     * @param  TermsAndConditionCommand  $command
     * @return void
     */
    public function handle(TermsAndConditionCommand $command)
    {
        $this->setting->updateSetting($this->map($command));
    }

    /**
     * map command
     * @param  Command $command 
     * @return           
     */
    public function map($command)
    {
        $input = [];

        $input['terms_conditions_title'] = $command->terms_conditions_title;
        $input['terms_conditions_description'] = $command->terms_conditions_description;

        return $input;
    }
}
