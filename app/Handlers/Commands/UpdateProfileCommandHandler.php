<?php

namespace App\Handlers\Commands;

use App\User;
use App\Profile;
use App\Services\FileUploader;
use App\Commands\UpdateProfileCommand;
use Illuminate\Queue\InteractsWithQueue;

class UpdateProfileCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(FileUploader $fileUploader, Profile $profile, User $user)
    {
        $this->user = $user;

        $this->profile = $profile;

        $this->fileUploader = $fileUploader;
    }

    /**
     * Handle the command.
     *
     * @param  UpdateProfileCommand  $command
     * @return void
     */
    public function handle(UpdateProfileCommand $command)
    {
        $profile = $this->profile->findBy('user_id', $command->user_id);

        $profile->user->updateUser($this->mapUserData($command));

        $profile->updateProfile($this->mapProfileData($command));
    }

    /**
     * create array from profile
     * 
     * @return 
     */
    public function mapProfileData($command)
    {
        $input = [];

        $input['phone'] = $command->phone;
        $input['fax'] = $command->fax;
        $input['about'] = $command->about;
        $input['facebook'] = $command->facebook;
        $input['twitter'] = $command->twitter;
        $input['gplus'] = $command->gplus;
        $input['linkedin'] = $command->linkedin;
        $input['image_icon'] = $this->fileUploader->makeThumbnail($command->image_icon, 200, 200);
        $input['front_image_icon'] = $this->fileUploader->makeThumbnail($command->image_icon, 141, 140);

        return $input;
    }

    /**
     * create array from user
     * 
     * @return 
     */
    public function mapUserData($command)
    {
        $input = [];

        $input['email'] = $command->email;
        $input['name'] = $command->name;
        $input['city_id'] = $command->city_id;

        return $input;
    }
}
