<?php

namespace App\Handlers\Commands;

use App\User;
use App\Role;
use App\Message;
use App\Commands\ContactCommand;
use App\Events\MessageWasCreated;
use Illuminate\Contracts\Auth\Guard;
use App\Events\CustomerUserWasCreated;
use Illuminate\Queue\InteractsWithQueue;

class ContactCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(Guard $auth, User $user, Message $message)
    {
        $this->user = $user;

        $this->auth = $auth;

        $this->message = $message;
    }

    /**
     * Handle the command.
     *
     * @param  ContactCommand  $command
     * @return void
     */
    public function handle(ContactCommand $command)
    {
        $this->command = $command;

        $this->authenticateMessageSender();

        event(new CustomerUserWasCreated($this->getMessageSender()));

        event(new MessageWasCreated($this->getMessageRecipient(), $this->saveNewMessage()));
    }

    /**
     * authenticate and remember message sender
     * @return           
     */
    public function authenticateMessageSender()
    {
        return $this->auth->login($this->getMessageSender(), true);
    }

    /**
     * get message sender
     * 
     * @return 
     */
    public function getMessageSender()
    {
        return $this->user->firstOrCreate($this->turnUserCommandToAnArray());
    }

    /**
     * save new message 
     * @return 
     */
    public function saveNewMessage()
    {
        return $this->message->initModel($this->turnMessageCommandToAnArray())->saveModel();
    }

    /**
     * get message recepient
     *  
     * @return           
     */
    public function getMessageRecipient()
    {
        return $this->user->findBy('role_id', Role::ADMIN);
    }

    /**
     * get message subject
     * 
     * @return property
     */
    public function getMessageSubject()
    {
        return $this->command->subject;
    }

    /**
     * turn message command to an array
     * 
     * @return 
     */
    public function turnMessageCommandToAnArray()
    {
        $input = [];

        $input['sender_id'] = $this->getMessageSender()->id;
        $input['subject'] = $this->getMessageSubject();
        $input['body'] = $this->command->message;

        return $input;
    }

    /**
     * turn command to an array
     * 
     * @param  Command $command 
     * @return           
     */
    public function turnUserCommandToAnArray()
    {
        $input = [];

        $input['name'] = $this->command->name;
        $input['email'] = $this->command->email;
        $input['role_id'] = $this->command->role_id;

        return $input;
    }
}
