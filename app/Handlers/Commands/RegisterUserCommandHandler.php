<?php

namespace App\Handlers\Commands;

use App\User;
use App\Events\UserWasCreated;
use App\Commands\RegisterUserCommand;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\TurnUserCreationToArray;

class RegisterUserCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(User $user, TurnUserCreationToArray $turnToArray)
    {
        $this->user = $user;

        $this->turnToArray = $turnToArray;
    }

    /**
     * Handle the command.
     *
     * @param  RegisterUserCommand  $command
     * @return void
     */
    public function handle(RegisterUserCommand $command)
    {
        $this->command = $command;

        event(new UserWasCreated($this->createNewUser()));
    }

    /**
     * create new user
     * 
     * @param  Command $command 
     * @return User          
     */
    public function createNewUser()
    {
        return $this->user
                    ->initModel($this->turnToArray->from($this->command))
                    ->saveModel();
    }
}
