<?php

namespace App;

use App\Traits\Sluggable;
use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	use ModelHelper, Sluggable;

	/**
	 * Generate slug from title
	 * 
	 * @return 
	 */
	public function getSluggableString()
	{
		return $this->name;
	}

	/**
	 * attr that can be mass assigned
	 * 
	 * @var [type]
	 */
    protected $fillable = ['slug', 'name'];

    /**
     * get all
     * 
     * @return 
     */
    public function getAll()
    {
    	return $this->get();	
    }

    /**
     * a category has many blogs
     * 
     * @return 
     */
    public function blog()
    {
    	return $this->hasMany('App\Blog', 'category_id');
    }

    
}
