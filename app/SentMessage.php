<?php

namespace App;

use App\Traits\Sluggable;
use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SentMessage extends Model
{

    use SoftDeletes, ModelHelper, Sluggable;

    /**
     * generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->message->id .' '. $this->message->subject;
    }

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['sender_id', 'message_id', 'slug', 'is_read', 'read_date_time', 'deleted_at'];

    protected $dates = ['deleted_at'];

    /**
     * a sent message belongs to message
     * 
     * @return 
     */
    public function message()
    {
        return $this->belongsTo('App\Message', 'message_id');
    }

    /**
     * a sent message belongs to a user
     * 
     * @return 
     */
    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    /**
     * Get sent messages by user
     * 
     * @return 
     */
    public function getMessageForUser($sender_id)
    {
        return $this->where('sender_id', $sender_id)->paginate(10);
    }

    /**
     * FindBy 
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * count all 
     * 
     * @return 
     */
    public function countAll($sender_id)
    {
        return $this->where('sender_id', $sender_id)->count();
    }
}
