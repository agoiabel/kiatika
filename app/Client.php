<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['name', 'image_path'];

    /**
     * get all features
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->get();
    }

    /**
     * get latest from value
     * 
     * @return 
     */
    public function getLimit($value)
    {
        return $this->limit($value)->latest()->get();
    }

    /**
     * Count all 
     * 
     * @return 
     */
    public function countAll()
    {
        return $this->count();
    }
}
