<?php 

namespace App\Http\Composers;

use App\Testimonial;
use Illuminate\Contracts\View\View;

class TestimonialViewComposer
{

    public function __construct(Testimonial $testimonial)
    {
        $this->testimonial = $testimonial;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('testimonials', $this->testimonial->getLimit(4));
    }
    
}
