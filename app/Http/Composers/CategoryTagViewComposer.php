<?php 

namespace App\Http\Composers;

use App\Tag;
use App\Category;
use Illuminate\Contracts\View\View;

class CategoryTagViewComposer
{
    public function __construct(Tag $tag, Category $category)
    {
        $this->tag = $tag;

        $this->category = $category;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('tags', $this->tag->getAll());
        
        $view->with('categories', $this->category->getAll());
    }
    
}
