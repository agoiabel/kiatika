<?php 

namespace App\Http\Composers;

use App\Service;
use Illuminate\Contracts\View\View;

class ServiceViewComposer
{
    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {        
        $view->with('services', (new Service())->getLimit(3));
    }
}
