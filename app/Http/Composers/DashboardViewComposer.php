<?php 

namespace App\Http\Composers;

use App\User;
use App\Client;
use App\Upload;
use App\Testimonial;
use App\SentMessage;
use App\ReceivedMessage;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\Auth\Guard;

class DashboardViewComposer
{
    public function __construct(Guard $auth, Upload $upload)
    {
        $this->auth = $auth;

        $this->upload = $upload;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $user_id = $this->auth->user()->id;

        $view->with('allPropertyCount', $this->upload->countAll());
        
        $view->with('ownPropertyCount', (new Upload())->countAllFor($user_id));

        $view->with('allFeaturedProperty', (new Upload())->countAllFeatured());

        $view->with('ownFeaturedProperty', (new Upload())->countAllFeaturedFor($user_id));

        $view->with('publishedPropertyCount', (new Upload())->countAllPublished());

        $view->with('ownPublishedPropertyCount', (new Upload())->countAllPublishedFor($user_id));
    
        $view->with('ownMessageInbox', (new ReceivedMessage())->countAll($user_id));
    
        $view->with('ownSentMessage', (new SentMessage())->countAll($user_id));

        $view->with('agentCount', (new User())->countAllAgent());

        $view->with('customerCount', (new User())->countAllCustomer());

        $view->with('allClientCount', (new Client())->countAll());

        $view->with('allTestimonialCount', (new Testimonial())->countAll());
    }
}
