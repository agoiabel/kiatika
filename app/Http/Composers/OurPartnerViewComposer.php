<?php 

namespace App\Http\Composers;

use App\Client;
use Illuminate\Contracts\View\View;

class OurPartnerViewComposer
{

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('clients', $this->client->getLimit(6));
    }
    
}
