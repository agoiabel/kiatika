<?php 

namespace App\Http\Composers;

use App\User;
use Illuminate\Contracts\View\View;

class CreateMessageViewComposer
{

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('agents', $this->user->getAllAgent());
    }
}
