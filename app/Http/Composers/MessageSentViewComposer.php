<?php 

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class MessageSentViewComposer extends MessageViewComposer
{

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('sentMessages', $this->sentMessage->getMessageForUser(Auth::user()->id));
    }
}
