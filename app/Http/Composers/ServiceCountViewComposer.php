<?php 

namespace App\Http\Composers;

use App\User;
use App\Upload;
use Illuminate\Contracts\View\View;

class ServiceCountViewComposer
{

    public function __construct(User $user, Upload $property)
    {
        $this->user = $user;

        $this->property = $property;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('satisfied_customers', $this->user->countAllCustomer());
        $view->with('team_members', $this->user->countAllAgent());
        $view->with('all_properties', $this->property->getAllFeatured(9));
        $view->with('featured_properties', $this->property->countAllPublished());
    }
    
}
