<?php 

namespace App\Http\Composers;

use App\City;
use App\Purpose;
use App\Feature;
use App\PropertyType;

abstract class ViewComposer
{
    public function __construct(City $city, Purpose $purpose, PropertyType $type, Feature $feature)
    {
        $this->type = $type;

        $this->city = $city;

        $this->feature = $feature;

        $this->purpose = $purpose;
    }
}
