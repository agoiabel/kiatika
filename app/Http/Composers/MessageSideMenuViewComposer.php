<?php 

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class MessageSideMenuViewComposer extends MessageViewComposer
{

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('recievedMessageCount', $this->message->getUnreadMessagesCountForCurrent(Auth::user()->id));
    }
}
