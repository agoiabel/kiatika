<?php 

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class TrashMessageViewComposer extends MessageViewComposer
{

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('recievedMessages',
                    $this->message->getTrashMessagesForCurrent(Auth::user()->id));
    }
}
