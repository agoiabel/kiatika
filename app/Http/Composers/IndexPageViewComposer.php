<?php 

namespace App\Http\Composers;

use App\Client;
use App\Upload;
use App\Experience;
use App\Testimonial;
use Illuminate\Contracts\View\View;

class IndexPageViewComposer
{

    public function __construct(Testimonial $testimonial, Upload $property, Client $client, Experience $experience)
    {
        $this->client = $client;

        $this->property = $property;

        $this->experience = $experience;

        $this->testimonial = $testimonial;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('clients', $this->client->getAll());

        $view->with('latest_properties', $this->property->getAllLatest());

        $view->with('testimonials', $this->testimonial->getAll());

        $view->with('experiences', $this->experience->getTop());
    }
    
}
