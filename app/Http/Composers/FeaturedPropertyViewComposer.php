<?php 

namespace App\Http\Composers;

use App\Upload;
use Illuminate\Contracts\View\View;

class FeaturedPropertyViewComposer
{

    public function __construct(Upload $property)
    {
        $this->property = $property;
    }

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('latest_featured_properties', $this->property->getAllFeatured(9));
    }
    
}
