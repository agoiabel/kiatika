<?php 

namespace App\Http\Composers;

use App\SentMessage;
use App\ReceivedMessage;

class MessageViewComposer
{

    public function __construct(ReceivedMessage $message, SentMessage $sentMessage)
    {
        $this->message = $message;

        $this->sentMessage = $sentMessage;
    }
}
