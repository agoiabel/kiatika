<?php 

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

class CreatePropertyViewComposer extends ViewComposer
{

    /**
     * inject into view
     * 
     * @return 
     */
    public function compose(View $view)
    {
        $view->with('types', $this->type->getAll());

        $view->with('cities', $this->city->getAll());

        $view->with('purposes', $this->purpose->getAll());

        $view->with('features', $this->feature->getAll());
    }
}
