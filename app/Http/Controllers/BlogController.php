<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Commands\CreateBlogCommand;
use App\Http\Controllers\Controller;

class BlogController extends ApplicationController
{
	/**
	 * display index view
	 * 
	 * @return 
	 */
    public function index(Blog $blog)
    {
        $blogs = $blog->getAllFor($this->user->id);
        
        if ($this->user->isAdmin()) {
            $blogs = $blog->getAll();    
        }

    	return view('blog.index', compact('blogs'));
    }

    /**
     * display view to create new blog
     * 
     * @return 
     */
    public function create()
    {
    	return view('blog.create');
    }

    /**
     * process the creation of blog
     * 
     * @return 
     */
    public function store(BlogRequest $request)
    {
    	$this->dispatchFrom(CreateBlogCommand::class, $request);

        flash()->success('your blog was created successfully');

        return redirect()->route('blog.index');
    }

    /**
     * delete blog 
     * 
     * @return 
     */
    public function destroy(Blog $blog, $id)
    {
        $blog->findBy('id', $id)->delete();

        flash()->success('the blog was deleted successfully');

        return redirect()->back();
    }
}
