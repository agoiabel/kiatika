<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Commands\CreateNewTagCommand;

class TagController extends ApplicationController
{
	/**
	 * display category index view
	 * 
	 * @return 
	 */
    public function index(Tag $tag)
    {
    	$tags = $tag->getAll();

    	return view('tag.index', compact('tags'));
    }

    /**
     * create new category
     * 
     * @return 
     */
    public function create()
    {
    	return view('tag.create');
    }

    /**
     * Process category
     * 
     * @return 
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required'
    	]);

    	$this->dispatchFrom(CreateNewTagCommand::class, $request);

    	flash()->success('category was created successfully');

    	return redirect()->route('tag.index');
    }

    /**
     * destroy category
     * 
     * @return 
     */
    public function destroy(Tag $tag, $id)
    {
        $tag->findBy('id', $id)->delete();

        flash()->success('category wsas deleted successfully');

        return redirect()->route('tag.index');
    }

}
