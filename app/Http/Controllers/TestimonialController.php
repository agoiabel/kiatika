<?php

namespace App\Http\Controllers;

use App\Testimonial;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TestimonialRequest;
use App\Commands\CreateTestimonialCommand;
use App\Commands\UpdateTestimonialCommand;

class TestimonialController extends ApplicationController
{
    /**
     * display all testimonial
     * 
     * @return 
     */
    public function index(Testimonial $testimonial)
    {
        $testimonials = $testimonial->getAll();

        return view('testimonial.index', compact('testimonials'));
    }

    /**
     * display view to create testimonial
     * 
     * @return 
     */
    public function create()
    {
        return view('testimonial.create');
    }

    /**
     * process testimonial upload
     * 
     * @param  Request $request 
     * @return            
     */
    public function store(TestimonialRequest $request)
    {
        $request['image'] = $request->image;

        $this->dispatchFrom(CreateTestimonialCommand::class, $request);

        flash('Testimonial was uploaded successfully');

        return redirect()->route('testimonial.index');
    }

    /**
     * display view to update testimonial
     * 
     * @return 
     */
    public function edit($slug, Testimonial $Testimonial)
    {
        $testimonial = $Testimonial->findBy('slug', $slug);

        return view('testimonial.edit', compact('testimonial'));
    }

    /**
     * update testimonial
     * 
     * @return 
     */
    public function update(TestimonialRequest $request)
    {
        $this->dispatchFrom(UpdateTestimonialCommand::class, $request);

        flash('Testimonial was updated successfully');

        return redirect()->route('testimonial.index');
    }

    /**
     * delete testimonial
     * 
     * @return 
     */
    public function destroy($slug, Testimonial $testimonial)
    {
        $testimonial->findBy('slug', $slug)->delete();

        flash('Testimonial was deleted successfully');

        return redirect()->route('testimonial.index');
    }
}
