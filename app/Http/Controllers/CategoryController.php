<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends ApplicationController
{
	/**
	 * display category index view
	 * 
	 * @return 
	 */
    public function index(Category $category)
    {
    	$categories = $category->getAll();

    	return view('category.index', compact('categories'));
    }

    /**
     * create new category
     * 
     * @return 
     */
    public function create()
    {
    	return view('category.create');
    }

    /**
     * Process category
     * 
     * @return 
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required'
    	]);

    	$this->dispatchFrom(CreateNewCategoryCommand::class, $request);

    	flash()->success('category was created successfully');

    	return redirect()->route('category.index');
    }

    /**
     * destroy category
     * 
     * @return 
     */
    public function destroy(Category $category, $id)
    {
        $category->findBy('id', $id)->delete();

        flash()->success('category wsas deleted successfully');

        return redirect()->route('category.index');
    }

}
