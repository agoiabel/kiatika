<?php

namespace App\Http\Controllers;

use App\Service;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Commands\WhatWeDoCommand;
use App\Http\Controllers\Controller;
use App\Http\Requests\WhatWeDoRequest;
use App\Commands\MissionVisionCommand;
use App\Http\Requests\MisionVisionRequest;
use App\Http\Requests\ConfigContactRequest;
use App\Http\Requests\PrimaryConfigRequest;
use App\Commands\ContactConfigurationCommand;
use App\Commands\PrimaryConfigurationCommand;

class ConfigurationController extends ApplicationController
{
    /**
     * display view to configure primary info
     * 
     * @return 
     */
    public function primary()
    {
        return view('configuration.primary');
    }

    /**
     * Process the primary site configuration
     * 
     * @return 
     */
    public function postPrimary(PrimaryConfigRequest $request)
    {
        $request['logo'] = $request->file('logo');
        $request['favicon'] = $request->file('favicon');

        $this->dispatchFrom(PrimaryConfigurationCommand::class, $request);

        flash()->success('your configuration was updated successfully');

        return redirect()->back();
    }

    /**
     * display view to configure contact setting
     * 
     * @return 
     */
    public function contact()
    {
        return view('configuration.contact');
    }

    /**
     * Process the contact configuration
     * 
     * @return 
     */
    public function postContact(ConfigContactRequest $request)
    {
        $this->dispatchFrom(ContactConfigurationCommand::class, $request);

        flash()->success('your configuration was updated successfully');

        return redirect()->back();
    }

    /**
     * display view to configure contact setting
     * 
     * @return 
     */
    public function what_we_do()
    {
        return view('configuration.what_we_do');
    }

    /**
     * Process what we do
     * 
     * @return 
     */
    public function post_what_we_do(WhatWeDoRequest $request)
    {
        $this->dispatchFrom(WhatWeDoCommand::class, $request);

        flash()->success('your configuration was updated successfully');

        return redirect()->back();
    }

    /**
     * display all what to do
     * 
     * @return 
     */
    public function what_we_do_index(Service $service)
    {
        $services = $service->getAll();

        return view('configuration.what_we_do_index', compact('services'));
    }

    /**
     * display view to show mission and vision
     * 
     * @return 
     */
    public function getMissionVision()
    {
        return view('configuration.mission_vision');
    }

    /**
     * Process mission and vision
     * 
     * @return 
     */
    public function postMissionVision(MisionVisionRequest $request)
    {
        $this->dispatchFrom(MissionVisionCommand::class, $request);

        flash()->success('Your mission and vision was successfully uploaded');

        return redirect()->back();
    }

    /**
     * destroy faq
     * 
     * @return 
     */
    public function destroy($id, Service $service)
    {
        $service->findBy('id', $id)->delete();

        flash()->success('service deleted successfully');

        return redirect()->route('what_we_do.index');
    }


    /**
     * change the local setting from frontend
     * 
     * @return 
     */
    public function changeLocal($slug)
    {
        session()->put('local', $slug);

        return redirect()->back();
    }

}
