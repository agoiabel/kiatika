<?php

namespace App\Http\Controllers;

use App\Purpose;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PurposeRequest;
use App\Commands\CreatePurposeCommand;
use App\Commands\UpdatePurposeCommand;
use App\Commands\DeletePurposeCommand;

class PurposeController extends ApplicationController
{
    /**
     * display all properties
     * 
     * @return 
     */
    public function index(Purpose $purpose)
    {
        $purposes = $purpose->getAll();

        return view('purpose.index', compact('purposes'));
    }

    /**
     * create new property
     * 
     * @return 
     */
    public function create()
    {
        return view('purpose.create');
    }

    /**
     * process create new purpose creation
     * 
     * @return 
     */
    public function store(PurposeRequest $request)
    {
        $this->dispatchFrom(CreatePurposeCommand::class, $request);

        flash('property purpose was created');

        return redirect()->route('purpose.index');
    }

    /**
     * edit property
     * 
     * @return 
     */
    public function edit($slug, Purpose $purpose)
    {
        $property_purpose = $purpose->findBy('slug', $slug);

        return view('purpose.edit', compact('property_purpose'));
    }

    /**
     * update property type
     * 
     * @return 
     */
    public function update(PurposeRequest $request)
    {
        $this->dispatchFrom(UpdatePurposeCommand::class, $request);

        flash('Property purpose was updated successfully');

        return redirect()->route('purpose.index');
    }

    /**
     * delete property type
     * 
     * @return 
     */
    public function destroy($slug, Request $request)
    {
        $request['slug'] = $slug;

        $this->dispatchFrom(DeletePurposeCommand::class, $request);

        flash('Property type deleted successfully');

        return redirect()->route('purpose.index');
    }
}
