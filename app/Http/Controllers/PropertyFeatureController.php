<?php

namespace App\Http\Controllers;

use App\Feature;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FeatureRequest;
use App\Commands\CreateFeatureCommand;
use App\Commands\UpdateFeatureCommand;
use App\Commands\DeleteFeatureCommand;

class PropertyFeatureController extends ApplicationController
{
    /**
     * display all properties
     * 
     * @return 
     */
    public function index(Feature $feature)
    {
        $features = $feature->getAll();

        return view('feature.index', compact('features'));
    }

    /**
     * create new property
     * 
     * @return 
     */
    public function create()
    {
        return view('feature.create');
    }

    /**
     * process create new feature creation
     * 
     * @return 
     */
    public function store(FeatureRequest $request)
    {
        $this->dispatchFrom(CreateFeatureCommand::class, $request);

        flash('property feature was created');

        return redirect()->route('propertyFeature.index');
    }

    /**
     * edit property
     * 
     * @return 
     */
    public function edit($slug, Feature $feature)
    {
        $propertyFeature = $feature->findBy('slug', $slug);

        return view('feature.edit', compact('propertyFeature'));
    }

    /**
     * update property type
     * 
     * @return 
     */
    public function update(FeatureRequest $request)
    {
        $this->dispatchFrom(UpdateFeatureCommand::class, $request);

        flash('Property feature was updated successfully');

        return redirect()->route('propertyFeature.index');
    }

    /**
     * delete property type
     * 
     * @return 
     */
    public function destroy($slug, Request $request)
    {
        $request['slug'] = $slug;

        $this->dispatchFrom(DeleteFeatureCommand::class, $request);

        flash('Property type deleted successfully');

        return redirect()->route('propertyType.index');
    }
}
