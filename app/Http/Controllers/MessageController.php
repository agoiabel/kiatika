<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ReceivedMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Commands\ReadMessageCommand;
use Illuminate\Support\Facades\Auth;
use App\Commands\SendMessageCommand;
use App\Http\Requests\MessageRequest;
use App\Commands\ReplyMessageCommand;
use App\Http\Requests\ReplyMessageRequest;
use App\Commands\DeleteSingleMessageCommand;
use App\Commands\RegisterGroupActionCommand;
use App\Services\TakeActionBaseOnSingleMessage;
use App\Http\Requests\GroupMessageActionRequest;

class MessageController extends ApplicationController
{
    /**
     * display all messages
     * 
     * @return 
     */
    public function inbox()
    {
        return view('message.inbox');
    }

    /**
     * admin send message
     * 
     * @param  MessageRequest $request 
     * @return                   
     */
    public function compose(MessageRequest $request)
    {
        $this->dispatchFrom(SendMessageCommand::class, $request);
    }

    /**
     * display single message detial
     * 
     * @return 
     */
    public function inboxDetial($slug, Request $request)
    {
        $request['slug'] = $slug;

        return $this->dispatchFrom(ReadMessageCommand::class, $request);
    }

    /**
     * Reply message
     * 
     * @param  Request $request 
     * @return            
     */
    public function reply(ReplyMessageRequest $request)
    {
        $this->dispatchFrom(ReplyMessageCommand::class, $request);
    }

    /**
     * Handle group action
     * 
     * @return 
     */
    public function groupAction(GroupMessageActionRequest $request)
    {
        $this->dispatchFrom(RegisterGroupActionCommand::class, $request);

        return redirect()->back();
    }

    /**
     * Delete message
     * 
     * @return 
     */
    public function destroy($slug, $message_type)
    {
        (new TakeActionBaseOnSingleMessage($slug, $message_type));

        return redirect()->back();
    }

    /**
     * Sent messages
     * 
     * @return 
     */
    public function sent()
    {
        return view('message.sent');
    }

    /**
     * display all trash message
     * 
     * @return 
     */
    public function trash()
    {
        return view('message.trash');
    }

    /**
     * Restore message
     * 
     * @return 
     */
    public function restore(ReceivedMessage $receivedMessage, $slug)
    {
        $receivedMessage->findTrashBy('slug', $slug)->restore();

        return redirect()->back();
    }
}
