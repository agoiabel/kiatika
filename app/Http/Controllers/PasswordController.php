<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordRequest;
use App\Commands\ResetUserPasswordCommand;
use App\Commands\EmailPasswordResetCommand;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PasswordController extends ApplicationController
{        
    /**
     * display view to collect password
     * 
     * @return 
     */
    public function getEmail()
    {
        return view('auth.password');
    }
    
    /**
     * process the sending email request
     * 
     * @param  Request $request 
     * @return            
     */
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $this->dispatchFrom(EmailPasswordResetCommand::class, $request);

        flash()->success('please check your email to reset your password');

        return redirect()->back();
    }

   /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset($token, User $user)
    {
        try {
            $user->findBy('confirmation_code', $token);

            return view('auth.reset', compact('token'));

        } catch (ModelNotFoundException $e) {

            flash()->error('it seems your token has expired, insert your email below to get new token');

            return redirect()->route('passwordReset.getEmail');
        }
    }
    
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(PasswordRequest $request, User $user)
    {
        $this->dispatchFrom(ResetUserPasswordCommand::class, $request);

        flash()->success('your password has been updated successfully, you can login with new password now');

        return redirect()->route('auth.login');
    }
}
