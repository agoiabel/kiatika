<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\RedirectBase;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Commands\AuthenticateCommand;
use App\Commands\CustomerLoginCommand;
use App\Http\Requests\CustomerLoginRequest;

class AuthenticationController extends ApplicationController
{
    /**
     * display login view
     * 
     * @return 
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * process the authentication
     * 
     * @param  Request $request 
     * @return            
     */
    public function postLogin(LoginRequest $request)
    {
        $user = $this->dispatchFrom(AuthenticateCommand::class, $request);

        return redirect()->intended('dashboard');
    }

    /**
     * log user out
     * 
     * @return 
     */
    public function logout(Guard $auth)
    {
        $auth->logout($this->user);

        return redirect()->route('testing_index');
    }

    /**
     * Display customer login view
     * 
     * @return 
     */
    public function getCustomerLogin()
    {
        return view('auth.customer');
    }

    /**
     * Process customer AuthenticationController
     * 
     * @return 
     */
    public function postCustomerLogin(CustomerLoginRequest $request, RedirectBase $redirect)
    {
        $user = $this->dispatchFrom(CustomerLoginCommand::class, $request);

        $redirect->customerLogin($user);
    }

}
