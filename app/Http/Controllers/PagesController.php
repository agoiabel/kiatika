<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Tag;
use App\User;
use App\Blog;
use App\Upload;
use App\BlogTag;
use App\Comment;
use App\Category;
use App\Experience;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Coordinate;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Commands\PropertySearchCommand;
use App\Http\Requests\PropertySearchRequest;

class PagesController extends ApplicationController
{
    /**
     * display home page
     * check App\Http\Composers\IndexPageViewComposer
     * 
     * @param  City   $city 
     * @return 
     */
    public function index(Coordinate $coordinate)
    {
        $index_view = getcong('index_page_view');

        if ($index_view == 'pages.map_index') {
            $longLat = $coordinate->getCoordinate(getcong('office_address'));

            return view($index_view, compact('longLat'));
        }

        return view($index_view);

        // return RedirectIndex::from(getcong('index_page_view'));
    }

    /**
     * display all agents
     * 
     * @return 
     */
    public function agent(User $user, Upload $property)
    {
        $properties = $property->getAll();

        $agents = $user->getAllExceptEndUser(8);

        return view('pages.agent', compact('agents', 'properties'));
    }

    /**
     * Property listing
     * 
     * @return 
     */
    public function propertylist(Upload $property)
    {
        $properties = $property->getAllPublished();

        return view('pages.property', compact('properties'));
    }

    /**
     * featured property list
     * 
     * @return 
     */
    public function featuredPropertylist(Upload $property)
    {
        $properties = $property->getAllPublishedFeatured();

        return view('pages.featured-property', compact('properties'));
    }

    /**
     * search for property
     * 
     * @return 
     */
    public function propertysearch(PropertySearchRequest $request)
    {
        $query = $request->except("page");
        
        $properties = $this->dispatchFrom(PropertySearchCommand::class, $request);

        return view('pages.propertylist', compact('properties', 'query'));
    }

    /**
     * display single property
     * 
     * @return 
     */
    public function propertydetial($slug, Upload $property, Request $request, Coordinate $coordinate)
    {
        $property = $property->findBy('slug', $slug);

        $property_count = $property->where('agent_id', $property->agent_id)->count();

        $similar_properties = $property->where('agent_id', $property->agent_id)->where('city_id', $property->city_id)->whereNotIn('id', [$property->id])->limit(3)->latest()->get();

        $longLat = $coordinate->getCoordinate($property->address.' '.$property->city->city_name.' '.getcong('country'));

        return view('pages.propertydetial', compact('property', 'longLat', 'similar_properties', 'property_count'));
    }

    /**
     * display about us page
     * check App\Http\Composers\AboutUsViewComposer
     * 
     * @return 
     */
    public function aboutUs()
    {
        return view('pages.about-us');
    }

    /**
     * display services page
     * 
     * @return 
     */
    public function services()
    {
        return view('pages.services');
    }

    /**
     * display faq page
     * 
     * @return 
     */
    public function faq(Faq $faq)
    {
        $faqs = $faq->paginateBy(10);

        return view('pages.faq', compact('faqs'));
    }


    /**
     * display view to show all blog
     * 
     * @return 
     */
    public function blog(Blog $blog)
    {
        $blogs = $blog->latest()->paginate(5);

        return view('pages.blog', compact('blogs'));    
    }

    /**
     * display view to show single blog
     * 
     * @return 
     */
    public function blog_single($slug, Comment $comment)
    {
        $blog = (new Blog())->findBy('slug', $slug); 

        $comments = $comment->where('blog_id', $blog->id)->get();

        return view('pages.blog-single', compact('blog', 'comments'));
    }

    /**
     * display all category
     * 
     * @return 
     */
    public function category($slug, Category $category, Blog $blog)
    {
        $blogs = $blog->where('category_id', $category->findBy('slug', $slug)->id)->latest()->paginate(10);

        return view('pages.blog', compact('blogs'));    
    }

    /**
     * display all tags
     * 
     * @return 
     */
    public function tag($slug, Tag $tag, BlogTag $blogTag)
    {
        $blogTags = $blogTag->where('tag_id', $tag->findBy('slug', $slug)->id)->latest()->paginate(10);

        return view('pages.blog-tag', compact('blogTags'));    
    }


}

