<?php 

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = Auth::user();

        view()->share('currentUser', Auth::user());

        view()->share('signedIn', Auth::user());
    }
}