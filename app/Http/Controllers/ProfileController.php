<?php

namespace App\Http\Controllers;

use App\User;
use App\City;
use App\Profile;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\Commands\UpdateProfileCommand;
use App\Commands\ChangePasswordCommand;

class ProfileController extends ApplicationController
{

    /**
     * display user profile
     * 
     * @return 
     */
    public function index(Profile $profile)
    {
        $profile = $profile->findBy('user_id', $this->user->id);

        return view('profile.index', compact('profile'));
    }

    /**
     * update profile
     * 
     * @return 
     */
    public function update(City $city, Profile $profile)
    {
        $city_list = $city->getAll();

        $profile = $profile->findBy('user_id', $this->user->id);

        return view('profile.update_testing', compact('city_list', 'profile'));
    }

    /**
     * update users profile 
     * 
     * @return 
     */
    public function postUpdate(ProfileRequest $request)
    {
        $request['image_icon'] = $request->file('image_icon');

        $this->dispatchFrom(UpdateProfileCommand::class, $request);

        flash('your profile was updated sucessfully');

        return redirect()->route('profile.index');
    }
 
    /**
     * change password
     * 
     * @return 
     */
    public function change(PasswordRequest $request, User $users)
    {
        $this->dispatchFrom(ChangePasswordCommand::class, $request);

        flash('Your password was changed sucessfully');

        return redirect()->back();
    }
}
