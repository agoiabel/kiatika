<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use App\Http\Controllers\Controller;
use App\Commands\CreateClientCommand;

class ClientController extends ApplicationController
{
    /**
     * display all clients
     * 
     * @return 
     */
    public function index(Client $client)
    {
        $clients = $client->getAll();

        return view('client.index', compact('clients'));
    }

    /**
     * display view to create new 
     * 
     * @return 
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * process client upload
     * 
     * @return 
     */
    public function store(ClientRequest $request)
    {
        $this->dispatchFrom(CreateClientCommand::class, $request);

        flash('new client added successfully');

        return redirect()->route('client.index');
    }

    /**
     * destroy client
     * 
     * @return 
     */
    public function destroy($id, Client $client)
    {
        $client->findBy('id', $id)->delete();

        flash('new client deleted successfully');

        return redirect()->route('client.index');
    }

}
