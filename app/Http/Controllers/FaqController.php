<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\FaqRequest;
use App\Commands\CreateFaqCommand;
use App\Http\Controllers\Controller;

class FaqController extends ApplicationController
{
    /**
     * display all questions
     * 
     * @return 
     */
    public function index(FAQ $faq)
    {
        $faqs = $faq->getAll();

        return view('faq.index', compact('faqs'));
    }

    /**
     * display view to create new 
     * 
     * @return 
     */
    public function create()
    {
        return view('faq.create');
    }

    /**
     * process faq upload
     * 
     * @return 
     */
    public function store(FaqRequest $request)
    {
        $this->dispatchFrom(CreateFaqCommand::class, $request);

        flash('new faq added successfully');

        return redirect()->route('faq.index');
    }

    /**
     * destroy faq
     * 
     * @return 
     */
    public function destroy($id, Faq $faq)
    {
        $faq->findBy('id', $id)->delete();

        flash()->success('new faq deleted successfully');

        return redirect()->route('faq.index');
    }

}
