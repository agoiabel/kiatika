<?php

namespace App\Http\Controllers;

use App\City;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\RedirectBase;
use App\Http\Controllers\Controller;
use App\Commands\RegisterUserCommand;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RegistrationController extends ApplicationController
{
    /**
     * display view to register
     * 
     * @return 
     */
    public function getRegister(City $city)
    {
        $cities = $city->where('status', '1')->orderBy('city_name')->get();

        return view('auth.register', compact('cities'));
    }

    /**
     * process the registration
     * 
     * @return 
     */
    public function postRegister(RegistrationRequest $request)
    {
        $this->dispatchFrom(RegisterUserCommand::class, $request);

        flash()->success('Please verify your account. We\'ll send a verification link to the email address');

        return redirect()->back();
    }

    /**
     * process the email confirmation
     * 
     * @return 
     */
    public function confirmEmail($confirmation_code, User $user)
    {
        try {
            $user->findBy('confirmation_code', $confirmation_code)->confirmEmail();

            flash()->success('Confirmation successful..., you can now login');

            return redirect()->route('auth.login');
        } catch (ModelNotFoundException $e) {
            return redirect()->route('auth.register');
        }
    }
}
