<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends ApplicationController
{
    /**
     * display all users
     * 
     * @return 
     */
    public function index(User $user)
    {
        $users = $user->getAllExceptEndUser(10);

        return view('users.allUsers', compact('users'));
    }

    /**
     * search user by letter
     * 
     * @return 
     */
    public function search($slug, User $user)
    {
        $users = $user->findByName($slug);

        return view('users.allUsers', compact('users'));
    }
}
