<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Coordinate;
use App\Commands\ContactCommand;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;

class ContactController extends ApplicationController
{
    /**
     * display contact page
     * 
     * @return 
     */
    public function index(Coordinate $coordinate)
    {
        $longLat = $coordinate->getCoordinate(getcong('office_address'));
        
        return view('pages.contact', compact('longLat'));
    }

    /**
     * Process sending the message
     * 
     * @return 
     */
    public function postContact(ContactRequest $request)
    {
        $this->dispatchFrom(ContactCommand::class, $request);

        flash('your message was sent successfully');

        return redirect()->back();
    }
}
