<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Commands\SubscribeCommand;
use App\Http\Controllers\Controller;

class SubscribeController extends ApplicationController
{
    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $request['ip'] = $request->ip();

        $this->dispatchFrom(SubscribeCommand::class, $request);
    }

    /**
     * display all subscribers
     * 
     * @return 
     */
    public function index()
    {
        #todo, displaying all subscribers     		
    }
}
