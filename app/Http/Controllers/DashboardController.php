<?php

namespace App\Http\Controllers;

use App\Upload;
use App\Http\Requests;
use Illuminate\Http\Request;

class DashboardController extends ApplicationController
{
    /**
     * display dashboard
     * 
     * check App\Http\Composer\DashboardViewComposer;
     * 
     * @return 
     */
    public function testing_dashboard()
    {
        return view('dashboard');
    }
}
