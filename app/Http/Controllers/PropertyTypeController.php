<?php

namespace App\Http\Controllers;

use App\PropertyType;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyTypeRequest;
use App\Commands\CreatePropertyTypeCommand;
use App\Commands\DeletePropertyTypeCommand;
use App\Commands\UpdatePropertyTypeCommand;

class PropertyTypeController extends ApplicationController
{
    /**
     * display all property
     * 
     * @return 
     */
    public function index(PropertyType $propertyType)
    {
        $types = $propertyType->getAll();

        return view('propertyType.index', compact('types'));
    }

    /**
     * create new property type
     * 
     * @return 
     */
    public function create()
    {
        return view('propertyType.create');
    }

    /**
     * process the property type creation
     * 
     * @return 
     */
    public function store(PropertyTypeRequest $request)
    {
        $this->dispatchFrom(CreatePropertyTypeCommand::class, $request);

        flash('property type created successfully');

        return redirect()->route('propertyType.index');
    }

    /**
     * edit property
     * 
     * @return 
     */
    public function edit($slug, PropertyType $propertyType)
    {
        $type = $propertyType->findBy('slug', $slug);

        return view('propertyType.edit', compact('type'));
    }

    /**
     * update property type
     * 
     * @return 
     */
    public function update(PropertyTypeRequest $request)
    {
        $this->dispatchFrom(UpdatePropertyTypeCommand::class, $request);

        flash('Property type updated successfully');

        return redirect()->route('propertyType.index');
    }

    /**
     * delete property type
     * 
     * @return 
     */
    public function destroy($slug, Request $request)
    {
        $request['slug'] = $slug;

        $this->dispatchFrom(DeletePropertyTypeCommand::class, $request);

        flash('Property type deleted successfully');

        return redirect()->route('propertyType.index');
    }
}
