<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;
use App\Commands\CreateCityCommand;
use App\Commands\UpdateCityCommand;
use App\Commands\DeleteCityCommand;
use App\Http\Controllers\Controller;
use App\Commands\UpdateCityStatusCommand;

class CityController extends ApplicationController
{
    /**
     * Display all cities
     *
     * @return \Illuminate\Http\Response
     */
    public function index(City $city)
    {
        $cities = $city->getAll();

        return view('city.index', compact('cities'));
    }

    /**
     * Show the page for creating new page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        $this->dispatchFrom(CreateCityCommand::class, $request);

        flash('City was created successfully');

        return redirect()->route('city.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city, $slug)
    {
        $city = $city->findBy('slug', $slug);

        return view('city.edit', compact('city'));
    }

    /**
     * Update city.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request)
    {
        $this->dispatchFrom(UpdateCityCommand::class, $request);

        flash('Your city was updated successfully');

        return redirect()->route('city.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug, Request $request)
    {
        $request['slug'] = $slug;

        $this->dispatchFrom(DeleteCityCommand::class, $request);

        flash('The city was deleted successfully');

        return redirect()->route('city.index');
    }

    /**
     * update status
     * 
     * @return 
     */
    public function status($slug, Request $request)
    {
        $request['slug'] = $slug;

        $this->dispatchFrom(UpdateCityStatusCommand::class, $request);

        flash('The city status was updated successfully');

        return redirect()->route('city.index');
    }
}
