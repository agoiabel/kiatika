<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MainAdminController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = Auth::user();

        view()->share('currentUser', $this->user);

        view()->share('signedIn', $this->user);
    }
}
