<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Commands\MakeEnquireCommand;
use App\Http\Requests\ContactRequest;

class EnquireController extends ApplicationController
{
    public function enquire(ContactRequest $request)
    {
        $this->dispatchFrom(MakeEnquireCommand::class, $request);

        flash()->success('Delivered successfully, Agent will contact you through email');

        return redirect()->back();
    }
}
