<?php

namespace App\Http\Controllers;

use App\Upload;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Commands\CreatePropertyCommand;
use App\Commands\PropertyStatusCommand;
use App\Commands\DeletePropertyCommand;
use App\Commands\DeletePropertyImageCommand;
use App\Http\Requests\UploadPropertyRequest;
use App\Commands\CreatePropertyImageCommand;
use App\Commands\UpdatePropertyFeaturedCommand;

class UploadController extends ApplicationController
{
    /**
     * display view to show all properties
     * 
     * @return 
     */
    public function index(Upload $upload)
    {
        $properties = $upload->getAllFor($this->user->id);

        return view('property.index', compact('properties'));
    }
    
    /**
     * display view to create new property
     * 
     * @return 
     */
    public function create()
    {
        return view('property.create');
    }

    /**
     * process the property upload
     * 
     * @param  UploadPropertyRequest $request 
     * @return                          
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type_id' => 'required',
            'purpose_id' => 'required',
            'city_id' => 'required',
            'price' => 'required|numeric',
            'description' => 'required|max:400',
        ]);

        $this->dispatchFrom(CreatePropertyCommand::class, $request);

        flash('property was uploaded successfully');

        return redirect()->route('upload.index');
    }
        
    /**
     * update property featured
     * 
     * @return 
     */
    public function featured(Request $request)
    {
        $this->dispatchFrom(UpdatePropertyFeaturedCommand::class, $request);

        flash('property marked as featured');

        return redirect()->back();
    }

    /**
     * Show property detial
     * 
     * @param string $slug   
     * @param  Upload $upload 
     * @return 
     */
    public function show($slug, Upload $upload)
    {
        $property = $upload->findBy('slug', $slug);

        return view('property.show', compact('property'));
    }

    /**
     * delete image
     * 
     * @param  Request $request 
     * @return            
     */
    public function destroyImage(Request $request)
    {
        $this->dispatchFrom(DeletePropertyImageCommand::class, $request);

        flash('property image deleted successfully');

        return redirect()->back();
    }

    /**
     * delete property
     * 
     * @return 
     */
    public function destroy($slug, Request $request)
    {
        $request['slug'] = $slug;

        $this->dispatchFrom(DeletePropertyCommand::class, $request);

        flash('property deleted successfully');

        return redirect()->back();
    }

    /**
     * Take action base on publish
     * 
     * @param  string $slug 
     * @return        
     */
    public function publish($slug, Request $request)
    {
        $request['slug'] = $slug;

        $this->dispatchFrom(PropertyStatusCommand::class, $request);

        flash('property status updated successfully');

        return redirect()->back();
    }
}
