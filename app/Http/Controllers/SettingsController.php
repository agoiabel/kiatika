<?php

namespace App\Http\Controllers;

use App\Settings;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Commands\AboutUsCommand;
use App\Commands\PrivacyCommand;
use App\Http\Requests\TermsRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\CareerRequest;
use App\Http\Requests\PrivacyRequest;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\AboutUsRequest;
use App\Commands\CareerWithUsCommand;
use App\Commands\SocialSettingCommand;
use App\Commands\CommentSettingCommand;
use App\Commands\GeneralSettingCommand;
use App\Commands\PostOtherSettingsCommand;
use App\Commands\TermsAndConditionCommand;
use App\Http\Requests\SocialSettingRequest;
use App\Http\Requests\GeneralSettingsRequest;
use App\Http\Requests\postOtherSettingsRequest;

class SettingsController extends ApplicationController
{
    /**
     * display view for settings
     * 
     * @return 
     */
    public function settings(Settings $settings)
    {
        $settings = $settings->findBy('id', 1);
        
        return view('admin.settings.settings', compact('settings'));
    }
    
    /**
     * Process general settings update
     * 
     * @param  GeneralSettingsRequest $request 
     * @return                           
     */
    public function postGeneral(GeneralSettingsRequest $request)
    {
        $request['site_logo'] = $request->file('site_logo');
        $request['site_favicon'] = $request->file('site_favicon');

        $this->dispatchFrom(GeneralSettingCommand::class, $request);

        flash('Settings successfully updated!');

        return redirect()->back();
    }


    /**
     * process the social update
     * 
     * @return 
     */
    public function postSocial(SocialSettingRequest $request)
    {
        $this->dispatchFrom(SocialSettingCommand::class, $request);
    }
    

    /**
     * process the comment update
     * 
     * @return 
     */
    public function postComment(CommentRequest $request)
    {
        $this->dispatchFrom(CommentSettingCommand::class, $request);
    }

    /**
     * process the about us page
     * 
     * @return 
     */
    public function postAboutUs(AboutUsRequest $request)
    {
        $this->dispatchFrom(AboutUsCommand::class, $request);
    }

    /**
     * process the career with us
     * 
     * @param  CareerRequest $request 
     * @return                  
     */
    public function postCareer(CareerRequest $request)
    {
        $this->dispatchFrom(CareerWithUsCommand::class, $request);
    }
    
    /**
     * process the terms and condition
     * 
     * @return 
     */
    public function postTerms(TermsRequest $request)
    {
        $this->dispatchFrom(TermsAndConditionCommand::class, $request);
    }

    /**
     * process the privacy policy
     * 
     * @param  PrivacyRequest $request 
     * @return                   
     */
    public function postPrivacy(PrivacyRequest $request)
    {
        $this->dispatchFrom(PrivacyCommand::class, $request);
    }

    /**
     * process the header footer section
     * 
     * @param  postOtherSettingsRequest $request 
     * @return                             
     */
    public function postOtherSettings(postOtherSettingsRequest $request)
    {
        $this->dispatchFrom(PostOtherSettingsCommand::class, $request);
    }
}
