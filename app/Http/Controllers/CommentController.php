<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Commands\CreateCommentCommand;
use App\Http\Requests\BlogCommentRequest;

class CommentController extends ApplicationController
{
	/**
	 * process storing of comment
	 * 
	 * @return 
	 */
    public function store(BlogCommentRequest $request)
    {
    	$this->dispatchFrom(CreateCommentCommand::class, $request);

    	flash()->success('Your comment was created successfully');

    	return redirect()->back();
    }

}
