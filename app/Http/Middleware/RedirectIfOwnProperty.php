<?php

namespace App\Http\Middleware;

use Closure;
use App\Upload;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfOwnProperty
{
    public function __construct(Guard $auth, Upload $property)
    {
        $this->auth = $auth;

        $this->property = $property;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if ($this->property->findBy('slug', $request->slug)->agent_id != $this->auth->user()->id) {
            return redirect()->back();
        }

        return $next($request);
    }
}
