<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;

class Localization
{

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $local = getcong('localization');

        if (session()->get('local')) {
            $local = session()->get('local');
        }

        $this->application->setLocale($local);
        
        return $next($request);
    }
}
