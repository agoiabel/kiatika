<?php

namespace App\Http\Middleware;

use Closure;
use App\ReceivedMessage;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfMessageReceiver
{
    public function __construct(ReceivedMessage $receivedMessage, Guard $auth)
    {
        $this->auth = $auth;

        $this->receivedMessage = $receivedMessage;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (is_null($this->auth->user())) {
            flash()->error('Please you need to login with the email you used when making enquiry');

            return redirect()->guest('customer/login');
        }

        $receiver_id = $this->receivedMessage->findBy('slug', $request->slug)
                            ->receiver_id;

        if ($receiver_id != $this->auth->user()->id) {
            return redirect()->back();
        }

        return $next($request);
    }
}
