<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;

class RedirectIfInstalled
{

    public function __construct(Application $app) 
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $this->installed() ) {
            
            $this->setLocalFromBrowserLanguage();

            return redirect('install');
        }

        return $next($request);
    }

    /**
     * check if install file exists
     * 
     * @return 
     */
    protected function installed()
    {
        return file_exists(storage_path('installed'));
    }

    /**
     * set local
     */
    protected function setLocalFromBrowserLanguage()
    {
        $supportedLanguages = ['ar', 'en', 'es', 'fr', 'pt', 'ro'];

        $languages = explode(',', substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));

        foreach ($languages as $language) 
        {
            if (in_array($language, $supportedLanguages))
            {
                $this->app->setLocale($language);

                break;
            }

            $this->app->setLocale('en');            
        }
    }

}
