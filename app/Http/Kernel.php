<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'admin' => \App\Http\Middleware\RedirectIfAdmin::class,
        'agent' => \App\Http\Middleware\RedirectIfAgent::class,
        'customer' => \App\Http\Middleware\RedirectIfCustomer::class,
        'admin_and_agent' => \App\Http\Middleware\RedirectIfAdminAndAgent::class,
        'message_receiver' => \App\Http\Middleware\RedirectIfMessageReceiver::class,
        'installed' => \App\Http\Middleware\RedirectIfInstalled::class,
        'localization' => \App\Http\Middleware\Localization::class,
        'own_property' => \App\Http\Middleware\RedirectIfOwnProperty::class,
    ];
}
