<?php 

namespace App\Http;

class Flash
{	
	/**
	 * set success message notification
	 * 
	 * @param  Message $message 
	 * @return           
	 */
	public function success($message)
	{
		session()->flash('success_message', $message);	
	}	

	/**
	 * set error message notification
	 * 
	 * @param  Message $message 
	 * @return           
	 */
	public function error($message)
	{
		session()->flash('error_message', $message);	
	}	

	/**
	 * set error message notification
	 * 
	 * @param  Message $message 
	 * @return           
	 */
	public function info($message)
	{
		session()->flash('info_message', $message);	
	}
}