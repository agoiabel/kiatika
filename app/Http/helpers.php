<?php

use App\User;
use App\Upload;
use App\Configuration;
use App\Services\GenerateMonthFromNumber;

function getcong($key)
{
    $config = Configuration::findOrFail('1');

    return $config->$key;
}

function flash($message = null)
{
    $flash = app('App\Http\Flash');

    return $flash;
}


if (!function_exists('classActivePathPublic')) {
    function classActivePublicPath($path)
    {
        $path = explode('.', $path);
        $segment = 1;
        foreach ($path as $p) {
            if ((request()->segment($segment) == $p) == false) {
                return '';
            }
            $segment++;
        }
        return ' active';
    }
}

    function words ($value, $words = 150, $end = '...')
    {
        return \Illuminate\Support\Str::words($value, $words, $end);
    }

function readStatus($status)
{
    if ($status == '0') {
        return 'unread';
    }

    return 'read';
}

function publish_status_action_on(Upload $property)
{
    if ($property->status == Upload::POSITIVE) {
        return "<a href=\"http://propertynew.dev\property/upload/publish/$property->slug\" class=\"btn btn-danger btn-xs btn-circle\"><i class=\"fa fa-hand-o-down\" aria-hidden=\"true\"></i></a>";
    }

    return "<a href=\"http://propertynew.dev\property/upload/publish/$property->slug\" class=\"btn btn-success btn-xs btn-circle\"><i class=\"fa fa-hand-o-up\" aria-hidden=\"true\"></i></a>";
}

function property_feature_status($status) {
    if ($status == '0') {
        return 'no';
    }
    return 'yes';
}


function agent_experience_rate($rate) {
    if ($rate <= '1') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i>
        <i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i> ";
    }
    if ($rate <= '5') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i>
        <i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i> ";
    }
    if ($rate <= '10') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i>
        <i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i> ";
    }
    if ($rate <= '20') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i>
        <i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i> ";
    }
    if ($rate <= '25') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i>
        <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i> ";
    }
    if ($rate <= '30') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i>
        <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i> ";
    }
    if ($rate <= '40') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i>
        <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa fa-star-o\" aria-hidden=\"true\"></i> ";
    }
    if ($rate >= '50') {
        return " <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i>
        <i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i><i class=\"fa fa-star\" aria-hidden=\"true\"></i> ";
    }
}

function blog_published_date($date) {
    $complete_date = explode('-', $date);

    return $complete_date[2];
}

function blog_published_month($date) {
    $complete_date = explode('-', $date);

    return (new GenerateMonthFromNumber())->getMonth($complete_date[1]);   
}