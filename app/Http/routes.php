<?php

Route::group(['middleware' => ['installed']], function () {

    Route::group(['middleware' => ['localization']], function () {

        get('/', ['as' => 'testing_index', 'uses' => 'PagesController@index']);
        get('propertylist', ['as' => 'propertylist', 'uses' => 'PagesController@propertylist']);
        get('feature-propertylist', ['as' => 'feature-propertylist', 'uses' => 'PagesController@featuredPropertylist']);
        get('agent', ['as' => 'agent', 'uses' => 'PagesController@agent']);
        get('about-us', ['as' => 'about-us', 'uses' => 'PagesController@aboutUs']); 
        get('contact', ['as' => 'contact', 'uses' => 'ContactController@index']);
        get('propertysearch', ['as' => 'propertysearch', 'uses' => 'PagesController@propertysearch']);
        get('faq', ['as' => 'faq', 'uses' => 'PagesController@faq']);

        get('blog', ['as' => 'blog', 'uses' => 'PagesController@blog']);
        get('blog-single/{slug}', ['as' => 'blog-single', 'uses' => 'PagesController@blog_single']);
        get('blog-category/{slug}', ['as' => 'category-blog', 'uses' => 'PagesController@category']);
        get('blog-tag/{slug}', ['as' => 'blog-tag', 'uses' => 'PagesController@tag']);

        post('comment', ['as' => 'comment.store', 'uses' => 'CommentController@store']);

        get('propertydetial/{slug}', ['as' => 'property_detial', 'uses' => 'PagesController@propertydetial']);
        post('enquire', ['as' => 'enquire', 'uses' => 'EnquireController@enquire']);

        get('change-local/{slug}', ['as' => 'change-local', 'uses' => 'ConfigurationController@changeLocal']);
        get('services', ['as' => 'services', 'uses' => 'PagesController@services']); 

        post('contact', ['as' => 'postContact', 'uses' => 'ContactController@postContact']);


        post('subscribe', ['as' => 'subscribe', 'uses' => 'SubscribeController@subscribe']);


        get('customer/login', ['as' => 'customer.login', 'uses' => 'AuthenticationController@getCustomerLogin']);
        post('customer/login', ['as' => 'customer.postLogin', 'uses' => 'AuthenticationController@postCustomerLogin']);
        get('auth/login', ['as' => 'auth.login', 'uses' => 'AuthenticationController@getLogin']);
        post('auth/login', ['as' => 'auth.postLogin', 'uses' => 'AuthenticationController@postLogin']);
        get('auth/logout', ['as' => 'auth.logout', 'uses' => 'AuthenticationController@logout']);

        get('auth/register', ['as' => 'auth.register', 'uses' => 'RegistrationController@getRegister']);
        post('auth/register', ['as' => 'auth.postRegister', 'uses' => 'RegistrationController@postRegister']);
        get('auth/register/confirm/{confirmation_code}', ['as' => 'auth.confirmEmail', 'uses' => 'RegistrationController@confirmEmail']);

        get('password/email', ['as' => 'passwordReset.getEmail', 'uses' => 'PasswordController@getEmail']);
        post('password/email', ['as' => 'passwordReset.postEmail', 'uses' => 'PasswordController@postEmail']);

        get('password/reset/{token}', ['as' => 'passwordReset.getToken', 'uses' => 'PasswordController@getReset']);
        post('password/reset', ['as' => 'passwordReset.postReset', 'uses' => 'PasswordController@postReset']);


        Route::group(['middleware' => ['auth']], function () {

            Route::group(['middleware' => ['admin']], function () {

                get('configure/primary', ['as' => 'config.primary', 'uses' => 'ConfigurationController@primary']);
                get('configure/contact', ['as' => 'config.contact', 'uses' => 'ConfigurationController@contact']);
                get('configure/what_we_do', ['as' => 'config.what_we_do', 'uses' => 'ConfigurationController@what_we_do']);
                get('configure/mission_vision', ['as' => 'config.mission_vision', 'uses' => 'ConfigurationController@getMissionVision']);

                post('configure/primary', ['as' => 'config.postPrimary', 'uses' => 'ConfigurationController@postPrimary']);
                post('configure/contact', ['as' => 'config.postContact', 'uses' => 'ConfigurationController@postContact']);
                post('configure/what_we_do', ['as' => 'config.post_what_we_do', 'uses' => 'ConfigurationController@post_what_we_do']);
                post('configure/mission_vision', ['as' => 'config.postMissionVision', 'uses' => 'ConfigurationController@postMissionVision']);

                get('what_we_do/index', ['as' => 'what_we_do.index', 'uses' => 'ConfigurationController@what_we_do_index']);
                get('what_we_do/destroy/{id}', ['as' => 'what_we_do.destroy', 'uses' => 'ConfigurationController@destroy']);

                get('city/index', ['as' => 'city.index', 'uses' => 'CityController@index']);
                get('city/create', ['as' => 'city.create', 'uses' => 'CityController@create']);
                post('city/store', ['as' => 'city.store', 'uses' => 'CityController@store']);
                get('city/edit/{slug}', ['as' => 'city.edit', 'uses' => 'CityController@edit']);
                post('city/update', ['as' => 'city.update', 'uses' => 'CityController@update']);
                get('city/delete/{slug}', ['as' => 'city.destroy', 'uses' => 'CityController@destroy']);
                get('city/status/{slug}', ['as' => 'city.status', 'uses' => 'CityController@status']);

                get('property/type/index', ['as' => 'propertyType.index', 'uses' => 'PropertyTypeController@index']);
                get('property/type/create', ['as' => 'propertyType.create', 'uses' => 'PropertyTypeController@create']);
                post('property/type/create', ['as' => 'propertyType.store', 'uses' => 'PropertyTypeController@store']);
                get('property/type/edit/{slug}', ['as' => 'propertyType.edit', 'uses' => 'PropertyTypeController@edit']);
                post('property/type/update', ['as' => 'propertyType.update', 'uses' => 'PropertyTypeController@update']);
                get('property/type/{slug}', ['as' => 'propertyType.destroy', 'uses' => 'PropertyTypeController@destroy']);

                get('property/feature/index', ['as' => 'propertyFeature.index', 'uses' => 'PropertyFeatureController@index']);
                get('property/feature/create', ['as' => 'propertyFeature.create', 'uses' => 'PropertyFeatureController@create']);
                post('property/feature/create', ['as' => 'propertyFeature.store', 'uses' => 'PropertyFeatureController@store']);
                get('property/feature/edit/{slug}', ['as' => 'propertyFeature.edit', 'uses' => 'PropertyFeatureController@edit']);
                post('property/feature/update', ['as' => 'propertyFeature.update', 'uses' => 'PropertyFeatureController@update']);
                get('property/feature/delete/{slug}', ['as' => 'propertyFeature.destroy', 'uses' => 'PropertyFeatureController@destroy']);

                get('property/purpose/index', ['as' => 'purpose.index', 'uses' => 'PurposeController@index']);
                get('property/purpose/create', ['as' => 'purpose.create', 'uses' => 'PurposeController@create']);
                post('property/purpose/create', ['as' => 'purpose.store', 'uses' => 'PurposeController@store']);
                get('property/purpose/edit/{slug}', ['as' => 'purpose.edit', 'uses' => 'PurposeController@edit']);
                post('property/purpose/update', ['as' => 'purpose.update', 'uses' => 'PurposeController@update']);
                get('property/purpose/{slug}', ['as' => 'purpose.destroy', 'uses' => 'PurposeController@destroy']);

                get('property/feature/{slug}', ['as' => 'property.featured', 'uses' => 'UploadController@featured']);

                get('testimonial/index', ['as' => 'testimonial.index', 'uses' => 'TestimonialController@index']);
                get('testimonial/create', ['as' => 'testimonial.create', 'uses' => 'TestimonialController@create']);
                post('testimonial/create', ['as' => 'testimonial.store', 'uses' => 'TestimonialController@store']);
                get('testimonial/edit/{slug}', ['as' => 'testimonial.edit', 'uses' => 'TestimonialController@edit']);
                post('testimonial/update', ['as' => 'testimonial.update', 'uses' => 'TestimonialController@update']);
                get('testimonial/delete/{slug}', ['as' => 'testimonial.destroy', 'uses' => 'TestimonialController@destroy']);

                get('client/index', ['as' => 'client.index', 'uses' => 'ClientController@index']);
                get('client/create', ['as' => 'client.create', 'uses' => 'ClientController@create']);
                post('client/create', ['as' => 'client.store', 'uses' => 'ClientController@store']);
                get('client/delete/{id}', ['as' => 'client.destroy', 'uses' => 'ClientController@destroy']);

                get('subscribe/index', ['as' => 'subscriber.index', 'uses' => 'SubscriberController@index']);

                get('faq/index', ['as' => 'faq.index', 'uses' => 'FaqController@index']);
                get('faq/create', ['as' => 'faq.create', 'uses' => 'FaqController@create']);
                post('faq/create', ['as' => 'faq.store', 'uses' => 'FaqController@store']);
                get('faq/delete/{id}', ['as' => 'faq.destroy', 'uses' => 'FaqController@destroy']);

            });

            Route::group(['middleware' => ['admin_and_agent']], function () {

                get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@testing_dashboard']);
            
                get('property/upload/index', ['as' => 'upload.index', 'uses' => 'UploadController@index']);
                get('property/upload/create', ['as' => 'upload.create', 'uses' => 'UploadController@create']);
                post('property/upload/create.', ['as' => 'upload.store', 'uses' => 'UploadController@store']);

                get('property/show/{slug}', ['middleware' => 'own_property', 'as' => 'upload.show', 'uses' => 'UploadController@show']);
                post('property/image/delete', ['as' => 'upload.image.delete', 'uses' => 'UploadController@destroyImage']);
                get('property/upload/delete/{slug}', ['as' => 'upload.destroy', 'uses' => 'UploadController@destroy']);
                get('property/upload/publish/{slug}', ['as' => 'upload.publish', 'uses' => 'UploadController@publish']);
                post('property/featured', ['as' => 'upload.featured', 'uses' => 'UploadController@featured']);

                get('users', ['as' => 'users', 'uses' => 'UserController@index']);
                get('users/{slug}', ['as' => 'users.search', 'uses' => 'UserController@search']);

                post('message/compose', ['as' => 'message.send', 'uses' => 'MessageController@compose']);
                get('message/inbox', ['as' => 'message.index', 'uses' => 'MessageController@inbox']);

                post('message/messageGroupAction', ['as' => 'messageGroupAction', 'uses' => 'MessageController@groupAction']);

                get('message/destroy/{slug}/{type}', ['as' => 'message.destroy', 'uses' => 'MessageController@destroy']);

                get('message/sent', ['as' => 'message.sent', 'uses' => 'MessageController@sent']);
                get('message/trash', ['as' => 'message.trash', 'uses' => 'MessageController@trash']);
                get('message/restore/{slug}', ['as' => 'message.restore', 'uses' => 'MessageController@restore']);

                get('profile/index', ['as' => 'profile.index', 'uses' => 'ProfileController@index']);
                get('profile/update', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
                post('profile/update', ['as' => 'profile.postUpdate', 'uses' => 'ProfileController@postUpdate']);
                post('change/password', ['as' => 'password_change', 'uses' => 'ProfileController@change']);

                get('blog/index', ['as' => 'blog.index', 'uses' => 'BlogController@index']);
                get('blog/create', ['as' => 'blog.create', 'uses' => 'BlogController@create']);
                post('blog/create', ['as' => 'blog.store', 'uses' => 'BlogController@store']);
                get('blog/delete/{id}', ['as' => 'blog.destroy', 'uses' => 'BlogController@destroy']);

                get('category/index', ['as' => 'category.index', 'uses' => 'CategoryController@index']);
                get('category/create', ['as' => 'category.create', 'uses' => 'CategoryController@create']);
                post('category/create', ['as' => 'category.store', 'uses' => 'CategoryController@store']);
                get('category/delete/{id}', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);

                get('tag/index', ['as' => 'tag.index', 'uses' => 'TagController@index']);
                get('tag/create', ['as' => 'tag.create', 'uses' => 'TagController@create']);
                post('tag/create', ['as' => 'tag.store', 'uses' => 'TagController@store']);
                get('tag/delete/{id}', ['as' => 'tag.destroy', 'uses' => 'TagController@destroy']);


            });

            post('message/reply', ['as' => 'message.reply', 'uses' => 'MessageController@reply']);

        });

        Route::group(['middleware' => ['message_receiver']], function () {

            get('message/inbox-details/{slug}', ['as' => 'message.inbox-detail', 'uses' => 'MessageController@inboxDetial']);

        });


    });
    
});
