<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PropertySearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => 'required',
            'type_id' => 'required',
            'purpose_id' => 'required',
            'min_price' => 'required',
            'max_price' => 'required',
        ];
    }
}
