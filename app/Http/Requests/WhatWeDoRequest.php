<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WhatWeDoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'what_we_do_title' => 'required|max:50',
            'what_we_do_icon' => 'required',
            'what_we_do_description' => 'required|max:200',
        ];
    }
}
