<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadPropertyRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $dontFlash = [ 'property_images' ];
        $dontFlash = [ 'file' ];

        return [
            'type_id' => 'required',
            'purpose_id' => 'required',
            'city_id' => 'required',
            'price' => 'required|numeric',
            // 'features_id' => 'required|min:3',
            'description' => 'required|max:400',
            'address' => 'required|max:21',
            'property_images' => 'required|min:2',
        ];
    }
}
