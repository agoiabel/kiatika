<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SocialSettingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'social_facebook' => 'required',
            'social_twitter' => 'required',
            'social_linkedin' => 'required',
            'social_gplus' => 'required',
        ];
    }
}
