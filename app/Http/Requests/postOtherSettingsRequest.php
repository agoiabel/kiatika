<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class postOtherSettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_header_code' => 'required',
            'site_footer_code' => 'required',
        ];
    }
}
