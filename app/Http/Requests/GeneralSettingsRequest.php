<?php

namespace App\Http\Requests;

use App\Settings;
use App\Http\Requests\Request;

class GeneralSettingsRequest extends Request
{

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;

        $this->setting = $this->settings->findBy('id', 1);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_style' => $this->setting->site_style == '' ? 'required' : '',
            'site_logo' => $this->setting->site_logo == '' ? 'required' : '',
            'site_favicon' => $this->setting->site_favicon == '' ? 'required' : '',
            'site_name' => 'required',
            'site_email' => 'required|email',
            'currency_sign' => 'required',
            'site_description' => 'required',
            'site_keywords' => 'required',
            'footer_widget1' => 'required',
            'footer_widget2' => 'required',
            'footer_widget3' => 'required',
            'site_copyright' => 'required',
        ];
    }
}
