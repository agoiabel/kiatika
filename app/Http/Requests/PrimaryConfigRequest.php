<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PrimaryConfigRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'required|mimes:png,jpg,jpeg',
            'favicon' => 'required|mimes:png,jpg,jpeg',
            'main_title' => 'required|max:30',
            'header_title' => 'required|max:30',
            'color_picker' => 'required',
            'index_page_view' => 'required',
            'localization' => 'required',
            'currency_sign' => 'required',
            'about_us' => 'required|max:165',
        ];
    }
}
