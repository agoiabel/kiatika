<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|numeric',
            'city_id' => 'required',
            'about' => 'required|max:175',
            'facebook' => 'required',
            'twitter' => 'required',
            'gplus' => 'required',
            'linkedin' => 'required',
            'image_icon' => 'required|mimes:jpg,jpeg,gif,png',
        ];
    }
}
