<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConfigContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone_number' => 'required',
            'office_address' => 'required',
            'country' => 'required',
            'twitter' => 'required',
            'facebook' => 'required',
            // 'youtube' => 'required',
            // 'instagram' => 'required',
            // 'linkedin' => 'required',
        ];
    }
}
