<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{

    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['sender_id', 'receiver_id', 'subject', 'body'];

    /**
     * a message belongs to a user
     * 
     * @return 
     */
    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    /**
     * a message belongs to a user
     * 
     * @return 
     */
    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    /**
     * a message has one sent messages
     * 
     * @return 
     */
    public function sentMessage()
    {
        return $this->hasOne('App\SentMessage', 'message_id');
    }

    /**
     * a message has one received messages
     * 
     * @return 
     */
    public function receivedMessage()
    {
        return $this->hasOne('App\ReceivedMessage', 'message_id');
    }
}
