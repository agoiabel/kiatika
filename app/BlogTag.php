<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
	use ModelHelper;
	
	/**
	 * attr that can mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['blog_id', 'tag_id'];

    /**
     * a blogtag belongs to a tag
     * 
     * @return 
     */
    public function tag()
    {
    	return $this->belongsTo('App\Tag');
    }

    /**
     * a blogtag belongs to a blog
     * 
     * @return 
     */
    public function blog()
    {
    	return $this->belongsTo('App\Blog');
    }

}
