<?php

namespace App;

use App\Traits\Sluggable;
use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	use ModelHelper, Sluggable;

	/**
	 * Generate slug from title
	 * 
	 * @return 
	 */
	public function getSluggableString()
	{
		return $this->name;
	}

	/**
	 * attr that can be mass assigned
	 * 
	 * @var [type]
	 */
    protected $fillable = ['slug', 'name'];

    /**
     * get all
     * 
     * @return 
     */
    public function getAll()
    {
    	return $this->get();	
    }

    /**
     * a tag belongs to many blog
     * 
     * @return 
     */
    public function tagblogs()
    {
    	return $this->hasMany('App\BlogTag');
    }
    
}
