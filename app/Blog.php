<?php

namespace App;

use App\Traits\Sluggable;
use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	use Sluggable, ModelHelper;

	/**
	 * generate slug
	 * 
	 * @return 
	 */
	public function getSluggableString()
	{
		return $this->title;
	}

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['slug', 'authur_id', 'category_id', 'title', 'description', 'image', 'published_at'];

    /**
     * a blog belongs to a user
     * 
     * @return 
     */
    public function authur()
    {
    	return $this->belongsTo('App\User', 'authur_id');
    }

    /**
     * a blog belongs to a category
     * 
     * @return 
     */
    public function category()
    {
    	return $this->belongsTo('App\Category', 'category_id');
    }

    /**
     * a blog belongs to many tags
     * 
     * @return 
     */
    public function blogtags()
    {
    	return $this->hasMany('App\BlogTag');
    }

    /**
     * sync blog 
     * 
     * @param  [type] $tag_ids 
     * @return [type]          
     */
    public function syncBlogAndTag($tag_ids)
    {
    	$this->tags()->sync($tag_ids);

    	return $this;
    }

    /**
     * get all blog
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->latest()->get();
    }

    /**
     * Get all for user
     * 
     * @param  User $user 
     * @return 
     */
    public function getAllFor($authur_id)
    {
        return $this->where('authur_id', $authur_id)->latest()->get();
    }

    /**
     * a blog has many comment
     * 
     * @return 
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    
}
