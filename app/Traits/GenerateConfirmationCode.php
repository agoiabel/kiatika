<?php 

namespace App\Traits;

trait GenerateConfirmationCode
{
    public static function bootGenerateConfirmationCode()
    {
        static::saving(function ($model) {
                
            $model->confirmation_code = $model->generate_confirmation_code();
            
        });
    }
}
