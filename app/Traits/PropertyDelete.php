<?php 

namespace App\Traits;

trait PropertyDelete
{
    
    public static function bootPropertyDelete()
    {
        static::deleting(function ($model) {
                foreach ($model->images()->get() as $property) {
                    $property->deletePropertyImage();
                }
            });
    }
}
