<?php

namespace App;

use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use ModelHelper;

    protected $table = 'subscriber';

    protected $fillable = ['email','ip'];
 
    public $timestamps = false;
 
    /**
     * count all subscribers
     * 
     * @return 
     */
    public function countAll()
    {
        return $this->count();
    }

    /**
     * count all subscribers
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->get();
    }
}
