<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GenerateConfirmationCode;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, GenerateConfirmationCode;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'status', 'confirmation_code', 'role_id', 'city_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
     
    /**
     * display all user
     * 
     * @return 
     */
    public function getAllExceptEndUser($page)
    {
        return $this->where('role_id', '!=', Role::CUSTOMER)->paginate($page);
    }

    /**
     * bcypt password
     * 
     * @param 
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * a user has one profile
     * 
     * @return 
     */
    public function profile()
    {
        return $this->hasOne('App\Profile', 'user_id');
    }

    /**
     * a user belong to a role
     * 
     * @return 
     */
    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }

    /**
     * find user table
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * count all properties
     * 
     * @return 
     */
    public function countAll()
    {
        return $this->count();
    }

    /**
     * count all property by
     * @param  string $column 
     * @param  string $attr   
     * @return          
     */
    public function countBy($column, $attr, $column2 = null, $attr2 = null)
    {
        if ($column2 && $attr2) {
            return $this->where($column, $attr)->where($column2, $attr2)->count();
        }

        return $this->where($column, $attr)->count();
    }

    
    /**
     * check if user is an admin
     * 
     * @return integer
     */
    public function isAdmin()
    {
        return $this->attributes['role_id'] === Role::ADMIN;
    }

    /**
     * check if user is an agent
     * 
     * @return integer
     */
    public function isAgent()
    {
        return $this->attributes['role_id'] === Role::AGENT;
    }

    /**
     * check if user is an customer
     * 
     * @return integer
     */
    public function isCustomer()
    {
        return $this->attributes['role_id'] === Role::CUSTOMER;
    }

    /**
     * save new property type
     * 
     * @return 
     */
    public function saveModel()
    {
        $this->save();

        return $this;
    }

    /**
     * initialize type
     * 
     * @return 
     */
    public static function initModel($params)
    {
        $array = [];

        foreach ($params as $key => $value) {
            $array[$key] = $value;
        }

        return new static($array);
    }

    /**
     * a user belongs to a city
     * 
     * @return 
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * confirm email
     * 
     * @return 
     */
    public function confirmEmail()
    {
        $this->status = '1';
     
        $this->confirmation_code = '';
     
        $this->saveModel();
    }

    /**
     * generate confirmation code
     * 
     * @return 
     */
    public function generate_confirmation_code()
    {
        return uniqid().time();
    }

    /**
     * update profile 
     * 
     * @return 
     */
    public function updateConfirmationOnPasswordReset($user)
    {
        $user->confirmation_code = $this->generate_confirmation_code();

        $user->save();

        return $user;
    }

    /**
     * update user password
     * 
     * @param  User $user     
     * @param  string $password 
     * @return            
     */
    public function updatePasswordFor($user, $password)
    {
        $user->confirmation_code = '';
     
        $user->password = $password;

        return $user->save();
    }

    /**
     * update profile
     * 
     * @param  User $user   
     * @param  [] $params 
     * @return          
     */
    public function updateUser($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        $this->save();
    }

    /**
     * a user has many properties
     * 
     * @return 
     */
    public function properties()
    {
        return $this->hasMany('App\Upload', 'agent_id');
    }

    /**
     * a user has an experience
     * 
     * @return 
     */
    public function experience()
    {
        return $this->hasOne('App\Experience', 'user_id');
    }
    
    /**
     * a user has many messages
     * 
     * @return 
     */
    public function message()
    {
        return $this->hasMany('App\Message', 'sender_id');
    }

    /**
     * a user has many sent messages
     * 
     * @return 
     */
    public function sentMessages()
    {
        return $this->hasMany('App\SentMessage', 'sender_id');
    }

    /**
     * a user has many received messages
     * 
     * @return 
     */
    public function receivedMessages()
    {
        return $this->hasMany('App\ReceivedMessage', 'receiver_id');
    }
        
    /**
     * find by name
     * 
     * @return 
     */
    public function findByName($slug)
    {
        return $this->where('name', 'like', $slug.'%')
                    ->paginate(10);
    }

    /**
     * return all user
     * 
     * @return 
     */
    public function getAllAgent()
    {
        return $this->where('role_id', Role::AGENT)->get();
    }

    /**
     * Count all agents
     * 
     * @return 
     */
    public function countAllAgent()
    {
        return $this->where('role_id', Role::AGENT)->count();
    }

    /**
     * Get all customer
     * 
     * @return 
     */
    public function getAllCustomer()
    {
        return $this->where('role_id', Role::CUSTOMER)->get();
    }
    
    /**
     * Count all agents
     * 
     * @return 
     */
    public function countAllCustomer()
    {
        return $this->where('role_id', Role::CUSTOMER)->count();
    }

    /**
     * a user has many blogs
     * 
     * @return 
     */
    public function blog()
    {
        return $this->hasMany('App\Blog', 'authur_id');
    }
        
    /**
     * a user has many comments
     * 
     * @return 
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

}
