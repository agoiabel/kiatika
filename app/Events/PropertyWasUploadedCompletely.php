<?php

namespace App\Events;

use App\Upload;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PropertyWasUploadedCompletely extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Upload $property, $images, $property_features)
    {
        $this->images = $images;

        $this->property = $property;

        $this->property_features = $property_features;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
