<?php

namespace App\Mailers;

use App\User;
use App\Upload;
use App\Message;
use App\ReceivedMessage;

class UserMailer extends AppMailer
{
    /**
     * send email confirmation to user
     * 
     * @return 
     */
    public function sendEmailConfirmationCodeTo(User $user)
    {
        $subject = getcong('site_name');
        $view = 'emails.confirm';
        $data = compact('user');

        return $this->sendTo($user, $subject, $view, $data);
    }

    /**
     * send password reset link to user
     * 
     * @param  User   $user 
     * @return        
     */
    public function sendPasswordResetLinkTo(User $user)
    {
        $subject = getcong('site_name');
        $view = 'emails.password';
        $data = compact('user');

        return $this->sendTo($user, $subject, $view, $data);
    }

    /**
     * notify receiver on message sent
     * 
     * @param  User   $user 
     * @return [type]       
     */
    public function sendMessageNotificationTo(User $user, Message $msg)
    {
        $receivedMessage = $this->getRecievedMessage();

        $subject = getcong('site_name');
        $view = 'emails.message_notice';
        $data = compact('msg', 'user', 'receivedMessage');

        return $this->sendTo($user, $subject, $view, $data);
    }

    /**
     * fetch message
     * 
     * @return 
     */
    protected function getRecievedMessage()
    {
        return (new ReceivedMessage())
                    ->findBy('slug', session()->pull('receivedMessage_slug'));
    }
}
