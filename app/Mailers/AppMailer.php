<?php

namespace App\Mailers;

use Illuminate\Mail\Mailer as Mail;

abstract class AppMailer
{

    /**
     * Mailer class
     * 
     * @param Mail $mail 
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * send message to
     * 
     * @param  string $user, $subject      
     * @param  string $view    
     * @param  [] $data
     *     
     * @return void          
     */
    public function sendTo($user, $subject, $view, $data = [])
    {
        $this->mail->queue($view, $data, function ($message) use ($user, $subject) {
            $message->to($user->email)
                    ->subject($subject);
        });
    }
}
