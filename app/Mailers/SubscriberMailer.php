<?php

namespace App\Mailers;

use App\Upload;
use App\Subscriber;

class SubscriberMailer extends AppMailer
{
    /**
     * send email confirmation to user
     * 
     * @return 
     */
    public function sendMailTo(Subscriber $subscriber, Upload $upload)
    {
        $subject = getcong('site_name');
        $view = 'emails.subscriber';
        $data = compact('upload');

        return $this->sendTo($subscriber, $subject, $view, $data);
    }
}
