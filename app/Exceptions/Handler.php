<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        #HandleException($e);

        switch ($e) {

            case ($e instanceof ModelNotFoundException):

                $e = new NotFoundHttpException($e->getMessage(), $e);

                break;

            case ($e instanceof PasswordNotCorrectException):

                return redirect()->to('auth/login')->with('error_message', 'password is not correct');
                
                break;

            case ($e instanceof UserNotFoundException):

                return redirect()->to('auth/login')->with('error_message', 'email is not correct');
                        
                break;

            case ($e instanceof EmailNotFoundException):
            
                return redirect()->to('auth/login')->with('error_message', 'The email does not exists in the our system');
            
                break;

            case ($e instanceof UserStatusNotVerifiedException):
            
                return redirect()->to('auth/login')->with('error_message', 'Your account is not active');
            
                break;

            case ($e instanceof CustomerEmailWithDifferentNameException):

                return redirect()->to($e->url)->with('error_message', 'same email in our system with a different name');

                break;

            case ($e instanceof EmailNotFoundOnPasswordResetException):

                return redirect()->to('password/email')->with('error_message', 'You have no account with the email provided');

                break;

            case ($e instanceof CustomerNotFoundException):

                return redirect()->to('customer/login')->with('error_message', 'You have no account with the email provided, try with a different email');
            
            default: return parent::render($request, $e);

        }
    }
}
