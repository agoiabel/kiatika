<?php 

namespace App\Exceptions;

class CustomerEmailWithDifferentNameException extends \Exception
{
	public function __construct($url)
	{
		$this->url = $url;
	}
}
