<?php

namespace App\Commands;

use App\Category;
use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateNewCategoryCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Category $category)
    {
        $category->initModel(['name' => $request->name])->saveModel();
    }
}
