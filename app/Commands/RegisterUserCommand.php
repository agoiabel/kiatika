<?php

namespace App\Commands;

use App\Commands\Command;

class RegisterUserCommand extends Command
{
    public $name, $email, $password, $city_id, $role_id;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($name, $email, $password, $city_id, $role_id)
    {
        $this->name = $name;

        $this->email = $email;

        $this->password = $password;

        $this->city_id = $city_id;

        $this->role_id = $role_id;
    }
}
