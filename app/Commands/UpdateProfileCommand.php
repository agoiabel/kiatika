<?php

namespace App\Commands;

use App\Commands\Command;

class UpdateProfileCommand extends Command
{
    /**
     * request
     * 
     * @var 
     */
    public $user_id, $name, $email, $phone, $fax, $city_id, $about, $facebook, $twitter, $gplus, $linkedin, $image_icon;
   
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($user_id, $name, $email, $phone, $fax, $city_id, $about, $facebook, $twitter, $gplus, $linkedin, $image_icon)
    {
        $this->user_id = $user_id;

        $this->name = $name;

        $this->email = $email;

        $this->phone = $phone;

        $this->fax = $fax;

        $this->city_id = $city_id;

        $this->about = $about;

        $this->facebook = $facebook;

        $this->twitter = $twitter;

        $this->gplus = $gplus;

        $this->linkedin = $linkedin;

        $this->image_icon = $image_icon;
    }
}
