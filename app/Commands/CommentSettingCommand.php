<?php

namespace App\Commands;

use App\Settings;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class CommentSettingCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Settings $settings)
    {
        $this->setting = $settings->findBy('id', 1);

        $this->setting->updateSetting($this->map($request));
    }

    /**
     * map command
     * 
     * @param  Command $command 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['disqus_comment_code'] = $request->disqus_comment_code;

        return $input;
    }
}
