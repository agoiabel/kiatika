<?php

namespace App\Commands;

use App\City;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class DeleteCityCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, City $city)
    {
        $city->findBy('slug', $request->slug)->delete();
    }
}
