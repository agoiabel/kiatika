<?php

namespace App\Commands;

use App\Commands\Command;

class PostOtherSettingsCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($site_header_code, $site_footer_code)
    {
        $this->site_header_code = $site_header_code;

        $this->site_footer_code = $site_footer_code;
    }
}
