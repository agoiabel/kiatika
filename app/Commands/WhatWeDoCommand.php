<?php

namespace App\Commands;

use App\Service;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class WhatWeDoCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Service $service)
    {
        $service->initModel($this->turn_to_array($request))->saveModel();
    }

    /**
     * Turn to array
     * 
     * @return 
     */
    public function turn_to_array($request)
    {
        $input = [];

        $input['title'] = $request->what_we_do_title;
        $input['icon'] = $request->what_we_do_icon;
        $input['description'] = $request->what_we_do_description;

        return $input;
    }
}
