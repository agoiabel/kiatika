<?php

namespace App\Commands;

use App\Commands\Command;

class SendMessageCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($recipient, $subject, $body)
    {
        $this->recipient = $recipient;

        $this->subject = $subject;

        $this->body = $body;
    }
}
