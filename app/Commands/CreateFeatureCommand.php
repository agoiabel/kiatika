<?php

namespace App\Commands;

use App\Feature;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateFeatureCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Feature $feature)
    {
        $feature->initModel($this->map($request))->saveModel();
    }

    /**
     * turn the request to an array
     * 
     * @param  Request $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['icon'] = $request->icon;
        $input['name'] = $request->name;

        return $input;
    }
}
