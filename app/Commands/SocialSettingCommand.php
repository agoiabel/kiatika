<?php

namespace App\Commands;

use App\Commands\Command;

class SocialSettingCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($social_facebook, $social_twitter, $social_linkedin, $social_gplus)
    {
        $this->social_facebook = $social_facebook;
        $this->social_twitter = $social_twitter;
        $this->social_linkedin = $social_linkedin;
        $this->social_gplus = $social_gplus;
    }
}
