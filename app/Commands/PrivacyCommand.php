<?php

namespace App\Commands;

use App\Commands\Command;

class PrivacyCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($privacy_policy_title, $privacy_policy_description)
    {
        $this->privacy_policy_title = $privacy_policy_title;

        $this->privacy_policy_description = $privacy_policy_description;
    }
}
