<?php

namespace App\Commands;

use App\PropertyImage;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class DeletePropertyImageCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(PropertyImage $image, Request $request)
    {
        $image->findBy('id', $request->image_id)->deletePropertyImage();
    }
}
