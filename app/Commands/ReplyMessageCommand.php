<?php

namespace App\Commands;

use App\User;
use App\Message;
use App\Commands\Command;
use Illuminate\Http\Request;
use App\Events\MessageWasCreated;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Bus\SelfHandling;

class ReplyMessageCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $this->request = $request;

        event(new MessageWasCreated($this->getRecipient(), $this->saveNewMessage()));
    }

    /**
     * Get message recepient
     * 
     * @return 
     */
    public function getRecipient()
    {

        return (new User())->findBy('id', $this->request->receiver_id);
    }

    /**
     * save new message
     * 
     * @return 
     */
    public function saveNewMessage()
    {
        return (new Message())->initModel($this->turnMessageToAnArray())->saveModel();
    }

    /**
     * turn message command to a array
     * 
     * @return 
     */
    public function turnMessageToAnArray()
    {
        $input = [];

        $input['sender_id'] = Auth::user()->id;
        $input['receiver_id'] = $this->request->receiver_id;
        $input['subject'] = $this->request->subject;
        $input['body'] = $this->request->body;

        return $input;
    }
}
