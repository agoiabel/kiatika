<?php

namespace App\Commands;

use App\Commands\Command;

class PrimaryConfigurationCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($logo, $favicon, $main_title, $header_title, $color_picker, $index_page_view, $localization, $currency_sign, $about_us)
    {
        $this->logo = $logo;

        $this->favicon = $favicon;

        $this->main_title = $main_title;

        $this->header_title = $header_title;

        $this->color_picker = $color_picker;

        $this->index_page_view = $index_page_view;
     
        $this->localization = $localization;

        $this->currency_sign = $currency_sign;
     
        $this->about_us = $about_us;
    }

}
