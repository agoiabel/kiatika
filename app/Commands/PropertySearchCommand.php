<?php

namespace App\Commands;

use App\Commands\Command;

class PropertySearchCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($city_id, $type_id, $purpose_id, $min_price, $max_price)
    {
        $this->city_id = $city_id;

        $this->type_id = $type_id;

        $this->purpose_id = $purpose_id;

        $this->min_price = $min_price;

        $this->max_price = $max_price;
    }
}
