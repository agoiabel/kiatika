<?php

namespace App\Commands;

use App\Commands\Command;

class CreatePropertyCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($type_id, $purpose_id, $price, $features_id, $description, $city_id, $address, $property_images)
    {
        $this->price = $price;

        $this->type_id = $type_id;
        
        $this->city_id = $city_id;

        $this->address = $address;

        $this->purpose_id = $purpose_id;

        $this->description = $description;

        $this->features_id = $features_id;

        $this->property_images = $property_images; 
    }
}
