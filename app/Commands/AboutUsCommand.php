<?php

namespace App\Commands;

use App\Commands\Command;

class AboutUsCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($about_us_title, $about_us_description)
    {
        $this->about_us_title = $about_us_title;

        $this->about_us_description = $about_us_description;
    }
}
