<?php

namespace App\Commands;

use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Services\TakeActionBaseOnSingleMessage;

class DeleteSingleMessageCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, TakeActionBaseOnSingleMessage $action)
    {
    }
}
