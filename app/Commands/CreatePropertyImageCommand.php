<?php

namespace App\Commands;

use App\PropertyImage;
use App\Commands\Command;
use Illuminate\Http\Request;
use App\Services\FileUploader;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Events\PropertyWasUploadedCompletely;

class CreatePropertyImageCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void 640, 425
     */
    public function handle(Request $request, PropertyImage $propertyImage, FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;

        event(new PropertyWasUploadedCompletely($propertyImage->initModel($this->map($request))->saveModel()->property));
    }

    /**
     * map request
     * 
     * @param  Request $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['property_id'] = session()->get('property_id');
        $input['slider_path'] = $this->fileUploader->makeThumbnail($request->file, 770, 460);
        $input['property_homepage_path'] = $this->fileUploader->makeThumbnail($request->file, 370, 240);
        $input['property_single_path'] = $this->fileUploader->makeThumbnail($request->file, 132, 80);

        return $input;
    }
}
