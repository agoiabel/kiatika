<?php

namespace App\Commands;

use App\Commands\Command;

class CareerWithUsCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($careers_with_us_title, $careers_with_us_description)
    {
        $this->careers_with_us_title = $careers_with_us_title;

        $this->careers_with_us_description = $careers_with_us_description;
    }
}
