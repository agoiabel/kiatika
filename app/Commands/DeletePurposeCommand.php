<?php

namespace App\Commands;

use App\Purpose;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class DeletePurposeCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Purpose $purpose, Request $request)
    {
        $purpose->findBy('slug', $request->slug)->delete();
    }
}
