<?php

namespace App\Commands;

use App\Upload;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class DeletePropertyCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Upload $property, Request $request)
    {
        $property->findBy('slug', $request->slug)->delete();
    }
}
