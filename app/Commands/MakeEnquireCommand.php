<?php

namespace App\Commands;

use App\Commands\Command;

class MakeEnquireCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($property_id, $role_id, $name, $email, $message)
    {
        $this->email = $email;

        $this->role_id = $role_id;

        $this->name = $name;

        $this->message = $message;

        $this->property_id = $property_id;
    }
}
