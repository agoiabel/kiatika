<?php

namespace App\Commands;

use App\Commands\Command;

class AuthenticateCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($email, $password)
    {
        $this->email = $email;

        $this->password = $password;
    }
}
