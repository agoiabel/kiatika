<?php

namespace App\Commands;

use App\Commands\Command;
use Illuminate\Http\Request;
use App\Services\TakeGroupMessageAction;
use Illuminate\Contracts\Bus\SelfHandling;

class RegisterGroupActionCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        return (new TakeGroupMessageAction($request));
    }
}
