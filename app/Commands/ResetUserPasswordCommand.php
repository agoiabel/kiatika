<?php

namespace App\Commands;

use App\Commands\Command;

class ResetUserPasswordCommand extends Command
{
    public $token, $password;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($token, $password)
    {
        $this->token = $token;

        $this->password = $password;
    }
}
