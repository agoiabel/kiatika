<?php

namespace App\Commands;

use App\Commands\Command;

class CreateCommentCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($role_id, $blog_id, $comment, $name, $email)
    {
        $this->role_id = $role_id;

        $this->blog_id = $blog_id;

        $this->comment = $comment;

        $this->name = $name;

        $this->email = $email;
    }
}



