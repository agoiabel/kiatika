<?php

namespace App\Commands;

use App\Commands\Command;

class EmailPasswordResetCommand extends Command
{
    public $email;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }
}
