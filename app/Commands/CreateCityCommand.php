<?php

namespace App\Commands;

use App\City;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateCityCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(City $city, Request $request)
    {
        $city->initialize($request->city_name)->saveNew();
    }
}
