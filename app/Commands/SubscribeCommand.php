<?php

namespace App\Commands;

use App\Commands\Command;

class SubscribeCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($ip, $email)
    {
        $this->ip = $ip;

        $this->email = $email;
    }
}
