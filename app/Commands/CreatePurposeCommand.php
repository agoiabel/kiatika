<?php

namespace App\Commands;

use App\Purpose;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class CreatePurposeCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Purpose $purpose)
    {
        $purpose->initModel($this->map($request))->saveModel();
    }

    /**
     * turn the request to an array
     * 
     * @param  Request $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['name'] = $request->name;

        return $input;
    }
}
