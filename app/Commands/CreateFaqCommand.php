<?php

namespace App\Commands;

use App\Faq;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateFaqCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Faq $faq)
    {
        $faq->initModel($this->map($request))->saveModel();
    }

    /**
     * map request
     * 
     * @return 
     */
    protected function map($request)
    {
        $input = [];

        $input['question'] = $request->question;    
        $input['answer'] = $request->answer;

        return $input;    
    }

}
