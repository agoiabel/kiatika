<?php

namespace App\Commands;

use App\Tag;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateNewTagCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Tag $tag)
    {
        $tag->initModel(['name' => $request->name])->saveModel();
    }

}
