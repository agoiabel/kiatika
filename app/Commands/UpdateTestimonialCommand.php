<?php

namespace App\Commands;

use App\Testimonial;
use App\Commands\Command;
use Illuminate\Http\Request;
use App\Services\FileUploader;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateTestimonialCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Testimonial $testimonial, FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;

        $testimonial->findBy('slug', $request->slug)->updateModel($this->map($request));
    }

    /**
     * turn request to an array
     * 
     * @param  Request $request 
     * @return [type]          
     */
    public function map($request)
    {
        $input = [];

        $input['name'] = $request->name;
        $input['occupation'] = $request->occupation;
        $input['testimony'] = $request->testimony;
        $input['testimony'] = $request->testimony;
        $input['image'] = $this->fileUploader->makeThumbnail($request->image, 100, 100);

        return $input;
    }
}
