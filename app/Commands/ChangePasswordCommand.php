<?php

namespace App\Commands;

use App\User;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class ChangePasswordCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, User $user)
    {
        $user = $users->findBy('id', $this->user->id);

        $user->password = $request->password;

        $user->save();
    }
}
