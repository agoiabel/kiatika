<?php

namespace App\Commands;

use App\Upload;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class PropertyStatusCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Upload $property)
    {
        $property->findBy('slug', $request->slug)->updateStatus();
    }
}
