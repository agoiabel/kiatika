<?php

namespace App\Commands;

use App\Blog;
use App\Commands\Command;
use Illuminate\Http\Request;
use App\Services\FileUploader;
use App\Services\ProcessTagAndCategory;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateBlogCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Blog $blog, Request $request, FileUploader $fileUploader, ProcessTagAndCategory $processTagAndCategory)
    {
    	$this->blog = $blog;

        $this->request = $request;

    	$this->fileUploader = $fileUploader;

        $this->processTagAndCategory = $processTagAndCategory;

        $this->processBlog();
    }

    /**
     * create new blog
     * 
     * @return 
     */
    protected function processBlog()
    {
    	$category_id = $this->processTagAndCategory->processCategory($this->request);

    	$blog_id = $this->createNewBlogIn($category_id)->id;

        $tag_ids = $this->processTagAndCategory->processTags($this->request, $blog_id);
    }

    /**
     * create new blog
     * 
     * @return 
     */
    protected function createNewBlogIn($category_id)
    {
        return $this->blog->initModel($this->mapBlogWith($category_id))->saveModel();
    }

    /**
     * turn blog dependencies to an array
     * @param   $category_id 
     * @return               
     */
    protected function mapBlogWith($category_id)
    {
        $input = [];

        $input['authur_id'] = $this->request->authur_id;
        $input['category_id'] = $category_id;
        $input['title'] = $this->request->title;
        $input['description'] = $this->request->description;
        $input['published_at'] = $this->request->published_at;
        $input['image'] = $this->fileUploader->makeThumbnail($this->request->image, 770, 460);

        return $input;
    }

}
