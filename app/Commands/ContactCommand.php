<?php

namespace App\Commands;

use App\Commands\Command;

class ContactCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($role_id, $name, $email, $message, $subject)
    {

        $this->name = $name;

        $this->email = $email;

        $this->subject = $subject;

        $this->message = $message;

        $this->role_id = $role_id;        
    }
}
