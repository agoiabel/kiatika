<?php

namespace App\Commands;

use App\Purpose;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdatePurposeCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Purpose $purpose, Request $request)
    {
        $purpose->findBy('slug', $request->slug)->updateModel($this->map($request));
    }

    /**
     * turn request to an array
     * 
     * @param  Request $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['name'] = $request->name;

        return $input;
    }
}
