<?php

namespace App\Commands;

use App\Configuration;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class MissionVisionCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Configuration $config)
    {
        $this->request = $request;

        $config->findBy('id', '1')->updateModel($this->map());
    }

    /**
     * turn to an array
     * 
     * @return 
     */
    protected function map()
    {
        $input = [];

        $input['mission'] = $this->request->mission;    
        $input['vision'] = $this->request->vision;    

        return $input;
    }

}
