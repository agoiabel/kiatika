<?php

namespace App\Commands;

use App\Settings;
use App\Commands\Command;
use Illuminate\Http\Request;
use App\Services\FileUploader;
use Illuminate\Contracts\Bus\SelfHandling;

class GeneralSettingCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Settings $settings, FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;

        $this->setting = $settings->findBy('id', 1);

        $this->setting->updateSetting($this->map($request));
    }

    /**
     * map request
     * 
     * @param  Request $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['site_style'] = $request->site_style;
        $input['site_name'] = $request->site_name;
        $input['site_logo'] = ($request->site_logo == null) ?  $this->setting->site_logo : $this->fileUploader->makeThumbnail($request->site_logo, 360, 125);
        $input['site_favicon'] = ($request->site_favicon) == null ?  $this->setting->site_favicon : $this->fileUploader->makeThumbnail($request->site_favicon, 16, 16);
        $input['site_email'] = $request->site_email;
        $input['currency_sign'] = $request->currency_sign;
        $input['site_description'] = $request->site_description;
        $input['site_keywords'] = $request->site_keywords;
        $input['footer_widget1'] = $request->footer_widget1;
        $input['footer_widget2'] = $request->footer_widget2;
        $input['footer_widget3'] = $request->footer_widget3;
        $input['site_copyright'] = $request->site_copyright;

        return $input;
    }
}
