<?php

namespace App\Commands;

use App\Client;
use App\Commands\Command;
use Illuminate\Http\Request;
use App\Services\FileUploader;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateClientCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Client $client, Request $request, FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;

        $client->initModel($this->map($request))->saveModel();
    }

    /**
     * turn request to an array
     * 
     * @param   $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['name'] = $request->name;
        $input['image_path'] = $this->fileUploader->makeThumbnail($request->image, 270, 170);

        return $input;
    }
    
}
