<?php

namespace App\Commands;

use App\PropertyType;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class CreatePropertyTypeCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(PropertyType $propertyType, Request $request)
    {
        $propertyType->initialize($request->type)->saveNew();
    }
}
