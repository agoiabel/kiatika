<?php

namespace App\Commands;

use App\Configuration;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class ContactConfigurationCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, Configuration $config)
    {
        $config->findBy('id', '1')->updateModel($this->turn_to_array($request));
    }

    /**
     * turn to array
     * 
     * @return 
     */
    public function turn_to_array($request)
    {
        $input = [];

        $input['email'] = $request->email;
        $input['phone_number'] = $request->phone_number;
        $input['office_address'] = $request->office_address;
        $input['country'] = $request->country;
        $input['twitter'] = $request->twitter;
        $input['twitter'] = $request->twitter;
        $input['facebook'] = $request->facebook;
        $input['youtube'] = $request->youtube;
        $input['instagram'] = $request->instagram;
        $input['linkedin'] = $request->linkedin;

        return $input;
    }
}
