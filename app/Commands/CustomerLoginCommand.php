<?php

namespace App\Commands;

use App\User;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Exceptions\CustomerNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CustomerLoginCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Request $request, User $user)
    {
        try {
            return $user->findBy('email', $request->email);
        } catch (ModelNotFoundException $e) {
            throw new CustomerNotFoundException();
        }
    }
}
