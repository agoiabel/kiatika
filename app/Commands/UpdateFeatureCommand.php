<?php

namespace App\Commands;

use App\Feature;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateFeatureCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Feature $feature, Request $request)
    {
        $feature->findBy('slug', $request->slug)->updateModel($this->map($request));
    }

    /**
     * turn request to an array
     * 
     * @param  Request $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['name'] = $request->name;

        return $input;
    }
}
