<?php

namespace App\Commands;

use App\Feature;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class DeleteFeatureCommand extends Command implements SelfHandling
{
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Feature $feature, Request $request)
    {
        $feature->findBy('slug', $request->slug)->delete();
    }
}
