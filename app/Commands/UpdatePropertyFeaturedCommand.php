<?php

namespace App\Commands;

use App\Upload;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdatePropertyFeaturedCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(Upload $upload, Request $request)
    {
        $upload->findBy('slug', $request->slug)->updateModel($this->map($request));
    }

    /**
     * turn request to an array
     * 
     * @param  Request $request 
     * @return           
     */
    public function map($request)
    {
        $input = [];

        $input['featured'] = $request->has('featured');

        return $input;
    }
}
