<?php

namespace App\Commands;

use App\Commands\Command;

class TermsAndConditionCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($terms_conditions_title, $terms_conditions_description)
    {
        $this->terms_conditions_title = $terms_conditions_title;

        $this->terms_conditions_description = $terms_conditions_description;
    }
}
