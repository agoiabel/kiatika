<?php

namespace App\Commands;

use App\Commands\Command;

class ReadMessageCommand extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }
}
