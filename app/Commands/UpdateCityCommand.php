<?php

namespace App\Commands;

use App\City;
use App\Commands\Command;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateCityCommand extends Command implements SelfHandling
{

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(City $city, Request $request)
    {
        $city->findBy('slug', $request->slug)->updateCity($request->city_name);
    }
}
