<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';

    protected $fillable = ['site_style','site_name', 'site_email', 'site_logo', 'site_favicon','site_description','site_header_code','site_footer_code','site_copyright','addthis_share_code','disqus_comment_code'];

 
    
    public $timestamps = false;
    
    /**
     * find by
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * save new
     * 
     * @return 
     */
    public function saveNew()
    {
        $this->save();

        return $this;
    }

    /**
     * update profile
     * 
     * @param  User $user   
     * @param  [] $params 
     * @return          
     */
    public function updateSetting($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        $this->saveNew();
    }
}
