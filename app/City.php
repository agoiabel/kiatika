<?php

namespace App;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use Sluggable;

    const Unpublished = '0';
    const Published = '1';

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['id','city_name'];

    /**
     * timestamp not included in database
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * generate slug from city name
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->city_name;
    }

    /**
     * a city has many users
     * 
     * @return 
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * get all
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->orderBy('city_name')->get();
    }

    /**
     * find city by
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }
    
    /**
     * initialize city
     * 
     * @return 
     */
    public static function initialize($name)
    {
        return new static([
            'city_name' => $name,
        ]);
    }

    /**
     * save new city
     * 
     * @return 
     */
    public function saveNew()
    {
        $this->save();

        return $this;
    }

    /**
     * update city
     * 
     * @return 
     */
    public function updateCity($name)
    {
        $this->city_name = $name;

        $this->saveNew();
    }

    /**
     * status lookup
     * 
     * @var []
     */
    public static $statusLookUp = [
        '0' => Services\CityStatusPublished::class,
        '1' => Services\CityStatusUnPublished::class,
    ];

    /**
     * update status
     * 
     * @return 
     */
    public static function updateStatus($city)
    {
        $class = static::$statusLookUp[$city->status];

        return new $class($city);
    }

    /**
     * a city has many properties
     * 
     * @return 
     */
    public function properties()
    {
        return $this->hasMany('App\Upload', 'city_id');
    }
}
