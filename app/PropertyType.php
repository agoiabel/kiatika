<?php

namespace App;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    use Sluggable;

    /**
     * generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->type;
    }

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['slug', 'type'];

    /**
     * a type has many properties
     * 
     * @return 
     */
    public function properties()
    {
        return $this->hasMany('App\Upload', 'type_id');
    }

    /**
     * get all property
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->get();
    }

    /**
     * save new property type
     * 
     * @return 
     */
    public function saveNew()
    {
        $this->save();

        return $this;
    }

    /**
     * initialize type
     * 
     * @return 
     */
    public static function initialize($type)
    {
        return new static([
            'type' => $type,
        ]);
    }

    /**
     * query table 
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * update property type
     * 
     * @return 
     */
    public function updatePropertyType($type)
    {
        $this->type = $type;

        $this->saveNew();
    }
}
