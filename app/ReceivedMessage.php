<?php

namespace App;

use App\Traits\Sluggable;
use App\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivedMessage extends Model
{
    use SoftDeletes, ModelHelper, Sluggable;

    const MSG_READ = '1';
    const MSG_UNREAD = '0';

    /**
     * generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->message->id .' '. $this->message->subject;
    }


    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['receiver_id', 'message_id', 'slug', 'is_read', 'read_date_time', 'deleted_at'];

    /**
     * deleted at 
     * 
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * a received message belongs to a receiver
     * 
     * @return 
     */
    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    /**
     * a received message belongs to a message
     * 
     * @return 
     */
    public function message()
    {
        return $this->belongsTo('App\Message', 'message_id');
    }

    /**
     * find by 
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * Get current user messages
     * 
     * @return 
     */
    public function getMessagesForCurrent($receiver_id)
    {
        return $this->where('receiver_id', $receiver_id)->latest()->paginate(10);
    }

    /**
     * Get all unread message
     * 
     * @return 
     */
    public function getUnreadMessagesCountForCurrent($receiver_id)
    {
        $message = $this->where('receiver_id', $receiver_id)
                    ->where('is_read', self::MSG_UNREAD)
                    ->count();

        if ($message == '0') {
            return false;
        }

        return $message;
    }

    /**
     * Get trash message for user
     * 
     * @return 
     */
    public function getTrashMessagesForCurrent($user_id)
    {
        return $this->onlyTrashed()
                    ->where('receiver_id', $user_id)
                    ->latest()
                    ->paginate(10);
    }

    /**
     * Get trash
     * 
     * @return 
     */
    public function findTrashBy($column, $attr)
    {
        return $this->onlyTrashed()
                    ->where($column, $attr)->firstOrFail();
    }

    /**
     * Count all received for user
     * 
     * @return 
     */
    public function countAll($receiver_id)
    {
        return $this->where('receiver_id', $receiver_id)->count();
    }
}
