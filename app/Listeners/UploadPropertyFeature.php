<?php

namespace App\Listeners;

use App\Feature;
use App\PropertyFeature;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\PropertyWasUploadedCompletely;

class UploadPropertyFeature
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Feature $feature, PropertyFeature $property_feature)
    {
        $this->feature = $feature;

        $this->property_feature = $property_feature;
    }

    /**
     * Handle the event.
     *
     * @param  PropertyWasUploadedCompletely  $event
     * @return void
     */
    public function handle(PropertyWasUploadedCompletely $event)
    {
        $this->event = $event;

        $this->saveAllFeatureForNewProperty();

        $this->updatePropertyFeature();
    }

    /**
     * save all feature to new property
     * 
     * @return 
     */
    protected function saveAllFeatureForNewProperty()
    {
        $all_features = $this->feature->lists('id');

        foreach ($all_features as $feature) {
            $this->property_feature->initModel(['upload_id' => $this->event->property->id, 'feature_id' => $feature])->saveModel();
        }
    }

    /**
     * Update property with their features
     * 
     * @return 
     */
    protected function updatePropertyFeature()
    {
        foreach ($this->event->property_features as $present_feature) {

            $property_to_update = $this->property_feature->where('feature_id', $present_feature)->where('upload_id', $this->event->property->id)->firstOrFail();

            $property_to_update->updateModel(['is_present' => true]);
        }
    }

}
