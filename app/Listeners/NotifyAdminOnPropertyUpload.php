<?php

namespace App\Listeners;

use App\Events\PropertyWasUploadedCompletely;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminOnPropertyUpload
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PropertyWasUploadedCompletely  $event
     * @return void
     */
    public function handle(PropertyWasUploadedCompletely $event)
    {
        #will work on notifying admin when my messaging system is added
    }
}
