<?php

namespace App\Listeners;

use App\User;
use App\Mailers\UserMailer;
use App\Events\MessageWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyReceiver
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserMailer $mailer, User $user)
    {
        $this->user = $user;

        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  MessageWasCreated  $event
     * @return void
     */
    public function handle(MessageWasCreated $event)
    {
        $this->mailer->sendMessageNotificationTo($this->getUser($event), $event->message);
    }

    /**
     * get user
     * 
     * @return 
     */
    public function getUser($event)
    {
        return $this->user->findBy('id', $event->receiver->id);
    }
}
