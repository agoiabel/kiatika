<?php

namespace App\Listeners;

use App\Subscriber;
use App\Mailers\SubscriberMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\PropertyWasUploadedCompletely;

class NotifySubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SubscriberMailer $subscriberMailer, Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;

        $this->subscriberMailer = $subscriberMailer;
    }

    /**
     * Handle the event.
     *
     * @param  PropertyWasUploadedCompletely  $event
     * @return void
     */
    public function handle(PropertyWasUploadedCompletely $event)
    {
        foreach ($this->subscriber->getAll() as $subscriber) {
            $this->subscriberMailer->sendMailTo($subscriber, $event->property);
        }
    }
}
