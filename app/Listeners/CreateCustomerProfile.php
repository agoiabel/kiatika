<?php

namespace App\Listeners;

use App\Profile;
use App\Events\CustomerUserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateCustomerProfile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }


    /**
     * Handle the event.
     *
     * @param  CustomerUserWasCreated  $event
     * @return void
     */
    public function handle(CustomerUserWasCreated $event)
    {
        $this->profile->initModel($this->map($event))->saveModel();
    }

    /**
     * turn event to an array
     * 
     * @param  Event $event 
     * @return         
     */
    public function map($event)
    {
        $input = [];

        $input['user_id'] = $event->user->id;

        return $input;
    }
}
