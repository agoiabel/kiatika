<?php

namespace App\Listeners;

use App\Experience;
use App\Events\UserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserExperience
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Experience $experience)
    {
        $this->experience = $experience;
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        $this->experience->initModel($this->map($event->user->id))->saveModel();
    }

    /**
     * map request
     * 
     * @return 
     */
    public function map($user_id)
    {
        $input = [];

        $input['user_id'] = $user_id;

        return $input;
    }
}
