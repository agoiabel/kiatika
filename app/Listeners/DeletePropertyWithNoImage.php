<?php

namespace App\Listeners;

use App\Upload;
use Carbon\Carbon;
use App\Events\PropertyWasUploadedCompletely;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeletePropertyWithNoImage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Upload $property)
    {
        $this->property = $property;
    }

    /**
     * Handle the event.
     *
     * @param  PropertyWasUploadedCompletely  $event
     * @return void
     */
    public function handle(PropertyWasUploadedCompletely $event)
    {
        $properties = $this->property->thatHasNoImage()->olderThan(Upload::IMAGE_INTERVAL)->get();

        foreach ($properties as $property) {
            $property->delete();
        }
    }
}
