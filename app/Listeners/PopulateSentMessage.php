<?php

namespace App\Listeners;

use App\SentMessage;
use App\Events\MessageWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PopulateSentMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SentMessage $sentMessage)
    {
        $this->sentMessage = $sentMessage;
    }

    /**
     * Handle the event.
     *
     * @param  MessageWasCreated  $event
     * @return void
     */
    public function handle(MessageWasCreated $event)
    {
        $this->sentMessage->initModel($this->map($event))->saveModel();
    }

    /**
     * turn event to an array
     * 
     * @param  Event $event 
     * @return         
     */
    public function map($event)
    {
        $input = [];

        $input['sender_id'] = $event->message->sender_id;
        $input['message_id'] = $event->message->id;

        return $input;
    }
}
