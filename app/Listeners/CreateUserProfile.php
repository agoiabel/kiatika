<?php

namespace App\Listeners;

use App\Profile;
use App\Events\UserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserProfile
{
    public $profile;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        $this->profile->initModel($this->map($event))->saveModel();
    }

    /**
     * turn event to an array
     * 
     * @param  Event $event 
     * @return         
     */
    public function map($event)
    {
        $input = [];

        $input['user_id'] = $event->user->id;

        return $input;
    }
}
