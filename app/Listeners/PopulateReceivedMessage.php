<?php

namespace App\Listeners;

use App\ReceivedMessage;
use App\Events\MessageWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PopulateReceivedMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ReceivedMessage $receivedMessage)
    {
        $this->receivedMessage = $receivedMessage;
    }

    /**
     * Handle the event.
     *
     * @param  MessageWasCreated  $event
     * @return void
     */
    public function handle(MessageWasCreated $event)
    {
        $receivedMessage = $this->receivedMessage->initModel($this->map($event))->saveModel();

        session()->put('receivedMessage_slug', $receivedMessage->slug);
    }

    /**
     * turn event to an array
     * 
     * @param   $event 
     * @return [type]        
     */
    public function map($event)
    {
        $input = [];

        $input['receiver_id'] = $event->receiver->id;
        $input['message_id'] = $event->message->id;

        return $input;
    }
}
