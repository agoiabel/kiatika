<?php

namespace App\Listeners;

use App\PropertyImage;
use App\Services\FileUploader;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\PropertyWasUploadedCompletely;

class UploadPropertyImage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PropertyImage $propertyImage, FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
     
        $this->propertyImage = $propertyImage;
    }

    /**
     * Handle the event.
     *
     * @param  PropertyWasUploaded  $event
     * @return void
     */
    public function handle(PropertyWasUploadedCompletely $event)
    {
        $this->event = $event;
        
        foreach ($this->event->images as $image) {
            $this->propertyImage->initModel($this->map($image))->saveModel();        
        }

    }

    /**
     * upload and turn image link to an array
     * 
     * @return 
     */
    protected function map($image)
    {
        $input = [];

        $input['property_id'] = $this->event->property->id;
        $input['slider_path'] = $this->fileUploader->makeThumbnail($image, 770, 460);
        $input['property_homepage_path'] = $this->fileUploader->makeThumbnail($image, 370, 240);
        $input['property_single_path'] = $this->fileUploader->makeThumbnail($image, 132, 80);

        return $input;
    }

}
