<?php

namespace App\Listeners;

use App\Upload;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\PropertyWasUploadedCompletely;

class AwardPointOnPropertyUpload
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PropertyWasUploadedCompletely  $event
     * @return void
     */
    public function handle(PropertyWasUploadedCompletely $event)
    {
        $event->property->agent->experience->award(Upload::POINT);
    }
    
}
