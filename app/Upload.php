<?php

namespace App;

use Carbon\Carbon;
use App\Traits\Sluggable;
use App\Traits\ModelHelper;
use App\Traits\PropertyDelete;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    use Sluggable, ModelHelper, PropertyDelete;

    const POSITIVE = true;
    const NEGATIVE = false;
    const POINT = 1;
    const FEATURED = true;
    const HAS_IMAGE = true;
    const IMAGE_INTERVAL = 10;
    const HAS_NO_IMAGE = false;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = [
        'agent_id', 'city_id', 'type_id', 'purpose_id', 'slug', 'price', 'address',
        'short_description', 'description', 'featured', 'status', 'has_image'
    ];

    /**
     * Generate slug from 
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->address .' '. $this->city->city_name ;
    }

    /**
     * get all property for current user
     * 
     * @return 
     */
    public function getAllFor($agent_id)
    {
        return $this->where('agent_id', $agent_id)->get();
    }

    /**
     * get all published properties
     * 
     * @return 
     */
    public function getAllPublished()
    {
        return $this->where('status', self::NEGATIVE)
                    ->paginate(9);
    }

    /**
     * get all featured published properties
     * 
     * @return 
     */
    public function getAllPublishedFeatured()
    {
        return $this->where('status', self::NEGATIVE)
                    ->where('featured', self::FEATURED)
                    ->paginate(9);        
    }

    /**
     * get all features
     * 
     * @return 
     */
    public function getAll()
    {
        return  $this->with(['images', 'type', 'city', 'purpose', 'features' => function ($query) {

                $query->latest()->get();

        }])->where('status', self::NEGATIVE)
           ->where('featured', self::FEATURED)
           ->limit(3)->latest()->get();
    }    

    /**
     * get six latest features properies
     * 
     * @return 
     */
    public function getAllFeatured($value)
    {
        return  $this->with(['images', 'type', 'city', 'purpose', 'features' => function ($query) {

                $query->latest()->get();

        }])->where('status', self::NEGATIVE)
           ->where('featured', self::FEATURED)
           ->limit($value)->latest()->get();
    }

    /**
     * get six latest properies
     * 
     * @return 
     */
    public function getAllLatest()
    {
        return  $this->with(['images', 'type', 'city', 'purpose', 'features' => function ($query) {

                $query->latest()->get();

        }])->where('status', self::NEGATIVE)
           ->where('featured', '!=', self::FEATURED)
           ->limit(6)->latest()->get();
    }

    /**
     * find this by
     * 
     * @return 
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * a property belongs to a purpose
     * 
     * @return 
     */
    public function purpose()
    {
        return $this->belongsTo('App\Purpose', 'purpose_id');
    }

    /**
     * an upload has many property features
     * 
     * @return 
     */
    public function features()
    {
        return $this->hasMany('App\PropertyFeature', 'upload_id');
    }

    /**
     * save property and feature
     * 
     * @return 
     */
    public function syncPropertyAndFeature($feature_ids)
    {
        $this->features()->sync($feature_ids);

        return $this;
    }

    /**
     * a property has many images
     * 
     * @return 
     */
    public function images()
    {
        return $this->hasMany('App\PropertyImage', 'property_id');
    }

    /**
     * a property belongs to a type
     * 
     * @return 
     */
    public function type()
    {
        return $this->belongsTo('App\PropertyType', 'type_id');
    }

    /**
     * a property belongs to a city
     * 
     * @return 
     */
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    /**
     * a property belongs to an agent
     * 
     * @return 
     */
    public function agent()
    {
        return $this->belongsTo('App\User', 'agent_id');
    }

    /**
     * fetch property with no image
     * 
     * @return 
     */
    public function scopeThatHasNoImage($query)
    {
        return $query->where('has_image', self::HAS_NO_IMAGE);
    }

    /**
     * get property older than 
     * 
     * @return 
     */
    public function scopeOlderThan($query, $interval)
    {
        return $query->where('created_at', '<=', Carbon::now()->subMinutes($interval)->toDateTimeString());
    }

    /**
     * search by
     * 
     * @return 
     */
    public function searchBy($command)
    {
        return $this->where('city_id', $command->city_id)
                    ->where('type_id', $command->type_id)
                    ->where('purpose_id', $command->purpose_id)
                    ->whereBetween('price', [$command->min_price, $command->max_price])
                    ->where('status', self::NEGATIVE)
                    ->paginate(4);
    }

    /**
     * Toogle property status
     * 
     * @return 
     */
    public function updateStatus()
    {
        ($this->status == self::POSITIVE) ? $this->unpublished() : $this->published();
    }

    /**
     * Unpublished property
     * 
     * @return 
     */
    public function unpublished()
    {
        $this->status = self::NEGATIVE;

        $this->save();
    }

    /**
     * Publish property
     * 
     * @return 
     */
    public function published()
    {
        $this->status = self::POSITIVE;

        $this->save();
    }

    /**
     * Count all property
     * 
     * @return 
     */
    public function countAll()
    {
        return $this->count();
    }

    /**
     * Count all user property
     * @param  User $user_id 
     * @return            
     */
    public function countAllFor($user_id)
    {
        return $this->where('agent_id', $user_id)->count();
    }

    /**
     * Count all featured 
     * 
     * @return 
     */
    public function countAllFeatured()
    {
        return $this->where('featured', self::POSITIVE)->count();
    }

    /**
     * Count all featured 
     * 
     * @return 
     */
    public function countAllFeaturedFor($user_id)
    {
        return $this->where('featured', self::POSITIVE)
                    ->where('agent_id', $user_id)
                    ->count();
    }

    /**
     * Count all published
     * 
     * @return 
     */
    public function countAllPublished()
    {
        return $this->where('status', self::POSITIVE)->count();
    }

    /**
     * Count all for this user
     * 
     * @return 
     */
    public function countAllPublishedFor($user_id)
    {
        return $this->where('status', self::POSITIVE)
                    ->where('agent_id', $user_id)
                    ->count();
    }
}
