<?php

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthenticationTest extends TestCase
{
    use WithoutMiddleware;

    /** @test */
    public function a_user_may_successfully_login()
    {
        $this->visit('auth/login')
        	 ->submitForm([
                    'email' => 'johndoe@admin.com',
                    'password' => 'abc',
                ], 'Sign In')
             ->seePageIs(route('dashboard'));
    }

}
